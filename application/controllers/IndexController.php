<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 9/30/14
 * Time: 5:16 PM
 */

class IndexController extends CI_Controller{
    function index()
    {
        $data['courses']=$this->db->where('parent_id','0')->get('categories')->result();
        $this->load->view('index',$data);
    }

    function description($id)
    {
        $data= $this->db->where('parent_id',$id)->get('categories')->result();
        foreach($data as $result)
        {
            echo $category_id =$result->id;
        }
        $data['data'] = $this->db->where('category_id',$category_id)->get('courses')->row();
        $data['name']= $this->db->where('id',$id)->get('categories')->row();
        $this->load->view('course',$data);
    }
}