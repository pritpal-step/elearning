<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 9/19/14
 * Time: 11:41 AM
 */
class Category extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // REDIRECT IF NOT LOGGED IN
        if(!$this->auth->is_admin())
        {
            redirect('admin/login');
        }

    }
    function index()
    {
        $count = $this->db->where('parent_id',0)->count_all_results('categories');
        $offset=$this->uri->segment(4);

        $config['base_url']=base_url('index.php/admin/category/index/');
        $config['per_page']=10;
        $config['uri_segment']=4;
        $config['total_rows']=$count;
        $config['use_page_numbers']  = TRUE;
        $config['first_url']=base_url('index.php/admin/category/index/1');

        $this->pagination->initialize($config);
        $offset=$offset>0?($offset-1)*$config['per_page']:$offset;
        $data['data'] = $this->db->limit($config['per_page'],$offset)->where('parent_id',0)->get('categories')->result();
        $data["links"] = $this->pagination->create_links();
        $this->load->view('admin/category',$data);
    }
    function add()
    {
        $this->load->view('admin/category_add');
    }
    function course_add($id)
    {
        $data['data'] = $this->db->where('id',$id)->get('categories')->row();

        $this->load->view('admin/course_add',$data);
    }
    function insert()
    {
        $id = $this->input->post('parent');
        if(!$id)
        {
               $this->form_validation->set_rules('name','name', 'required|is_unique[categories.name]');
               $this->form_validation->set_rules('description','description','required');
                if($this->form_validation->run() == false)
                {
                    if($_FILES['file']['name'] == "")
                    {
                        $this->form_validation->set_rules('file','file', 'required');
                        if($this->form_validation->run() == false)
                        {
                            $this->form_validation->set_message('insert',"Field cannot be left empty");
                            $this->load->view('admin/category_add');
                        }
                    }
                    else{
                    $this->form_validation->set_message('insert',"Field cannot be left empty");
                    $this->load->view('admin/category_add');
                    }
                }
                else if($_FILES['file']['name'] == "")
                {
                    $this->form_validation->set_rules('file','file', 'required');
                    if($this->form_validation->run() == false)
                    {
                        $this->form_validation->set_message('insert',"Field cannot be left empty");
                        $this->load->view('admin/category_add');
                    }
                }

               else{
                    $logo = $this->upload();

               $data = array(
                   'name'=>$this->input->post('name'),
                   'parent_id'=> 0,
                   'logo' =>$logo['file_name'],
                   'detail'=>$this->input->post('description'),
                   'display_order'=>$this->input->post('order')
               );
               $status = $this->db->insert('categories',$data);
               if($status == 1)
               $this->session->set_flashdata('flashSuccess','Category has been successfully added');
               redirect('admin/category');
                }
        }
        else
        {
                $this->form_validation->set_rules('name','name', 'required|is_unique[categories.name]');
                if($this->form_validation->run() == false)
                {
                    $this->form_validation->set_message('insert',"Field cannot be left empty");
                    $data['data']= $this->db->where('id',$id)->get('categories')->row();


                    $this->load->view('admin/course_add',$data);
                }
                else{
                    $data = array(
                        'name'=>$this->input->post('name'),
                        'parent_id'=> $id,
                        );
                    $this->db->insert('categories',$data);
                    $this->db->select_max('id');
                    $data =$this->db->where('parent_id',$id)->get('categories')->row();

                    redirect('admin/course/add/'.$data->id);

                }
        }


    }
    function category_edit($id)
    {
        $data['data'] = $this->db->where('id',$id)->get('categories')->row();
        $this->load->view('admin/category_edit',$data);
    }
    function update()
    {
       $id = $this->input->post('id');
        $data = array(
            'name'=>$this->input->post('name'),
            'detail'=>$this->input->post('description'),
        );
        if(!empty($_FILES['file']['name']))
        {
            $logo = $this->upload();
            $data = array(
                'logo'=>$logo['file_name'],
            );

        }
        $this->db->where('id',$id)->update('categories',$data);
        $this->session->set_flashdata('flashSuccess','Data updated successfully');
        redirect('admin/category');
    }
    function course_edit($id)
    {
        $data['data'] = $this->db->where('id',$id)->get('categories')->row();

        $this->load->view('admin/course_edit',$data);
    }
    function course_update(){
       $id = $this->input->post('id');
        $data = array(
            'name'=>$this->input->post('name')
        );
        $this->db->where('id',$id)->update('categories',$data);
        $data = $this->db->where('id',$id)->get('categories')->row();
        $id = $data->parent_id;
        $this->session->set_flashdata('flashSuccess','Data updated successfully');
        redirect('admin/category/course/'.$id);
    }
    function upload()
    {

            $config['upload_path'] = './upload/';
            $config['allowed_types'] = '*';
            $config['max_size']	= '0';
            $config['remove_spaces'] = TRUE;
            //$config['encrypt_name']  = TRUE;
            $name=$_FILES['file']['name'];
            $config['file_name']=uniqid().$name;//to make filename unique

            $this->load->library('upload', $config);

            //$this->upload->initialize($config);
            //var_dump($_FILES);
            if ( ! $this->upload->do_upload('file'))
            {

                $error=array('error' => $this->upload->display_errors());

                die();
            }
            else
            {

                $config['image_library'] = 'gd2';
                $config['source_image'] = './upload/'.$name;
                $config['create_thumb'] = TRUE;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 75;
                $config['height'] = 50;

                $this->load->library('image_lib', $config);

                $this->image_lib->resize();
                $logo = array('file' => $this->upload->data());

                return $logo ['file'];
            }

    }
    function delete($id)
    {
        $this->db->where('id',$id)->delete('categories');
        $this->db->where('parent_id',$id)->delete('categories');
        $data = $this->db->where('parent_id',$id)->get('categories')->row();
        $cat_id = $data->id;
        $this->db->where('category_id',$id)->delete('courses');

        $this->db->where('category_id',$id)->delete('tutorials');
        $this->db->where('category_id',$id)->delete('pdf');
        $this->session->set_flashdata('flashSuccess','Category has been successfully deleted');
        redirect($_SERVER['HTTP_REFERER']);

    }
    function course($id)
    {

        $this->db->where('id',$id)->where('parent_id','0');
        $cat= $this->db->get('categories')->row();
        $data['id']=$id;
        $data['name'] = $cat->name;
        $select = $this->course_category($cat->id);
        $data['data'] = $select;
        $this->load->view('admin/course', $data);
    }

    function course_category($id)
    {
        $this->db->where('parent_id', $id);
        $data = $this->db->get('categories')->result();
        return $data;
    }
}
