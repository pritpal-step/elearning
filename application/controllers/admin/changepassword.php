<?php
  class Changepassword extends CI_Controller
  {
    function index()
    {
      $this->load->view('admin/changepassword');

    }
    function change()
    {
      $this->form_validation->set_rules('oldpassword','oldpassword', 'required');
      $this->form_validation->set_rules('newpassword','newpassword', 'required|callback_confirm_password');
      $this->form_validation->set_rules('confirmpassword','confirmpassword', 'required');
      if($this->form_validation->run() == false)
      {
          $this->form_validation->set_message('required','*this field is required');
          $this->load->view('admin/changepassword');
      }
      else
      {
        $data = array(
            'password' => $this->input->post('newpassword')
        );
        $user = $this->session->userdata('type');
        $this->db->where('username',$user)->update('admin',$data);
        $this->session->set_flashdata('flashSuccess','Password successfully changed');
        redirect('admin/changepassword');
      }

    }
    function confirm_password()
    {
        if($this->input->post('newpassword') == $this->input->post('confirmpassword'))
        {
            return true;
        }
        else{

            $this->form_validation->set_message('confirmpassword',"Please re-enter your new password");
            return false;
        }
    }

  }
?>
