<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 9/22/14
 * Time: 4:23 PM
 */
Class Course extends  CI_Controller{
    function __contruct()
    {
        parent::__construct();

        // REDIRECT IF ALREADY LOGGED IN
        if (!$this->auth->is_admin()) {
            redirect('admin/login');
        }
    }
    function index()
    {
        $data['data'] = $this->db->get('courses')->result();
        $data["links"] = "";
        $this->load->view('admin/course_info',$data);
    }
    function view($id)
    {

        $data['parent'] = $this->db->where('id',$id)->get('categories')->row();
        $data['data'] = $this->db->where('category_id',$id)->get('courses')->row();
        $this->load->view('admin/course_view',$data);
    }
    function add($id)
    {

        $count= $this->db->where('category_id',$id)->count_all_results('courses');
        if($count == 0)
        {
         $data['data']= $this->db->where('id',$id)->get('categories')->row();
         $this->load->view('admin/course_form',$data);
        }
        else{
            $this->session->set_flashdata('flashSuccess','Course Info Already Added. You can <a href="'.site_url('admin/course').'">edit</a> here');
            redirect($_SERVER['HTTP_REFERER']);
        }

    }
    function insert($id)
    {
        $category_id= $this->input->post('Category_id');
        $this->form_validation->set_rules('description','description','required');
        $this->form_validation->set_rules('features','features','required');
        $this->form_validation->set_rules('syllabus','syllabus','required');
        $this->form_validation->set_rules('number','number','required');
        $this->form_validation->set_rules('period','period','required');
        $this->form_validation->set_rules('fee','fee','required');
        if($this->form_validation->run() == false)
        {
            $this->form_validation->set_message('insert',"Field cannot be left empty");
            $data['data']= $this->db->where('id',$id)->get('categories')->row();
            $this->load->view('admin/course_form',$data);
        }
        else{
            $data = array(
                'category_id'=>$category_id,
                'name'=>$this->input->post('name'),
                'description'=>$this->input->post('description'),
                'features'=>$this->input->post('features'),
                'syllabus'=>$this->input->post('syllabus'),
                'duration'=>$this->input->post('number')." ".$this->input->post('period'),
                'start_date'=>$this->input->post('start_date'),
                'end_date'=>$this->input->post('end_date'),
                'fee'=>$this->input->post('fee')
            );
            $this->db->insert('courses',$data);
            $data = $this->db->where('id',$id)->get('categories')->row();
            $id = $data->parent_id;
            $this->session->set_flashdata('flashSuccess','Category has been successfully added');
            redirect('admin/category/course/'.$id);
            //redirect('admin/course');
        }
    }
    function course_edit($id)
    {
        $data['data'] = $this->db->where('id',$id)->get('courses')->row();
        $this->load->view('admin/course_info_edit',$data);
    }
    function update()
    {
        $d = $this->input->post('id');
        $data = array(
            'category_id'=>$this->input->post('Category_id'),
            'name'=>$this->input->post('name'),
            'description'=>$this->input->post('description'),
            'features'=>$this->input->post('features'),
            'syllabus'=>$this->input->post('syllabus'),
            'duration'=>$this->input->post('number')." ".$this->input->post('period'),
            'start_date'=>$this->input->post('start_date'),
            'end_date'=>$this->input->post('end_date'),
            'fee'=>$this->input->post('fee')
        );
        $this->db->where('id',$d)->update('courses',$data);
        $id = $this->input->post('Category_id');
        $this->session->set_flashdata('flashSuccess','Data successfully updated');
        redirect('admin/course/view/'.$id);
    }
    function delete($id)
    {
        $d = $id;
        $category_id = $this->db->where('id',$d)->get('courses')->row();
        $cat_id = $category_id->category_id;

        $a = $this->db->where('id',$cat_id)->get('categories')->row();
        $id = $a ->parent_id;

        $this->db->where('id',$d)->delete('courses');
        $this->session->set_flashdata('flashSuccess','Category has been successfully deleted');
        redirect('admin/category/course/'.$id);
    }
    function search()
    {
        $term = $this->input->post('search');
        $this->session->set_userdata('term',$term);
        $search="";
        if($this->session->userdata('term'))
        {echo "inside";
            $search= $this->session->userdata('term');
        echo $search;
        }
        else{ echo "out";
            $this->session->set_userdata('term',$term);
        }
        echo $search;

       $this->db->like('name',$search);
        $this->db->or_like('duration',$search);
        $this->db->or_like('fee',$search);
        $query = $this->db->get('courses');
        $count=$query->num_rows();
     //   $cc=array();
     //   $cc=$this->get($query);

       if($count>0)
        {   $config=array();
            $config["base_url"] = site_url().'/admin/course/search/';
            $config["total_rows"] =$count;
            $config["per_page"]=1; //limit to show no of record on page
            $config["uri_segment"] = 4;

            $this->pagination->initialize($config);
            $page = (int)$this->uri->segment($config['uri_segment']);
            $this->db->like('name',$search);
            $this->db->or_like('duration',$search);
            $this->db->or_like('fee',$search);
            $data["data"] = $this->db->get('courses',$config['per_page'],$page)->result();

            $data["links"] = $this->pagination->create_links();

            $this->load->view('admin/course_info',$data);
        }
        else
        {

        }

    }

    function get($query)
{
//    $search = $this->input->post('search');
//    $this->db->like('name',$search);
//    $this->db->or_like('duration',$search);
//    $this->db->or_like('fee',$search);
    $cc = $query->result();
    return $cc;
}
}
