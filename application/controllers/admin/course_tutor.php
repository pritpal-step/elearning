<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 10/6/14
 * Time: 10:31 AM
 */
class course_tutor extends CI_Controller{

    function __construct()
    {
        parent::__construct();

        // REDIRECT IF ALREADY LOGGED IN
        if (!$this->auth->is_admin()) {
            redirect('admin/login');
        }
    }
    function index()
    {
        $this->db->select('course_tutors.id,tutors.name as tutor_name, categories.name as cat_name');
        $this->db->join('tutors','course_tutors.tutor_id = tutors.id');
        $this->db->join('categories','course_tutors.course_id = categories.id');
        $result['data'] = $this->db->get('course_tutors')->result();
        $this->load->view('admin/course_tutor',$result);
    }
    function add($id)
    {

        $data['data'] = $this->db->where('id',$id)->get('categories')->row();
//        $data['course'] = $this->db->where('parent_id',0)->get('categories')->result();
        $data['tutor'] = $this->db->get('tutors')->result();
        $this->load->view('admin/course_tutor_add',$data);
    }
    function insert()
    {

//        $this->form_validation->set_rules('course','course', 'required');
        $this->form_validation->set_rules('tutor','tutor', 'required');
        if($this->form_validation->run() == false)
        {

            $this->session->set_flashdata('course','Please select atleast one option');
            $this->session->set_flashdata('tutor','Please select atleast one option');
            redirect('admin/course_tutor/add/');
        }
        else
        {

            $data = array(
                'category_id' => $this->input->post('category_id'),
                'tutor_id' => $this->input->post('tutor')
            );
            $id = $this->input->post('category_id');
            $this->db->insert('course_tutors',$data);
            redirect('admin/course_tutor/course/'.$id);
        }

    }
    function edit($id)
    {

        $data['selected'] = $this->db->where('id',$id)->get('course_tutors')->row();
        $this->db->select('course_tutors.id, categories.id as category_id, categories.name as cat_name, tutors.id as tutor_id , tutors.name as tutor_name');
        $this->db->join('categories','course_tutors.category_id = categories.id');
        $this->db->join('tutors','course_tutors.tutor_id = tutors.id');
        $data['data'] = $this->db->where('course_tutors.id',$id)->get('course_tutors')->row();

//        echo "<pre>";
//        print_r($data);exit();
        $data['course'] = $this->db->where('parent_id',0)->get('categories')->result();
        $data['tutor'] = $this->db->get('tutors')->result();
        $this->load->view('admin/course_tutor_edit',$data);
    }
    function update()
    {
        $id = $this->input->post('category_id');
        $data = array(
            'tutor_id' => $this->input->post('tutor'),
        );
        $this->db->where('id',$this->input->post('id'))->update('course_tutors',$data);
        redirect('admin/course_tutor/course/'.$id);
    }
    function delete($id)
    {
        $this->db->where('id',$id)->delete('course_tutors');
        $this->session->set_flashdata('flashSuccess','Category has been successfully deleted');

        redirect($_SERVER['HTTP_REFERER']);
    }
    function course($id)
    {

        $result['data'] = $this->db->select('course_tutors.id, tutors.id as tutor_id, tutors.name as tutor_name, tutors.description,  categories.id as cat_id, categories.name as cat_name')
        ->join('tutors','course_tutors.tutor_id = tutors.id')->join('categories', 'course_tutors.category_id= categories.id')->where('course_tutors.category_id',$id)
        ->get('course_tutors')->result();

        $this->load->view('admin/course_tutor', $result);
        //$this->load->view('admin/course_tutor', $data);
    }
    function course_tutorial($id)
    {

        $this->db->where('category_id', $id);
        $data = $this->db->get('course_tutors')->result();
        return $data;
    }
}
