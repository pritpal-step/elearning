<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 9/26/14
 * Time: 11:22 AM
 */
class Login extends CI_Controller
{
    function __contruct()
    {
        parent::__construct();

        // REDIRECT IF ALREADY LOGGED IN
        if ($this->auth->is_admin()) {

            redirect('admin/welcome');
        }
    }
    // LOGIN PAGE DISPLAY
    function index()
    {
        // VALIDATE LOGIN DETAILS AND VERIFY CREDENTIALS
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'required|xss_clean|callback_check_login');

        if ($this->form_validation->run() == FALSE) {
            // INVALID DATA OR NO DATA, DISPLAY FORM

            $this->load->view('admin/login');
        } else {
            // REDIRECT AFTER SUCCESSFUL LOGIN
            redirect('admin/welcome');

        }

    }

    /**
     * check_login() : Admin Login Validator, used as callback
     * @return bool
     */
    public function check_login()
    {
        // GET FORM DATA

        $type = $this->input->post('type',true);
        $username = $this->input->post('username', true);
        $password = $this->input->post('password', true);

        // CHECK CREDENTIALS VIA AUTH MODEL's CHECK_LOGIN METHOD
        if ($this->auth->check_login($username,$password,$type) == FALSE) {
            // INVALID LOGIN DETAILS
            $this->form_validation->set_message('check_login', 'Error: Invalid Login Details.');
        return false;
        } else {
            // VALID LOGIN DETAILS
        return true;
        }
    }
}