<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 10/10/14
 * Time: 2:59 PM
 */
Class Logout extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // REDIRECT IF NOT LOGGED IN
        if(!$this->auth->is_admin())
        {
            redirect('admin/login');
        }

    }
    function index()
    {
      
        $this->session->unset_userdata('type');
        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();
        redirect(site_url('admin/login'));
        //$this->auth->admin_logout();
    }
}
