<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 9/25/14
 * Time: 12:29 PM
 */
class Pdf extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // REDIRECT IF ALREADY LOGGED IN
        if (!$this->auth->is_admin()) {
            redirect('admin/login');
        }
    }

    function index()
    {

    }
    function add($id)
    {
        $data['data'] = $this->db->where('id',$id)->get('categories')->row();
        $this->load->view('admin/pdf_add',$data);
    }
    function insert()
    {
        $id = $this->input->post('category_id');
        $this->form_validation->set_rules('name','name', 'required|is_unique[pdf.name]');
        if($this->form_validation->run() == false)
        {
            if($_FILES['file']['name'] == "")
            {
                $this->form_validation->set_rules('file','file', 'required');
                if($this->form_validation->run() == false)
                {
                    $this->session->set_flashdata('flashError','Please provide file name');
                    $this->session->set_flashdata('flashError1','Please Upload file');
                    redirect('admin/pdf/add/'.$id);
                }
            }

            $this->session->set_flashdata('flashError','Please provide file name');
            redirect('admin/pdf/add/'.$id);
        }
        else if($_FILES['file']['name'] == "")
        {
            $this->form_validation->set_rules('file','file', 'required');
            if($this->form_validation->run() == false)
            {
                $this->session->set_flashdata('flashError1','Please Upload file');
                redirect('admin/pdf/add/'.$id);
            }
        }
        else{

            $logo = $this->upload();

            $data = array(
                'name'=>$this->input->post('name'),
                'category_id'=>$id ,
                'url' =>$logo['file_name'],

            );
            $this->db->insert('pdf',$data);

            $this->session->set_flashdata('flashSuccess','Category has been successfully added');
            redirect('admin/pdf/course/'.$id);
        }
    }
    function upload()
    {

        $config['upload_path'] = './pdf/';
        $config['allowed_types'] = '*';
        $config['max_size']	= '0';
        $config['remove_spaces'] = TRUE;
        //$config['encrypt_name']  = TRUE;
        echo $name=$_FILES['file']['name'];
        $config['file_name']=uniqid().$name;//to make filename unique

        $this->load->library('upload', $config);

        //$this->upload->initialize($config);
        //var_dump($_FILES);
        if ( ! $this->upload->do_upload('file'))
        {

            $error=array('error' => $this->upload->display_errors());
            print_r($error); 
            die();
        }
        else
        {
            $logo = array('file' => $this->upload->data());
            return $logo ['file'];
        }

    }
    function edit($id)
    {
        $data['data'] = $this->db->where('id',$id)->get('pdf')->row();
        $this->load->view('admin/pdf_edit',$data);
    }
    function update()
    {
        $id = $this->input->post('category_id');
        $data = array(
            'name'=>$this->input->post('name')
        );
        if(!empty($_FILES['file']['name']))
        {
            $logo = $this->upload();
            $data = array(
                'url'=>$logo['file_name'],
            );

        }
        $this->db->where('id',$this->input->post('id'))->update('pdf',$data);
        $this->session->set_flashdata('flashSuccess','Data successfully updated');
        redirect('admin/pdf/course/'.$id);
    }
    function course($id)
    {
        $data['name'] = $this->db->where('id',$id)->get('categories')->row();
        $data['id']=$id;
        $select = $this->course_tutorial($id);
        $data['data'] = $select;
        $this->load->view('admin/pdf', $data);
    }
    function course_tutorial($id)
    {
        $this->db->where('category_id', $id);
        $data = $this->db->get('pdf')->result();
        return $data;
    }
    function delete($id)
    {
        $this->db->where('id',$id)->delete('pdf');
        $this->session->set_flashdata('flashSuccess','Category has been successfully deleted');
        redirect($_SERVER['HTTP_REFERER']);
    }
}
