<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 10/16/14
 * Time: 11:19 AM
 */
class Student extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        // REDIRECT IF NOT LOGGED IN
        if(!$this->auth->is_admin())
        {
            redirect('admin/login');
        }

    }

    function index()
    {
      $data['data'] = $this->db->get('students')->result();
      $this->load->view('admin/student',$data);
    }
    function view($id)
    {

        $data['data'] = $this->db->select('students.user_id as stu_id,students.username, students.first_name as student_last, students.first_name as student_name,student_courses.name as student_course,student_courses.status,student_courses.price')
        ->join('student_courses','students.user_id = student_courses.user_id')
        ->where('students.user_id',$id)->get('students')->result();

        $data['name']= $this->db->where('user_id',$id)->get('students')->row();


        $this->load->view('admin/student_record',$data);
    }
    function edit($id)
    {
      $data['data'] = $this->db->where('user_id',$id)->get('student_courses')->row();
      $this->load->view('admin/student_status',$data);

    }
    function course()
    {
      $data['data'] = $this->db->get('student_courses')->result();
      $this->load->view('admin/student_course',$data);
    }

    function update(){

      $course=$this->db->where('category_id',$this->input->post('category_id'))->get('courses')->row();
      date('d.M.Y', strtotime('+'.$course->duration));
      $status = array(
        'status' => $this->input->post('status'),
        'start_date' => date('Y:m:d'),
        'end_date' => date('Y:m:d', strtotime('+'.$course->duration))
      );

      $this->db->where('id',$this->input->post('id'))->update('student_courses',$status);

      redirect('admin/student/view/'.$this->input->post('user_id'));

    }
    function delete($id)
    {
      $this->db->where('user_id',$id)->delete('students');
      
      redirect('admin/student');
    }
}
