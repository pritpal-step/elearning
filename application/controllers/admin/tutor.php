<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 9/25/14
 * Time: 3:25 PM
 */
class Tutor extends CI_Controller{
    function __contruct()
    {
        parent::__construct();

        // REDIRECT IF ALREADY LOGGED IN
        if (!$this->auth->is_admin()) {
            redirect('admin/login');
        }
    }
    function index()
    {
        $data['data'] = $this->db->get('tutors')->result();
        $this->load->view('admin/tutor',$data);
    }
    function add()
    {
        $this->load->view('admin/tutor_add');
    }
    function insert()
    {
        $this->form_validation->set_rules('name','name', 'required|is_unique[tutors.name]');
        $this->form_validation->set_rules('description','description', 'required');
        $this->form_validation->set_rules('email','email', 'required|valid_email|is_unique[tutors.email]');
        if($this->form_validation->run() == false)
        {
            if($_FILES['file']['name'] == "")
            {
                $this->form_validation->set_rules('file','file', 'required');
                if($this->form_validation->run() == false)
                {
                    $this->form_validation->set_message('insert',"Field cannot be left empty");
                    $this->load->view('admin/tutor_add');
                }
            }
            else{
                $this->form_validation->set_message('insert',"Field cannot be left empty");
                $this->load->view('admin/tutor_add');
            }
        }
        else if($_FILES['file']['name'] == "")
        {
            $this->form_validation->set_rules('file','file', 'required');
            if($this->form_validation->run() == false)
            {
                $this->form_validation->set_message('insert',"Field cannot be left empty");
                $this->load->view('admin/tutor_add');
            }
        }

        else{
            $logo = $this->upload();

            $data = array(
                'name'=>$this->input->post('name'),
                'description'=>$this->input->post('description'),
                'email'=>$this->input->post('email'),
                'image' =>$logo['file_name']
            );


            $this->db->insert('tutors',$data);

            $this->session->set_flashdata('flashSuccess','Category has been successfully added');
            redirect('admin/tutor');
        }
    }
    function upload()
    {

        $config['upload_path'] = './tutor/';
        $config['allowed_types'] = '*';
        $config['max_size']	= '0';
        $config['remove_spaces'] = TRUE;
        //$config['encrypt_name']  = TRUE;
        $name=$_FILES['file']['name'];
        $config['file_name']=uniqid().$name;//to make filename unique

        $this->load->library('upload', $config);

        //$this->upload->initialize($config);
        //var_dump($_FILES);
        if ( ! $this->upload->do_upload('file'))
        {

            $error=array('error' => $this->upload->display_errors());

            die();
        }
        else
        {
            $logo = array('file' => $this->upload->data());
            return $logo ['file'];
        }

    }
    function edit($id)
    {
        $data['data'] = $this->db->where('id',$id)->get('tutors')->row();
        $this->load->view('admin/tutor_edit',$data);
    }
    function update()
    {
        $id = $this->input->post('id');
        $data = array(
            'name'=>$this->input->post('name'),
            'description '=>$this->input->post('description'),
            'email'=>$this->input->post('email')
        );
        if(!empty($_FILES['file']['name']))
        {
            $logo = $this->upload();
            $data = array(
                'image'=>$logo['file_name'],
            );

        }
        $this->db->where('id',$id)->update('tutors',$data);
        $this->session->set_flashdata('flashSuccess','Data updated successfully');
        redirect('admin/tutor');
    }
    function delete($id)
    {
        $this->db->where('id',$id)->delete('tutors');
        $this->session->set_flashdata('flashSuccess','Row has been successfully deleted');
        redirect($_SERVER['HTTP_REFERER']);
    }

}