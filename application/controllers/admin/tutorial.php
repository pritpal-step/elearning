<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 9/25/14
 * Time: 12:29 PM
 */
class Tutorial extends CI_Controller
{
    function __contruct()
    {
        parent::__construct();

        // REDIRECT IF ALREADY LOGGED IN
        if (!$this->auth->is_admin()) {
            redirect('admin/login');
        }
    }
    function index()
    {

    }
    function add($id)
    {
        $data['data'] = $this->db->where('id',$id)->get('categories')->row();
        $this->load->view('admin/tutorial_add',$data);
    }
    function insert()
    {
        $id = $this->input->post('category_id');
        $this->form_validation->set_rules('name','name', 'required|is_unique[tutorials.name]');
        $this->form_validation->set_rules('url','url', 'required|is_unique[tutorials.url]');
        if($this->form_validation->run() == false)
        {
            $this->form_validation->set_message('insert',"Field cannot be left empty");
            $data['data']= $this->db->where('id',$id)->get('categories')->row();
            $this->load->view('admin/tutorial_add',$data);
        }

        else{


            $data = array(
                'name'=>$this->input->post('name'),
                'category_id'=>$id ,
                'url' =>$this->input->post('url'),

            );
            $this->db->insert('tutorials',$data);
            $this->session->set_flashdata('flashSuccess','Category has been successfully added');
            redirect('admin/tutorial/course/'.$id);
        }
    }
    function edit($id)
    {
        $data['data'] = $this->db->where('id',$id)->get('tutorials')->row();
        $this->load->view('admin/tutorial_edit',$data);
    }
    function update()
    {
        $id = $this->input->post('category_id');
        $data = array(
            'url' => $this->input->post('url'),
            'name' =>$this->input->post('name')
        );
        $this->db->where('id',$this->input->post('id'))->update('tutorials',$data);
        $this->session->set_flashdata('flashSuccess','Data successfully updated');
        redirect('admin/tutorial/course/'.$id);
    }

    function course($id)
    {
        $data['name'] = $this->db->where('id',$id)->get('categories')->row();
        //$cat= $this->db->get('categories')->row();
        $data['id']=$id;
        //$data['name'] = $cat->name;
        $select = $this->course_tutorial($id);
        $data['data'] = $select;
        $this->load->view('admin/tutorial', $data);
    }
    function course_tutorial($id)
    {
//        echo $id; exit();
        $this->db->where('category_id', $id);
        $data = $this->db->get('tutorials')->result();
        return $data;
    }
    function delete($id)
    {
        $this->db->where('id',$id)->delete('tutorials');
        $this->session->set_flashdata('flashSuccess','Category has been successfully deleted');
        redirect($_SERVER['HTTP_REFERER']);
    }
}
