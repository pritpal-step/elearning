<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 9/19/14
 * Time: 3:02 PM
 */
class Welcome extends CI_Controller{
    
    function __construct()
    {
        parent::__construct();

        // REDIRECT IF NOT LOGGED IN
        if(!$this->auth->is_admin())
        {
            redirect('admin/login');
        }

    }
    function index()
    {
         $this->load->view('admin/index');
    }
}
