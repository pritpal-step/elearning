<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 9/30/14
 * Time: 5:16 PM
 */

class Indexcontroller extends CI_Controller{

    function __construct(){
        parent::__construct();
        
      $this->load->helper('string');
        $this->load->library('email');
    }

    function index()
    {
        $data['tutor'] = $this->db->get('tutors')->result();

        $data['courses']=$this->db->where('parent_id','0')->get('categories')->result();
      //  $this->load->view('index',$data);
    }

    function description($id)
    {
        $data= $this->db->where('parent_id',$id)->get('categories')->result();
        foreach($data as $result)
        {
            $category_id =$result->id;
        }
        if(!empty($data))
        {
          $data['data'] = $this->db->where('category_id',$category_id)->get('courses')->row();
          $data['tutor'] = $this->db->select('tutors.name as tutor_name, tutors.description,tutors.image')
              ->join('tutors','course_tutors.tutor_id = tutors.id')
              ->where('course_tutors.category_id',$id)->get('course_tutors')->result();
          $data['name']= $this->db->where('id',$id)->get('categories')->row();
          $this->session->set_flashdata('no_course','');
          $this->load->view('course',$data);
        }
        else{
            $data['tutor'] = $this->db->select('tutors.name as tutor_name, tutors.description,tutors.image')
                ->join('tutors','course_tutors.tutor_id = tutors.id')
                ->where('course_tutors.category_id',$id)->get('course_tutors')->result();
            $data['name']= $this->db->where('id',$id)->get('categories')->row();
            $this->session->set_flashdata('no_course','Currently no course available');
            $this->load->view('course',$data);
      }
    }
    function sign_up()
    {
        $this->form_validation->set_rules('first_name','first name','required');
        $this->form_validation->set_rules('email','email','required|valid_email|is_unique[students.username]');
        $this->form_validation->set_rules('password','password','required');

        if($this->form_validation->run() == false)
        {
            $this->form_validation->set_message('required','*this field is required');
            $this->load->view('index');
        }
        else
        {
            $u_id= random_string('alnum',10);
            $student = $this->input->post('email');
            $data = array(
                'first_name'=>$this->input->post('first_name'),
                'last_name' =>$this->input->post('last_name'),
                'password' => $this->input->post('password'),
                'username' =>$this->input->post('email'),
                'user_id'=> $u_id
            );
            $this->db->insert('students',$data);

            $this->email->from('aulkah8ranjit@gmail.com');
            $this->email->to($student);
            $this->email->subject('success register');
            $this->email->message("you have success fully registered");
            if($this->email->send())
            {
                $this->session->set_flashdata('flashSuccess','Accout Successfully Created Please Login to Continue!!!');

            }
            else
            {
                show_error($this->email->print_debugger());
            }


            redirect(site_url());
        }
    }
    function contact_us()
    {
      $email = $this->input->post('email');
      $subject = $this->input->post('subject');
      $message = $this->input->post('message');

      $this->form_validation->set_rules('subject','Subject','required');
      $this->form_validation->set_rules('email','email','required|valid_email');
      $this->form_validation->set_rules('message','Message','required');
      if($this->form_validation->run() == false)
      {
          $this->form_validation->set_message('required','*this field is required');
          $this->load->view('index');
      }
      else
      {
          $this->email->from($email);
          $this->email->to('aulakh8ranjit@gmail.com');
          $this->email->subject($subject);
          $this->email->message($message);
          if($this->email->send())
          {
              $this->session->set_flashdata('flashSuccess','Accout Successfully Created Please Login to Continue!!!');
          }
          else
          {
            $this->email->print_debugger();
          }
          redirect(site_url());
        }
      }
}
