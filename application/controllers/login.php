<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 9/19/14
 * Time: 11:49 AM
 */
class Login extends CI_Controller
{
    function __contruct()
    {
        parent::__construct();

        // REDIRECT IF ALREADY LOGGED IN
        if ($this->auth->is_user()) {
            redirect('student/profile');
        }
    }
    // LOGIN PAGE DISPLAY

    /**
     * check_login() : Admin Login Validator, used as callback
     * @return bool
     */
    public function check_login()
    {
        // GET FORM DATA
        $type = $this->input->post('type',true);
        $username = $this->input->post('username', true);
        $password = $this->input->post('password', true);

        // CHECK CREDENTIALS VIA AUTH MODEL's CHECK_LOGIN METHOD
        if ($this->auth->check_login($username, $password,$type) == FALSE) {
            // INVALID LOGIN DETAILS
            //$this->form_validation->set_message('check_login', 'Error: Invalid Login Details.');
           echo "not matched";

        } else {
            // VALID LOGIN DETAILS
            echo "success";
        }
    }
    function forget_password()
    {
      $username = $this->input->post('username');
      $data = $this->db->where('username',$username)->get('students')->row();
      if(count($data) == 1)
      {
        $message="hello'.$data->first_name.'\nYour password is ".$data->password;
        $this->email->from('gopalkumar315@gmail.com');
        $this->email->to($username);
        $this->email->subject('Password Recover');
        $this->email->message($message);
        $this->email->send();
        echo "success";
      }
      else
      {
        echo "fail";
      }
    }
}
