<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 10/10/14
 * Time: 2:59 PM
 */
Class Logout extends CI_Controller
{
  
    function index()
    {
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('name');
        $this->session->unset_userdata('type');
        $this->session->unset_userdata('logged_in');
        redirect(site_url());

        //$this->auth->logout();
    }
}
