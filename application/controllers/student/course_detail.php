<?php
class Course_detail extends CI_Controller{
  function index()
  {

  }
  function description($id,$category_id)
  {   $user = $this->session->userdata('id');
      $student = $this->db->where('user_id',$user)->get('students')->row();
      $info = $this->db->where('parent_id',$id)->get('categories')->result();
        if(!empty($info))
        {
          if($category_id == 0)
          {

          $cat_id= $this->db->where('parent_id',$id)->get('categories')->result();
          $category_id = $cat_id[0]->id;
          $details = $this->db->where('category_id',$category_id)->get('courses')->row();

          $tutors = $this->db->select('tutors.name as tutor_name, tutors.description,tutors.image')
            ->join('tutors','course_tutors.tutor_id = tutors.id')
            ->where('course_tutors.category_id',$id)->get('course_tutors')->result();
          $courses = $this->db->where('parent_id',$id)->get('categories')->result();
          $names = $this->db->where('id',$id)->where('parent_id','0')->get('categories')->row();
          $data = array(
            'student' => $student,
            'detail' => $details,
            'tutor' => $tutors,
            'course' => $courses,
            'name' => $names
          );
          // echo "<pre>";
          // print_r($data); exit();
          $this->load->view('student/course_detail',$data);
          }
          else
          {

            $category_id;
            $details = $this->db->where('category_id',$category_id)->get('courses')->row();

            $tutors = $this->db->select('tutors.name as tutor_name, tutors.description,tutors.image')
              ->join('tutors','course_tutors.tutor_id = tutors.id')
              ->where('course_tutors.category_id',$id)->get('course_tutors')->result();
            $courses = $this->db->where('parent_id',$id)->get('categories')->result();
            $names = $this->db->where('id',$id)->where('parent_id','0')->get('categories')->row();
            $data = array(
              'student' => $student,
              'detail' => $details,
              'tutor' => $tutors,
              'course' => $courses,
              'name' => $names
            );

            $this->load->view('student/course_detail',$data);

          }

        }
        else
        {
          $this->session->set_flashdata('no_course','No Course Available');
          $tutors = $this->db->select('tutors.name as tutor_name, tutors.description,tutors.image')
            ->join('tutors','course_tutors.tutor_id = tutors.id')
            ->where('course_tutors.category_id',$id)->get('course_tutors')->result();
          $courses = $this->db->where('parent_id',$id)->get('categories')->result();
          $names = $this->db->where('id',$id)->where('parent_id','0')->get('categories')->row();
          $data = array(
            'student' => $student,
            'tutor' => $tutors,
            'course' => $courses,
            'name' => $names
          );

          $this->load->view('student/course_detail',$data);
        }

  }
}
?>
