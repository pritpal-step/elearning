<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 10/13/14
 * Time: 4:19 PM
 */

class Join extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        if(!$this->auth->is_user()){
            redirect(site_url());
        }

    }
    function index()
    {

        $this->db->where('parent_id', '0');
        $data['category'] = $this->db->get('categories')->result();
        $id = $this->session->userdata('id');
        $data['student'] = $this->db->where('user_id',$id)->get('students')->row();
        $this->load->view('student/join',$data);
    }
    function add()
    {
      $this->load->library('cart');
      $id = $this->input->post('sub_category');
      $user_id = $this->session->userdata('id');
      $data = $this->db->where('category_id',$id)->where('user_id',$user_id)
      ->get('student_courses')->result();
      
      if(count($data) > 0)
      {
        $this->session->set_flashdata('course_exist','Already enrolled this course!');
      }
      else{
        $detail = $this->db->where('category_id',$id)->get('courses')->row();
        $data = array(
            'id'=>$id,
            'qty'     => 1,
            'price'   => $detail->fee,
            'name'    => $detail->name,
        );
        $this->cart->insert($data);

      }
      redirect('student/join');
    }
    function pay()
    {
          $cart = $this->cart->contents();
          foreach($cart as $items)
          {
              $course=$this->db->where('category_id',$items['id'])->get('courses')->row();
              date('d.M.Y', strtotime('+'.$course->duration));
              $data = array(
                  'name'=> $items['name'],
                  'price'=> $items['price'],
                  'category_id' => $items['id'],
                  'status' =>'paid',
                  'start_date' => date('Y:m:d'),
                  'end_date' => date('Y:m:d', strtotime('+'.$course->duration)),
                  'user_id' => $this->session->userdata('id')
              );
              $this->db->insert('student_courses',$data);
          }

          $this->merchant->load('paypal_express');
          $settings = $this->merchant->default_settings();

          $settings = array(
                'username' => 'go.palkumar315-facilitator_api1.gmail.com',
                'password' => 'UUFURT3KQ2F753M5',
                'signature' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AEa8J5teuxGRc3bC2gHuNnM6jaqX',
                'test_mode' => true);

                $this->merchant->initialize($settings);
                $amount = $this->cart->format_number($this->cart->total());
                $amount=str_replace(',','',$amount);

                $params = array(
                'amount' =>$amount,
                'currency' => 'USD',
                'description' => 'Total Charges',
                'return_url' =>  site_url('student/join/pay'),
                'cancel_url' => site_url('student/join/cancel'));

                $response = $this->merchant->purchase_return($params);

                if($response->success())
                {
                  $id=$this->session->userdata('id');
                  $data=$this->db->where('user_id',$id)->get('students')->row();

                  $message="Your payment has been Successfully Received.
                  Your Tansaction id is :$response->reference()";
                  $this->load->library('email');
                  $this->email->from('gopalkumar315@gmail.com', 'Gopal kumar');
                  $this->email->to($data->username);
                  $this->email->subject('Elearning payment');
                  $this->email->message($message);
                  $this->email->send();

                  $message="Your have received payment with that reference that id :$response->reference()";
                  $this->load->library('email');
                  $this->email->from('gopalkumar315@gmail.com', 'Gopal kumar');
                  $this->email->to('gopalkumar315@gmail');
                  $this->email->subject('Elearning payment');
                  $this->email->message($message);
                  $this->email->send();
                }
                else{
                     $message = $response->message();
                    echo('Error processing payment: ' . $message);

                }

                $this->cart->destroy();
                $this->session->set_flashdata('flashsuccess','You can continue with your classes');
                redirect('student/profile/courses');
    }
    function payment()
    {
        $cart = $this->cart->contents();
          if(empty($cart))
          {
            $this->session->set_flashdata('empty_cart','Please select a course, the cart is empty');
            redirect('student/join');
          }
        else{

              $this->merchant->load('paypal_express');
              $settings = $this->merchant->default_settings();

              $settings = array(
              'username' => 'go.palkumar315-facilitator_api1.gmail.com',
              'password' => 'UUFURT3KQ2F753M5',
              'signature' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AEa8J5teuxGRc3bC2gHuNnM6jaqX',
              'test_mode' => true);

              $this->merchant->initialize($settings);
              $amount = $this->cart->format_number($this->cart->total());
              $amount=str_replace(',','',$amount);

              $params = array(
              'amount' =>$amount,
              'currency' => 'USD',
              'description' => 'Total Charges',
              'return_url' =>  site_url('student/join/pay'),
              'cancel_url' => site_url('student/join/cancel'));
              $response = $this->merchant->purchase($params);

      }

    }
    function cancel()
    {
      $this->session->set_flashdata('empty_cart','An error occured during transaction, please try again!');
      redirect('student/join');
    }
    function payment_return()
    {
        echo "hjfjdsgfj";
    }
    function delete_item($rowid)
    {
        $data = array(
            'rowid'   => $rowid,
            'qty'     => 0
        );

        $this->cart->update($data);

        redirect('student/join');
    }
}
