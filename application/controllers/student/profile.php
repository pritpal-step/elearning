<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 10/13/14
 * Time: 10:51 AM
 */
class Profile extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        if(!$this->auth->is_user()){
            redirect(site_url());
        }

    }
    function index()
    {
        $id = $this->session->userdata('id');
        $student = $this->db->where('user_id',$id)->get('students')->row();
        $tutors = $this->db->get('tutors')->result();
        $courses = $this->db->where('parent_id','0')->get('categories')->result();

        $data = array(
            'student' => $student,
            'tutor' => $tutors,
            'cources' => $courses
          );
        //   echo "<pre>";
        // print_r($data);exit();
        $this->load->view('student/profile',$data);

    }
    function courses()
    {
      $id = $this->session->userdata('id');

      $course = $this->db->where('user_id',$id)->where('status','paid')->get('student_courses')->result();
      $student = $this->db->where('user_id',$id)->get('students')->row();
      if(!empty($course))
      {
        $data = array(
          'courses' => $course,
          'student' => $student
        );
      }
      else
      {
        $data = array(
          'student' => $student
        );
      }

      $this->load->view('student/course_list',$data);
    }
    function description($category_id){
        $id = $this->session->userdata('id');
        $detail = $this->db->where('category_id',$category_id)->get('courses')->result();

        $tutorial = $this->db->select('pdf.url as pdf_url,pdf.name as pdf_name,tutorials.url as tutorial_url,tutorials.name as tutorial_name')
        ->join('pdf','categories.parent_id = pdf.category_id')
        ->join('tutorials','categories.parent_id = tutorials.category_id')
        ->where('categories.id',$category_id)->get('categories')->result();

        $student = $this->db->where('user_id',$id)->get('students')->row();

        $data = array(
          'detail' => $detail,
          'tutorial' =>$tutorial,
          'student' => $student
        );
        $this->load->view('student/courses',$data);
    }
    function name($category_id)
    {
      echo $category_id;
      $name= $this->db->where('id',$category_id)->get('categories')->result();
      return $name;
    }
    function change_password()
    {
        $id = $this->session->userdata('id');
        $student = $this->db->where('user_id',$id)->get('students')->row();

        $data = array(
          'student' => $student
        );

        $this->load->view('student/change_password',$data);
    }
    function update_password()
    {
        $user = $this->session->userdata('id');
        $this->form_validation->set_rules('old_password','password','required|callback_check_password');
        $this->form_validation->set_rules('new_password','new password','required|callback_confirm_password');
        $this->form_validation->set_rules('confirm_password','confirm password','required');
        if($this->form_validation->run() == false)
        {
            $this->form_validation->set_message('required','*this field is required');
            $this->load->view('student/change_password');
        }
        else
        {
            $data = array(
                'password' => $this->input->post('new_password')
            );
            $this->db->where('user_id',$user)->update('students',$data);
            $this->session->set_flashdata('flashSuccess','Password successfully changed');
            redirect('student/profile/change_password');
        }

    }
    function check_password()
    {
        $user = $this->session->userdata('id');
        $data = $this->db->where('user_id',$user)->get('students')->row();
        $password = $data->password;
        if($this->input->post('old_password') == $password)
        {
           return true;
        }
        else{

            $this->form_validation->set_message('check_password',"Invalid password detail");
            return false;
        }
    }
    function confirm_password()
    {
        if($this->input->post('new_password') == $this->input->post('confirm_password'))
        {
            return true;
        }
        else{

            $this->form_validation->set_message('confirm_password',"Please re-enter your new password");
            return false;
        }
    }

    function select()
    {
        $id = $this->input->post('id');
        if($id == "")
        {
          echo "<select name='sub_category' class='form-input' style='color:#000;' >
          <option value=''>Select Course</option>
          </select>";
        }
        else{
        $sub_category = $this->db->where('parent_id',$id)->get('categories')->result();
        echo "<select name='sub_category' class='form-input' style='color:#000;' >
        <option value=''>Select Course</option>";
        foreach($sub_category  as $sub )
        {
            echo "<option value='".$sub->id."'>".$sub->name."</option>";
        }
        echo "</select>";
      }
    }
    function contact_us()
    {
      $id = $this->session->userdata('id');
      $student = $this->db->where('user_id',$id)->get('students')->row();

      $data = array(
        'student' => $student
      );
      $this->load->view('student/contact_us',$data);
    }
}
