<?php
class Welcome extends CI_Controller
{
  function __construct()
  {
      parent::__construct();

      // REDIRECT IF NOT LOGGED IN
      if(!$this->tutor_model->is_tutor())
      {
          redirect('tutor/login');
      }

  }
  function index()
  {
    $id = $this->session->userdata('tutor_id');
    $data['tutor'] = $this->db->where('id',$id)->get('tutors')->row();

    $this->load->view('tutor/index',$data);
  }
  function student()
  {
    $tutor_id = $this->session->userdata('tutor_id');

    //get_course id of tutor
    $tutor = $this->db->where('tutor_id',$tutor_id)->get('course_tutors')->result();
    foreach($tutor as $tutors)
    {
      $tutors->category_id;
      $course = $this->db->where('parent_id',$tutors->category_id)->get('categories')->result();
      foreach($course as $courses)
      {
        $courses->id;
        $total[] = $this->db->where('category_id',$courses->id)->get('student_courses')->result();

      }

    }
    //get_course name that tutor Teach from tutor course id






    $total=count($total);

    //pagination
     $offset=$this->uri->segment(4);
     $this->load->library('pagination');
     $config['total_rows'] = $total;
     $config['base_url']=base_url().'index.php/tutor/welcome/student';
     $config['per_page'] = 15;
     $config['use_page_numbers']  = TRUE;
     $config['prev_link']='Older';
     $config['next_link']='Newer';
     $config['uri_segment']=4;

     $this->pagination->initialize($config);
     $page=$this->pagination->create_links();
     $offset=$offset>0?($offset-1)*15:$offset;


    //select a.user_id,b.first_name,b.last_name,a.name from student_courses a INNER JOIN students b on a.user_id=b.user_id where a.category_id=5
    ///get students list of tutor course
    $data = array();
    foreach($tutor as $tutors)
    {
      $tutors->category_id;
      $course = $this->db->where('parent_id',$tutors->category_id)->get('categories')->result();
      foreach($course as $courses)
      {

      $this->db->select('a.user_id,b.first_name,b.last_name,b.username,a.name,a.start_date,a.end_date,a.status');
      $this->db->from('student_courses a');
      $this->db->join('students b', 'a.user_id=b.user_id');
      $this->db->order_by('b.id','desc');
      $this->db->order_by('a.name','desc');
      $this->db->where('a.category_id',$courses->id);
      $this->db->limit($config['per_page'],$offset);
      $data['student'][] = $this->db->get()->result();
      }
    //  $data['student'] = $data1;
    }

    $data['pagination']=$this->pagination->create_links();

    $this->load->View('tutor/student',$data);

  }


  public function active_students()
  {

    $tutor_id = $this->session->userdata('tutor_id');

    $tutor = $this->db->where('tutor_id',$tutor_id)->get('course_tutors')->result();
    foreach($tutor as $tutors)
    {
      $tutors->category_id;
      $course = $this->db->where('parent_id',$tutors->category_id)->get('categories')->result();
      foreach($course as $courses)
      {

        $total[]=$this->db->where('category_id',$courses->id)->where('end_date >=',date('Y-m-d'))->get('student_courses')->result();

      }
    }


    $total=count($total);

    //pagination
     $offset=$this->uri->segment(4);
     $this->load->library('pagination');
     $config['total_rows'] = $total;
     $config['base_url']=base_url().'index.php/tutor/welcome/student_status';
     $config['per_page'] = 15;
     $config['use_page_numbers']  = TRUE;
     $config['prev_link']='Older';
     $config['next_link']='Newer';
     $config['uri_segment']=4;

     $this->pagination->initialize($config);
     $page=$this->pagination->create_links();
     $offset=$offset>0?($offset-1)*15:$offset;


    //select a.user_id,b.first_name,b.last_name,a.name from student_courses a INNER JOIN students b on a.user_id=b.user_id where a.category_id=5
    ///get students list of tutor course
    $tutor = $this->db->where('tutor_id',$tutor_id)->get('course_tutors')->result();
    foreach($tutor as $tutors)
    {
      $tutors->category_id;
      $course = $this->db->where('parent_id',$tutors->category_id)->get('categories')->result();
      foreach($course as $courses)
      {
        $this->db->select('a.status,a.user_id,b.first_name,b.last_name,b.username,a.name,a.start_date,a.end_date');
        $this->db->from('student_courses a');
        $this->db->join('students b', 'a.user_id=b.user_id');
        $this->db->order_by('b.id','desc');
        $this->db->order_by('a.name','desc');
        $this->db->where('a.category_id',$courses->id);
        $this->db->where('end_date >=',date('Y-m-d'));
        $this->db->limit($config['per_page'],$offset);
        $data['student'][] = $this->db->get()->result();

      }
    }

    $data['pagination']=$this->pagination->create_links();
    $this->load->View('tutor/student_status',$data);

  }
  function expire_course()
  {
    $tutor_id = $this->session->userdata('tutor_id');

    $tutor = $this->db->where('tutor_id',$tutor_id)->get('course_tutors')->result();
    foreach($tutor as $tutors)
    {
      $tutors->category_id;
      $course = $this->db->where('parent_id',$tutors->category_id)->get('categories')->result();
      foreach($course as $courses)
      {
        $total[]=$this->db->where('category_id',$courses->id)->where('end_date <',date('Y-m-d'))->get('student_courses')->result_array();
      }
    }
    $this->db->last_query();
    $total=count($total);

    //pagination
     $offset=$this->uri->segment(4);
     $this->load->library('pagination');
     $config['total_rows'] = $total;
     $config['base_url']=base_url().'index.php/tutor/welcome/student_status';
     $config['per_page'] = 15;
     $config['use_page_numbers']  = TRUE;
     $config['prev_link']='Older';
     $config['next_link']='Newer';
     $config['uri_segment']=4;

     $this->pagination->initialize($config);
     $page=$this->pagination->create_links();
     $offset=$offset>0?($offset-1)*15:$offset;


    //select a.user_id,b.first_name,b.last_name,a.name from student_courses a INNER JOIN students b on a.user_id=b.user_id where a.category_id=5
    ///get students list of tutor course
    $tutor = $this->db->where('tutor_id',$tutor_id)->get('course_tutors')->result();
    foreach($tutor as $tutors)
    {
      $tutors->category_id;
      $course = $this->db->where('parent_id',$tutors->category_id)->get('categories')->result();
      foreach($course as $courses)
      {
        $this->db->select('a.status,a.user_id,b.first_name,b.last_name,b.username,a.name,a.start_date,a.end_date');
        $this->db->from('student_courses a');
        $this->db->join('students b', 'a.user_id=b.user_id');
        $this->db->order_by('b.id','desc');
        $this->db->order_by('a.name','desc');
        $this->db->where('a.category_id',$courses->id);
        $this->db->where('end_date <',date('Y-m-d'));
        $this->db->limit($config['per_page'],$offset);
        $data['student'] = $this->db->get()->result();
      }
    }
    $data['pagination']=$this->pagination->create_links();
    $this->load->View('tutor/student_status',$data);
  }



}
