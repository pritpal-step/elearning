<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
			parent::__construct();

			$this->load->helper('string');
			$this->model_cron->index();
			$this->load->library('email');
		}
	public function index()
	{
				$tutors = $this->db->get('tutors')->result();
        $courses = $this->db->where('parent_id','0')->get('categories')->result();
				foreach($courses as $course_detail)
				{
					// echo $course_detail->id;
					$detail = $this->db->where('category_id',$course_detail->id)->get('courses')->row();
					// echo "<pre>";print_r($detail);
				}

				$data = array(
					'tutor' => $tutors,
					'cources' => $courses,
					'detail'=> $detail
				);

				$this->load->view('index',$data);

	}
	function description($id,$category_id)
	{		$info = $this->db->where('parent_id',$id)->get('categories')->result();
			if(!empty($info))
			{
				if($category_id == 0)
				{

				$cat_id= $this->db->where('parent_id',$id)->get('categories')->result();
				$category_id = $cat_id[0]->id;
				$details = $this->db->where('category_id',$category_id)->get('courses')->row();

				$tutors = $this->db->select('tutors.name as tutor_name, tutors.description,tutors.image')
					->join('tutors','course_tutors.tutor_id = tutors.id')
					->where('course_tutors.category_id',$id)->get('course_tutors')->result();
				$courses = $this->db->where('parent_id',$id)->get('categories')->result();
				$names = $this->db->where('id',$id)->where('parent_id','0')->get('categories')->row();
				$data = array(
					'detail' => $details,
					'tutor' => $tutors,
					'course' => $courses,
					'name' => $names
				);
				$this->load->view('course',$data);
				}
				else
				{

					$category_id;
					$details = $this->db->where('category_id',$category_id)->get('courses')->row();

					$tutors = $this->db->select('tutors.name as tutor_name, tutors.description,tutors.image')
						->join('tutors','course_tutors.tutor_id = tutors.id')
						->where('course_tutors.category_id',$id)->get('course_tutors')->result();
					$courses = $this->db->where('parent_id',$id)->get('categories')->result();
					$names = $this->db->where('id',$id)->where('parent_id','0')->get('categories')->row();
					$data = array(
						'detail' => $details,
						'tutor' => $tutors,
						'course' => $courses,
						'name' => $names
					);
					$this->load->view('course',$data);
				}

			}
			else
			{
				$this->session->set_flashdata('no_course','No Course Available');
				$tutors = $this->db->select('tutors.name as tutor_name, tutors.description,tutors.image')
					->join('tutors','course_tutors.tutor_id = tutors.id')
					->where('course_tutors.category_id',$id)->get('course_tutors')->result();
				$courses = $this->db->where('parent_id',$id)->get('categories')->result();
				$names = $this->db->where('id',$id)->where('parent_id','0')->get('categories')->row();
				$data = array(

					'tutor' => $tutors,
					'course' => $courses,
					'name' => $names
				);
				$this->load->view('course',$data);
			}


	}

	function sign_up()
	{
			$this->form_validation->set_rules('first_name','first name','required');
			$this->form_validation->set_rules('email','email','required|valid_email|is_unique[students.username]');
			$this->form_validation->set_rules('password','password','required');

			if($this->form_validation->run() == false)
			{

					$tutors = $this->db->get('tutors')->result();
					$courses = $this->db->where('parent_id','0')->get('categories')->result();

					$data = array(
						'tutor' => $tutors,
						'cources' => $courses
					);
					$this->form_validation->set_message('required','*this field is required');
					$this->load->view('index',$data);
			}
			else
			{
					$u_id= random_string('alnum',10);
					$student = $this->input->post('email');
					$data = array(
							'first_name'=>$this->input->post('first_name'),
							'last_name' =>$this->input->post('last_name'),
							'password' => $this->input->post('password'),
							'username' =>$this->input->post('email'),
							'user_id'=> $u_id
					);
					$this->db->insert('students',$data);

					$this->email->from('aulkah8ranjit@gmail.com');
					$this->email->to($student);
					$this->email->subject('success register');
					$this->email->message("you have success fully registered");

					if($this->email->send())
					{
							$this->session->set_flashdata('flashSuccess','Accout Successfully Created Please Login to Continue!');

					}
					else
					{
							show_error($this->email->print_debugger());
					}


					redirect(site_url());
			}
	}
	function contact_us()
	{
		$email = $this->input->post('username');
		$subject = $this->input->post('subject');
		$message = $this->input->post('message');

		$this->form_validation->set_rules('subject','Subject','required');
		$this->form_validation->set_rules('username','username','required|valid_email');
		$this->form_validation->set_rules('message','Message','required');
		if($this->form_validation->run() == false)

		{
				if($this->session->userdata('id'))
				{
					$id = $this->session->userdata('id');
					$student = $this->db->where('user_id',$id)->get('students')->row();

					$data = array(
							'student' => $student
						);
					$this->form_validation->set_message('required','*this field is required');

					$this->load->view('student/contact_us',$data);
				}
				else
				{
				$tutors = $this->db->get('tutors')->result();
				$courses = $this->db->where('parent_id','0')->get('categories')->result();

				$data = array(
					'tutor' => $tutors,
					'cources' => $courses
				);
				$this->form_validation->set_message('required','*this field is required');
				$this->load->view('index',$data);
			}
		}
		else
		{
				$this->email->from($email);
				$this->email->to('aulakh8ranjit@gmail.com');
				$this->email->subject($subject);
				$this->email->message($message);
				if($this->email->send())
				{
						$this->session->set_flashdata('flashcontact','Your message has been sent!');
				}
				else
				{
					//$this->email->print_debugger();
				}
				if($this->session->userdata('id'))
				{
					$id = $this->session->userdata('id');
					$student = $this->db->where('user_id',$id)->get('students')->row();

					$data = array(
							'student' => $student
						);
					$this->session->set_flashdata('flashsuccess','Your Message has been sent successfully');

					$this->load->view('student/contact_us',$data);
				}
				else
				{
					redirect(site_url());
				}

			}
		}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
