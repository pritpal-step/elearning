<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 10/10/14
 * Time: 12:09 PM
 */
class Auth extends CI_Model
{
    function is_admin()
    {
        if ($this->session->userdata('type') == 'admin') {
            return true;
        } else {
            return false;
        }
    }

    function is_user()
    {
        if ($this->session->userdata('type') == 'user') {
            return true;
        } else {
            return false;
        }
    }

    function set_auth($type)
    {
        $this->session->set_userdata('type', $type);
        $this->session->set_userdata('logged_in', true);
    }

    function check_login($username, $password, $type)
    {
        switch ($type) {
            case 'admin':
                $this->db->where('username', $username);
                $this->db->where('password', $password);
                $users = $this->db->get('admin');
                if ($users->num_rows() > 0) {
                    $this->session->unset_userdata('id');
                    $this->session->unset_userdata('name');
                    $this->session->unset_userdata('tutor_id');
                    $this->session->unset_userdata('tutor_name');
                    $this->auth->set_auth('admin');
                    return true;
                } else {
                    return false;
                }
                break;
            case 'user':
                $this->db->where('username', $username);
                $this->db->where('password', $password);
                //$this->db->where('enabled', 1);
                $users = $this->db->get('students');

                if ($users->num_rows() > 0) {
                    $user = $users->row_array();
                    $this->session->unset_userdata('tutor_id');
                    $this->session->unset_userdata('tutor_name');
                    $this->session->set_userdata('id', $user['user_id']);
                    $this->session->set_userdata('name', $user['username']);
                    $this->auth->set_auth('user');
                    return true;
                } else {
                    return false;
                }
                break;
        }
    }

    function logout()
    {
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('name');
        $this->session->unset_userdata('type');
        $this->session->unset_userdata('logged_in');
        redirect(site_url());
    }
    function admin_logout()
    {
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('name');
        $this->session->unset_userdata('type');
        $this->session->unset_userdata('logged_in');
        redirect(site_url('admin/login'));
    }
}
