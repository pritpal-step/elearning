<?php
class Model_profile extends CI_Model{
  function course_detail($id)
  {

    $info = $this->db->where('user_id',$id)->where('status','paid')->get('student_courses')->result();
    foreach($info as $info)
    {
      $info->category_id;
      $information[]= $this->db->where('category_id',$info->category_id)->get('courses')->result();

    }
    //print_r($information);
    
    return $information;

  }
  function course_tutorial($id)
  {
    $info = $this->db->where('user_id',$id)->where('status','paid')->get('student_courses')->result();
    foreach($info as $info)
    {
      $info->category_id;
      $information[] = $this->db->select('pdf.url as pdf_url,pdf.name as pdf_name,tutorials.url as tutorial_url,tutorials.name as tutorial_name')
      ->join('pdf','categories.parent_id = pdf.category_id')
      ->join('tutorials','categories.parent_id = tutorials.category_id')
      ->where('categories.id',$info->category_id)->get('categories')->result();
    }
    //print_r($information);
    return $information;
  }
  function course_tutor($id)
  {
    $info = $this->db->where('user_id',$id)->where('status','paid')->get('student_courses')->result();
    foreach($info as $info)
    {
      $category_id = $info->category_id;
      $information[] = $this->model_profile->tutors($category_id);
    }
    //print_r($information);
    return $information;
  }
  function tutors($category_id)
  {
    $tutor_id = $this->db->where('id',$category_id)->get('categories')->result();
    foreach($tutor_id as $tutor_id)
    {
      $tutor_id->parent_id;
      $data = $this->db->select('tutors.name,tutors.description,tutors.email,tutors.image')
      ->join('tutors','course_tutors.tutor_id = tutors.id')
      ->where('course_tutors.category_id',$tutor_id->parent_id)->get('course_tutors')->result();
    }
    return $data;

  }
}
