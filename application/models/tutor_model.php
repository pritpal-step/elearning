<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 10/10/14
 * Time: 12:09 PM
 */
class Tutor_model extends CI_Model
{
    function is_tutor()
    {
        if ($this->session->userdata('type') == 'tutor') {
            return true;
        } else {
            return false;
        }
    }



    function set_auth($type)
    {
        $this->session->set_userdata('type', $type);
        $this->session->set_userdata('logged_in', true);
    }

    function check_login($username, $password, $type)
    {

          $this->db->where('email', $username);
          $this->db->where('password', $password);
          $users = $this->db->get('tutors');
          if ($users->num_rows() > 0) {
          $user = $users->row_array();
          $this->session->unset_userdata('id');
          $this->session->unset_userdata('name');
          $this->session->set_userdata('tutor_id',$user['id']);
          $this->session->set_userdata('tutor_name',$user['name']);
          $this->auth->set_auth('tutor');
          return true;
          }
          else
          {
            return false;
          }
    }

    function logout()
    {
        $this->session->unset_userdata('tutor_id');
        $this->session->unset_userdata('tutor_name');
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('name');
        $this->session->unset_userdata('type');
        $this->session->unset_userdata('logged_in');
        redirect(site_url());
    }

}
