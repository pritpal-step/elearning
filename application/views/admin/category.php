<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 9/19/14
 * Time: 3:04 PM
 */
include('nav.php');
?>
<div class="pageheader">
    <h2><i class=""></i>Course Categories</h2>
    <div class="breadcrumb-wrapper">
    </div>
</div>

<div class="contentpanel">
    <?php if($this->session->flashdata('flashSuccess')):?>
        <p class='alert-success'> <?=$this->session->flashdata('flashSuccess')?> </p>
    <?php endif?>
    <div class="well" style="background-color: #F7F7F7">

           <a style="text-decoration:none" href="<?php echo site_url('admin/category/add');?>">
            <button class="btn btn-primary">
                Add Category
            </button>
            </a>
    </div>

        <table class="table embed-responsive">
            <tr>
                <th>#</th>
                <th>Course Name</th>
                <th>Description</th>
                <th>Logo</th>
                <th>Action</th>
                <th>Manage</th>
            </tr>
            <?php foreach($data as $data) { ?>
            <tr>
                <th><?php echo $data->id?></th>
                <td><?php echo $data->name?></td>
                <td><?php echo substr($data->detail,0,20)?>....</td>
                <td><img border="1" height="30" width="30" src="<?php echo base_url('upload')."/".$data->logo?>"</td>
                <td>
                    <a style="text-decoration:none" href="<?php echo site_url('admin/category/category_edit/'.$data->id)?>">
                        <button class="btn btn-primary" type="button">
                            Edit
                        </button>
                    </a>
                    <a style="text-decoration:none" href="<?php echo site_url('admin/category/delete/'.$data->id)?>">
                        <button class="btn btn-danger" type="button" onclick="return confirm('Really delete?')">
                            Delete
                        </button>
                    </a>

                </td>
                <td>
                        <a style="text-decoration:none" href="<?php echo site_url('admin/category/course/'.$data->id)?>">
                            <button class="btn btn-primary" type="button">
                               Courses
                            </button>
                        </a>
                        <a style="text-decoration:none" href="<?php echo site_url('admin/tutorial/course/'.$data->id)?>">
                            <button class="btn btn-primary" type="button">
                                Tutorials
                            </button>
                        </a>
                        <a style="text-decoration:none" href="<?php echo site_url('admin/pdf/course/'.$data->id)?>">
                            <button class="btn btn-primary" type="button">
                               Pdf
                            </button>
                        </a>

                    <a style="text-decoration:none" href="<?php echo site_url('admin/course_tutor/course/'.$data->id)?>">
                        <button class="btn btn-primary" type="button">
                            Tutors
                        </button>
                    </a>
                 </td>
            </tr>
            <?php } ?>
        </table>

      <ul class="pagination"><li><?php echo $links ?></li></ul>
      <!--<div class="dataTables_paginate paging_simple_numbers" id="table1_paginate">
        <a class="paginate_button previous disabled" aria-controls="table1" data-dt-idx="0" tabindex="0" id="table1_previous">Previous</a>
        <span>
          <a class="paginate_button current" aria-controls="table1" data-dt-idx="1" tabindex="0"><?php echo $links ?></a>

        </span>
          <a class="paginate_button next" aria-controls="table1" data-dt-idx="7" tabindex="0" id="table1_next">Next</a>
      </div>-->
</div><!-- contentpanel -->


</div><!-- mainpanel -->


</section>


<script src="<?php echo base_url()?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url()?>js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>js/modernizr.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url()?>js/toggles.min.js"></script>
<script src="<?php echo base_url()?>js/retina.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.cookies.js"></script>

<script src="<?php echo base_url()?>js/flot/flot.min.js"></script>
<script src="<?php echo base_url()?>js/flot/flot.resize.min.js"></script>
<script src="<?php echo base_url()?>js/morris.min.js"></script>
<script src="<?php echo base_url()?>js/raphael-2.1.0.min.js"></script>

<script src="<?php echo base_url()?>js/jquery.datatables.min.js"></script>
<script src="<?php echo base_url()?>js/chosen.jquery.min.js"></script>

<script src="<?php echo base_url()?>js/custom.js"></script>
<script src="<?php echo base_url()?>js/dashboard.js"></script>

</body>
</html>
