<?php
/**
 * Created by PhpStorm.
 * User: Ranjit
 * Date: 9/21/14
 * Time: 10:46 PM
 */
//echo $data->id, $data->name,"<img src='".base_url('upload/'.$data->logo)."'>";
 include('nav.php')?>
<div class="pageheader">
    <h2><i class=""></i>Edit <?php echo $data->name?></h2>
    <div class="breadcrumb-wrapper">


    </div>
</div>

<div class="contentpanel">
    <div class="well" style="background-color: #F7F7F7">
        <a href="<?php echo site_url('admin/category')?>">
            <button class="btn btn-primary ">
                Back
            </button>
        </a>
    </div>
    <div class="panel-body">
        <form method="post" enctype= "multipart/form-data"  action="<?php echo site_url('admin/category/update')?>">
            <div class="form-group">
                <label class="col-sm-4 control-label">Course Name:</label>
                <div class="col-sm-8">

                    <input type="text" class="form-control" name="name" value="<?php echo $data->name?>"/>
                    <span><?Php echo form_error('name')?></span>
                </div>
                <br><br><br>
                <label class="col-sm-4 control-label">Description<span class="asterisk">*</span></label>
                <div class="col-sm-8">
                    <textarea style="height:100px; width:625px;" name="description" id=""  ><?php echo $data->detail; ?></textarea>

                </div>
                <br><br><br>
                <label class="col-sm-4 control-label">Upload Image <span class="asterisk">*</span></label>
                <div class="col-sm-8">
                    <input type="file" name="file" class="form-control"/>
                    <span><?Php echo form_error('file')?></span>
                </div>

                    <input type="hidden" class="form-control" name="id" value="<?php echo $data->id?>">

            </div>


    </div><!-- panel-body -->
    <div class="panel-footer">
        <button type="submit" class="btn btn-primary">Update</button>
        <button class="btn btn-default" type="reset">Reset</button>
    </div><!-- panel-footer -->
    </form>

</div><!-- contentpanel -->

</div><!-- mainpanel -->


</section>


<script src="<?php echo base_url()?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url()?>js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url();?>redactor/redactor.min.js"></script>
<script type="text/javascript">
    $(document).ready(
        function()
        {
            $('.redactor').redactor({

            });
        }
    );
</script>
<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>js/modernizr.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url()?>js/toggles.min.js"></script>
<script src="<?php echo base_url()?>js/retina.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.cookies.js"></script>

<script src="<?php echo base_url()?>js/flot/flot.min.js"></script>
<script src="<?php echo base_url()?>js/flot/flot.resize.min.js"></script>
<script src="<?php echo base_url()?>js/morris.min.js"></script>
<script src="<?php echo base_url()?>js/raphael-2.1.0.min.js"></script>

<script src="<?php echo base_url()?>js/jquery.datatables.min.js"></script>
<script src="<?php echo base_url()?>js/chosen.jquery.min.js"></script>

<script src="<?php echo base_url()?>js/custom.js"></script>
<script src="<?php echo base_url()?>js/dashboard.js"></script>

</body>
</html>
