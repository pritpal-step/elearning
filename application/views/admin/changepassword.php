<?php include('nav.php')?>
<div class="pageheader">
    <h2><i class=""></i>Change your password</h2>
    <div class="breadcrumb-wrapper">


    </div>
</div>

<div class="contentpanel">
    <?php if($this->session->flashdata('flashSuccess')):?>
        <p class='alert-success'> <?=$this->session->flashdata('flashSuccess')?> </p>
    <?php endif?>
    <div class="well" style="background-color: #F7F7F7">
        <a style="text-decoration:none" href="<?php echo site_url('admin/welcome')?>">
            <button class="btn btn-primary ">
                Back
            </button>
        </a>
    </div>
    <div class="col-md-6 col-md-offset-3 ">
        <form method="post" action="<?php echo site_url('admin/changepassword/change'); ?>">

            <h4 class="nomargin">Change Password</h4>
            <p class="mt5 mb20"></p>

            <input type="text" class="form-control uname" placeholder="old Password" name="oldpassword" />
            <?php echo form_error('oldpassword')?>
            <input type="password" class="form-control pword" placeholder="New Password" name="newpassword" />
            <?php echo form_error('newpassword')?>
            <input type="password" class="form-control pword" placeholder="Confirm Password" name="confirmpassword" />
            <?php echo form_error('confirmpassword')?>
          </br>
            <input type="hidden" value="admin" class="form-control pword" placeholder="" name="type" />

            <button class="btn btn-success btn-block">Sign In</button>

        </form>

    </div>


</div><!-- contentpanel -->

</div><!-- mainpanel -->


</section>


<script src="<?php echo base_url()?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url()?>js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>js/modernizr.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url()?>js/toggles.min.js"></script>
<script src="<?php echo base_url()?>js/retina.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.cookies.js"></script>

<script src="<?php echo base_url()?>js/flot/flot.min.js"></script>
<script src="<?php echo base_url()?>js/flot/flot.resize.min.js"></script>
<script src="<?php echo base_url()?>js/morris.min.js"></script>
<script src="<?php echo base_url()?>js/raphael-2.1.0.min.js"></script>

<script src="<?php echo base_url()?>js/jquery.datatables.min.js"></script>
<script src="<?php echo base_url()?>js/chosen.jquery.min.js"></script>

<script src="<?php echo base_url()?>js/custom.js"></script>
<script src="<?php echo base_url()?>js/dashboard.js"></script>

</body>
</html>
