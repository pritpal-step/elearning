<?php
include('nav.php')?>
<div class="pageheader">
    <h2><i class=""></i>List of Course for <?php echo $name ?></h2>
    <div class="breadcrumb-wrapper">


    </div>
</div>

<div class="contentpanel">
    <?php if($this->session->flashdata('flashSuccess')):?>
        <p class='alert-success'> <?=$this->session->flashdata('flashSuccess')?> </p>
    <?php endif?>
    <div class="well" style="background-color: #F7F7F7">
        <a style="text-decoration:none" href="<?php echo site_url('admin/category')?>">
            <button class="btn btn-primary ">
                Back
            </button>
        </a>

        <a style="text-decoration:none" href="<?php echo site_url('admin/category/course_add/'.$id)?>">
            <button class="btn btn-primary ">
                Add Course
            </button>
        </a>

    </div>


        <table class="table embed-responsive">
           <tr>
                <th>Course</th>
               <th>Action</th>

           </tr>
            <?php foreach($data as $data) {?>
            <tr>
                <td>
                    <?php echo"<span style='color:#3276B1; font-size:15px; font-weight:bold'>" .$name." > </span>" . $data->name?>
                </td>
                <td>
                    <a style="text-decoration:none" href="<?php echo site_url('admin/course/view/'.$data->id)?>">
                        <button class="btn btn-primary" type="button">
                            View
                        </button>
                    </a>
                    <a style="text-decoration:none" href="<?php echo site_url('admin/category/course_edit/'.$data->id)?>">
                        <button class="btn btn-primary" type="button">
                            Edit
                        </button>
                    </a>
                    <a style="text-decoration:none" href="<?php echo site_url('admin/category/delete/'.$data->id)?>">
                        <button class="btn btn-danger" type="button" onclick="return confirm('Really delete?')">
                            Delete
                        </button>
                    </a>
                </td>
            </tr>
            <?php }?>
        </table>


</div><!-- contentpanel -->

</div><!-- mainpanel -->


</section>


<script src="<?php echo base_url()?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url()?>js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>js/modernizr.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url()?>js/toggles.min.js"></script>
<script src="<?php echo base_url()?>js/retina.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.cookies.js"></script>

<script src="<?php echo base_url()?>js/flot/flot.min.js"></script>
<script src="<?php echo base_url()?>js/flot/flot.resize.min.js"></script>
<script src="<?php echo base_url()?>js/morris.min.js"></script>
<script src="<?php echo base_url()?>js/raphael-2.1.0.min.js"></script>

<script src="<?php echo base_url()?>js/jquery.datatables.min.js"></script>
<script src="<?php echo base_url()?>js/chosen.jquery.min.js"></script>

<script src="<?php echo base_url()?>js/custom.js"></script>
<script src="<?php echo base_url()?>js/dashboard.js"></script>

</body>
</html>
