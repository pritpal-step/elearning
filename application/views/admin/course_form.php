
<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 9/22/14
 * Time: 5:15 PM
 */

include('nav.php')?>
<div class="pageheader">
    <h2><i class=""></i>Add Details TO <?php echo $data->name?></h2>
    <div class="breadcrumb-wrapper">


    </div>
</div>

<div class="contentpanel">
    <?php if($this->session->flashdata('flashSuccess')):?>
        <p class='alert-success'> <?=$this->session->flashdata('flashSuccess')?> </p>
    <?php endif?>

    <div class="panel-body">
        <form method="post" enctype= "multipart/form-data"  action="<?php echo site_url('admin/course/insert/'.$data->id)?>">
            <div class="form-group">

                <input type="hidden" class="form-control" name="Category_id" value="<?php echo $data->id?>">

                <label class="col-sm-4 control-label">Course Name:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="name" value="<?php echo $data->name?>"/>
                    <span class="alert-danger"><?Php echo form_error('name')?></span>
                </div>

                    <br><br><br>

                 <label class="col-sm-4 control-label">Duration</label>
                <div class="col-sm-8">
                    <input type="number" class="form-control" max="12" min="0" name="number" value="<?php set_value('number')?>" id="no">
                    <span class="alert-danger"><?php echo form_error('number')?></span>
                    <div class="btn-group mr5">
                        <select class="btn btn-primary dropdown-toggle" name="period" id="p" >
                            <option value="days">Select Period</option>
                            <option value="days">Days</option>
                            <option value="weeks">Weeks</option>
                            <option value="months">Months</option>
                            <option value="year">Year</option>
                        </select>
                    </div>

                    <span class="alert-danger"><?php echo form_error('period')?></span>
                </div>

                <br><br><br>

                <label class="col-sm-4 control-label">Start Date</label>
                <div class="col-sm-8">
                    <input type="date" class="form-control" name="start_date" id="sd" value="<?php echo date('Y-m-d')?>">
                </div>

                <br><br><br><br>

                <label class="col-sm-4 control-label">End Date</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="end_date" value="<?php echo date('Y-m-d')?>" onfocus="func()" id="ed">
                </div>

                <br><br><br>

                <label class="col-sm-4 control-label">Fee</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="fee" value="">
                    <span class="alert-danger"><?php echo form_error('fee')?></span>
                </div>

                <br><br><br>

                <label class="col-sm-4 control-label">Description<span class="asterisk">*</span></label>
                <div class="col-sm-8">
                    <textarea style="height:50px;"name="description" id="" class="redactor"><?php echo set_value('description'); ?></textarea>
                    <span class="alert-danger"><?php echo form_error('description')?></span>
                </div>

                <br><br><br><br><br>
                <label class="col-sm-4 control-label">Features<span class="asterisk">*</span></label>
                <div class="col-sm-8">
                    <textarea style="height:50px;"name="features" id="" class="redactor"><?php echo set_value('features'); ?></textarea>
                    <span class="alert-danger"><?php echo form_error('features')?></span>
                </div>

                <br><br><br><br><br>

                <label class="col-sm-4 control-label">Syllabus</label>
                <div class="col-sm-8">
                    <textarea name="syllabus" id="" cols="50" rows="10" class="redactor"><?php echo set_value('syllabus'); ?></textarea>
                    <span class="alert-danger"><?php echo form_error('syllabus')?></span>
                </div>
<!--                <label class="col-sm-4 control-label"></label>-->
<!--                <div class="col-sm-8">-->
<!--                    <input type="text" class="form-control" name="" value="">-->
<!--                </div>-->
            </div>


    </div><!-- panel-body -->
    <div class="panel-footer">
        <button type="submit" class="btn btn-primary">Add</button>
        <button class="btn btn-default" type="reset">Reset</button>
    </div><!-- panel-footer -->
    </form>

</div><!-- contentpanel -->

</div><!-- mainpanel -->


</section>


<script src="<?php echo base_url()?>js/jquery-1.10.2.min.js"></script>

<script src="<?php echo base_url()?>js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url();?>redactor/redactor.min.js"></script>
<script type="text/javascript">
    $(document).ready(
        function()
        {
            $('.redactor').redactor({

            });
        }
    );
</script>

<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>js/modernizr.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url()?>js/toggles.min.js"></script>
<script src="<?php echo base_url()?>js/retina.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.cookies.js"></script>

<script src="<?php echo base_url()?>js/flot/flot.min.js"></script>
<script src="<?php echo base_url()?>js/flot/flot.resize.min.js"></script>
<script src="<?php echo base_url()?>js/morris.min.js"></script>
<script src="<?php echo base_url()?>js/raphael-2.1.0.min.js"></script>

<script src="<?php echo base_url()?>js/jquery.datatables.min.js"></script>
<script src="<?php echo base_url()?>js/chosen.jquery.min.js"></script>

<script src="<?php echo base_url()?>js/custom.js"></script>
<script src="<?php echo base_url()?>js/dashboard.js"></script>

</body>
</html>
