<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 9/22/14
 * Time: 4:33 PM
 */
 include('nav.php')?>
<div class="pageheader">
    <h2><i class=""></i>List of Course Information</h2>
    <div class="breadcrumb-wrapper">


    </div>
</div>

<div class="contentpanel">
    <?php if($this->session->flashdata('flashSuccess')):?>
        <p class='alert-success'> <?=$this->session->flashdata('flashSuccess')?> </p>
    <?php endif?>
    <form action ="<?php echo site_url('admin/course/search')?>" method="post" id="searchform">
        Search the database:
        <input type="text" name="search" id="searchterm" value="<?php echo  $this->session->userdata('term');?>" />
        <input type="submit" value="Search >>" id="submit" />
    </form><?php echo $links;?>
    <table class="table embed-responsive">
        <tr>
            <th>#</th>
            <th>Name</th>
<!--            <th>Description</th>-->
<!--            <th>Syllabus</th>-->
            <th>Duration</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Fee</th>
            <th>Action</th>
        </tr>
        <?php foreach($data as $data) {?>
        <tr>
            <td><?php echo $data->id?></td>
            <td><?php echo $data->name?></td>
<!--            <td>--><?php //echo $data->description?><!--</td>-->
<!--            <td>--><?php //echo $data->syllabus?><!--</td>-->
            <td><?php echo $data->duration?></td>
            <td><?php echo date('Y-m-d',strtotime($data->start_date))?></td>
            <td><?php echo date('Y-m-d',strtotime($data->end_date))?></td>
            <td><?php echo $data->fee?></td>
            <td>
                <a style="text-decoration:none" href="<?php echo site_url('admin/course/course_edit/'.$data->id)?>">
                    <button class="btn btn-primary" type="button">
                        Edit
                    </button>
                </a>
                <a style="text-decoration:none" href="<?php echo site_url('admin/course/delete/'.$data->id)?>">
                    <button class="btn btn-danger" type="button" onclick="return confirm('Really delete?')">
                        Delete
                    </button>
                </a>
            </td>
         </tr>
        <?php }?>
    </table>

</div><!-- contentpanel -->

</div><!-- mainpanel -->


</section>


<script src="<?php echo base_url()?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url()?>js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>js/modernizr.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url()?>js/toggles.min.js"></script>
<script src="<?php echo base_url()?>js/retina.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.cookies.js"></script>

<script src="<?php echo base_url()?>js/flot/flot.min.js"></script>
<script src="<?php echo base_url()?>js/flot/flot.resize.min.js"></script>
<script src="<?php echo base_url()?>js/morris.min.js"></script>
<script src="<?php echo base_url()?>js/raphael-2.1.0.min.js"></script>

<script src="<?php echo base_url()?>js/jquery.datatables.min.js"></script>
<script src="<?php echo base_url()?>js/chosen.jquery.min.js"></script>

<script src="<?php echo base_url()?>js/custom.js"></script>
<script src="<?php echo base_url()?>js/dashboard.js"></script>

</body>
</html>
