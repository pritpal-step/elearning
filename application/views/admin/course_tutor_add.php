<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 10/6/14
 * Time: 11:06 AM
 */


include('nav.php');?>
<div class="pageheader">
    <h2><i class=""></i>Assign Tutor</h2>
    <div class="breadcrumb-wrapper">


    </div>
</div>

<div class="contentpanel">
    <?php if($this->session->flashdata('flashSuccess')):?>
        <p class='alert-success'> <?=$this->session->flashdata('flashSuccess')?> </p>
    <?php endif?>
    <div class="well" style="background-color: #F7F7F7">
        <a style="text-decoration:none" href="<?php echo site_url('admin/course_tutor/course/'.$data->id)?>">
            <button class="btn btn-primary ">
                Back
            </button>
        </a>
    </div>
    <div class="panel-body">
        <form method="post"   action="<?php echo site_url('admin/course_tutor/insert')?>">
            <div class="form-group">
                <label class="col-sm-4 control-label">Course Name:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control"  name="course" value="<?php echo $data->name ?>">
<!--                    <select name="course" class="form-control" data-placeholder="please choose a course"/>-->
<!--                    <option value="">Choose a course...</option>-->
<!--                        --><?php //foreach($course as $course) { ?>
<!---->
<!--                            <option value="--><?php //echo $course->id?><!--">--><?php //echo $course->name?><!--</option>-->
<!--                        --><?php //} ?>
<!--                    </select>-->
                    <?php if($this->session->flashdata('course')):?>
                    <span class="alert-danger"><?=$this->session->flashdata('course')?></span>
                    <?php endif?>

                </div>

                <br/><br/>

                <label class="col-sm-4 control-label">Tutor Name <span class="asterisk">*</span></label>
                <div class="col-sm-8">
                    <select name="tutor" class="form-control" data-placeholder="please choose a course"/>
                    <option value="">Choose a Tutor...</option>
                    <?php foreach($tutor as $tutor) { ?>

                        <option value="<?php echo $tutor->id?>"><?php echo $tutor->name?></option>
                    <?php } ?>
                    </select>
                    <?php if($this->session->flashdata('tutor')):?>
                    <span class="alert-danger"> <?=$this->session->flashdata('tutor')?></span>
                    <?php endif?>
                    <input type="text" name="category_id" class="form-control" value="<?php echo $data->id?>">
                </div>

            </div>


    </div><!-- panel-body -->
    <div class="panel-footer">
        <button type="submit" class="btn btn-primary">Add</button>
        <button class="btn btn-default" type="reset">Reset</button>
    </div><!-- panel-footer -->
    </form>


</div><!-- contentpanel -->

</div><!-- mainpanel -->


</section>


<script src="<?php echo base_url()?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url()?>js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>js/modernizr.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url()?>js/toggles.min.js"></script>
<script src="<?php echo base_url()?>js/retina.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.cookies.js"></script>

<script src="<?php echo base_url()?>js/flot/flot.min.js"></script>
<script src="<?php echo base_url()?>js/flot/flot.resize.min.js"></script>
<script src="<?php echo base_url()?>js/morris.min.js"></script>
<script src="<?php echo base_url()?>js/raphael-2.1.0.min.js"></script>

<script src="<?php echo base_url()?>js/jquery.datatables.min.js"></script>
<script src="<?php echo base_url()?>js/chosen.jquery.min.js"></script>

<script src="<?php echo base_url()?>js/custom.js"></script>
<script src="<?php echo base_url()?>js/dashboard.js"></script>

</body>
</html>
