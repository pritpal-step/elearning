<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 10/6/14
 * Time: 4:08 PM
 */
 include('nav.php')?>
<div class="pageheader">
    <h2><i class=""></i>Edit Tutor for <?php echo $data->cat_name; ?></h2>
    <div class="breadcrumb-wrapper">


    </div>
</div>

<div class="contentpanel">
    <?php if($this->session->flashdata('flashSuccess')):?>
        <p class='alert-success'> <?=$this->session->flashdata('flashSuccess')?> </p>
    <?php endif?>
    <div class="well" style="background-color: #F7F7F7">
        <a style="text-decoration:none" href="<?php $id = $data->category_id; echo site_url('admin/course_tutor/course/'.$id)?>">
            <button class="btn btn-primary ">
                Back
            </button>
        </a>
    </div>
    <div class="panel-body">
        <form method="post" enctype= "multipart/form-data"  action="<?php echo site_url('admin/course_tutor/update')?>">
            <div class="form-group">
                <label class="col-sm-4 control-label">Course Name:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="" value="<?php echo $data->cat_name; ?>">
                </div>
                <br/><br/>
                <label class="col-sm-4 control-label">Tutor Name</label>
                <div class="col-sm-8">
                    <select class="form-control" name="tutor">
                    <option value = <?php echo $selected->category_id?>><?php echo $data->tutor_name?></option>
                    <?php foreach($tutor as $tutor) {?>

                        <option value="<?php echo $tutor->id?>"><?php if($selected->category_id == $tutor->id){echo "";} else {echo $tutor->name;}?></option>
                    <?php } ?>
                    </select>
                </div>

                <input type="hidden" class="form-control" name="id" value="<?php echo $selected->id?>">
                <input type="hidden" class="form-control" name="category_id" value="<?php echo $selected->category_id?>">

            </div>


    </div><!-- panel-body -->
    <div class="panel-footer">
        <button type="submit" class="btn btn-primary">Update</button>
        <button class="btn btn-default" type="reset">Reset</button>
    </div><!-- panel-footer -->
    </form>


</div><!-- contentpanel -->

</div><!-- mainpanel -->


</section>


<script src="<?php echo base_url()?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url()?>js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>js/modernizr.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url()?>js/toggles.min.js"></script>
<script src="<?php echo base_url()?>js/retina.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.cookies.js"></script>

<script src="<?php echo base_url()?>js/flot/flot.min.js"></script>
<script src="<?php echo base_url()?>js/flot/flot.resize.min.js"></script>
<script src="<?php echo base_url()?>js/morris.min.js"></script>
<script src="<?php echo base_url()?>js/raphael-2.1.0.min.js"></script>

<script src="<?php echo base_url()?>js/jquery.datatables.min.js"></script>
<script src="<?php echo base_url()?>js/chosen.jquery.min.js"></script>

<script src="<?php echo base_url()?>js/custom.js"></script>
<script src="<?php echo base_url()?>js/dashboard.js"></script>

</body>
</html>
