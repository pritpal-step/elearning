<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 10/7/14
 * Time: 4:13 PM
 */

include('nav.php')?>
<div class="pageheader">
    <h2><i class=""></i>Detail of <?php echo $data->name?> course</h2>
    <div class="breadcrumb-wrapper">


    </div>
</div>

<div class="contentpanel">
    <?php if($this->session->flashdata('flashSuccess')):?>
        <p class='alert-success'> <?=$this->session->flashdata('flashSuccess')?> </p>
    <?php endif?>
    <div class="well" style="background-color: #F7F7F7">
        <a style="text-decoration:none" href="<?php $id = $parent->parent_id; echo site_url('admin/category/course/'.$id)?>">
            <button class="btn btn-primary ">
                Back
            </button>
        </a>
        <a style="text-decoration:none" href="<?php echo site_url('admin/course/course_edit/'.$data->id)?>">
            <button class="btn btn-primary" type="button">
                Edit
            </button>
        </a>
<!--        <a style="text-decoration:none" href="--><?php //echo site_url('admin/course/delete/'.$data->id)?><!--">-->
<!--            <button class="btn btn-danger" type="button" onclick="return confirm('Really delete?')">-->
<!--                Delete-->
<!--            </button>-->
<!--        </a>-->
    </div>
    <table class="table table-responsive">
         <tr>
            <th>#</th>
            <td><?php echo $data->id?></td>
         </tr>
         <tr>
             <th>Name</th>
             <td><?php echo $data->name?></td>
         </tr>
         <tr>
             <th>Description</th>
             <td><?php echo $data->description?></td>
         </tr>
         <tr>
             <th>Syllabus</th>
             <td><?php echo $data->syllabus?></td>
         </tr>
         <tr>
             <th>Duration</th>
             <td><?php echo $data->duration?></td>
         </tr>
         <tr>
             <th>Start Date</th>
             <td><?php echo $data->start_date?></td>
         </tr>
        <tr>
            <th>End Date</th>
            <td><?php echo $data->end_date?></td>
        </tr>
        <tr>
            <th>Fee</th>
            <td><?php echo $data->fee?></td>
        </tr>
    </table>


</div><!-- contentpanel -->

</div><!-- mainpanel -->


</section>


<script src="<?php echo base_url()?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url()?>js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>js/modernizr.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url()?>js/toggles.min.js"></script>
<script src="<?php echo base_url()?>js/retina.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.cookies.js"></script>

<script src="<?php echo base_url()?>js/flot/flot.min.js"></script>
<script src="<?php echo base_url()?>js/flot/flot.resize.min.js"></script>
<script src="<?php echo base_url()?>js/morris.min.js"></script>
<script src="<?php echo base_url()?>js/raphael-2.1.0.min.js"></script>

<script src="<?php echo base_url()?>js/jquery.datatables.min.js"></script>
<script src="<?php echo base_url()?>js/chosen.jquery.min.js"></script>

<script src="<?php echo base_url()?>js/custom.js"></script>
<script src="<?php echo base_url()?>js/dashboard.js"></script>

</body>
</html>
