<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/favicon.png" type="image/png">

    <title>Web Training'z</title>


    <link href="<?php echo base_url();?>css/style.default.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>-->
    <script src="<?php echo base_url();?>js/html5shiv.js"></script>
    <script src="<?php echo base_url();?>js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="signin">


<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>

    <div class="signinpanel">

        <div class="row">

            <div class="col-md-7">

                <div class="signin-info">
                    <br><br><br><br><br>
                    <div class="logopanel">
                        <h1><span>[</span> E-Learning <span>]</span></h1>
                    </div><!-- logopanel -->

                    <div class="mb20"></div>

                    <h5><strong>Welcome to E-Learning Admin Pannel</strong></h5>

                    <div class="mb20"></div>

                </div><!-- signin0-info -->

            </div><!-- col-sm-7 -->

            <div class="col-md-5">
                <form method="post" action="<?php echo site_url('admin/login'); ?>">

                    <h4 class="nomargin">Sign In</h4>
                    <p class="mt5 mb20">Login to access your account.</p>

                    <input type="text" class="form-control uname" placeholder="Username" name="username" />
                    <?php echo form_error('username')?>
                    <input type="password" class="form-control pword" placeholder="Password" name="password" />
                    <?php echo form_error('password')?>
                    <input type="hidden" value="admin" class="form-control pword" placeholder="" name="type" />
                    <a href="#"><small>Forgot Your Password?</small></a>
                    <button class="btn btn-success btn-block">Sign In</button>

                </form>

            </div><!-- col-sm-5 -->

        </div><!-- row -->

        <div class="signup-footer">
            <div class="pull-left">
                &copy; 2014. All Rights Reserved. Step Consultancy.
            </div>
            <div class="pull-right">
                <!--Created By: <a href="http://themepixels.com/" target="_blank">ThemePixels</a>->
            </div>
        </div>

    </div><!-- signin -->

</section>

<script src="<?php echo base_url();?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url();?>js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>js/modernizr.min.js"></script>
<script src="<?php echo base_url();?>js/retina.min.js"></script>
<script src="<?php echo base_url();?>js/custom.js"></script>


</body>
</html>
