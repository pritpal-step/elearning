<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">


    <link rel="shortcut icon" sizes="32x32" href="<?php echo base_url('images/favicon.png')?>">
    <title>Web Training'z</title>
    <link href="<?php echo base_url()?>css/style.default.css" rel="stylesheet">
    <link href="<?php echo base_url()?>css/jquery.datatables.css" rel="stylesheet">
    <link href="<?php echo base_url()?>redactor/redactor.css" rel="stylesheet">



</head>

<body>

<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>
<div class="leftpanel">

    <div class="logopanel">
        <h1><span>[</span> Web Training'z<span>]</span></h1>
    </div><!-- logopanel -->

    <div class="leftpanelinner">

        <!-- This is only visible to small devices -->
        <div class="visible-xs hidden-sm hidden-md hidden-lg">



            <ul class="nav nav-pills nav-stacked nav-bracket mb30">
                <li><a href="profile.html"><i class="fa fa-user"></i> <span>Profile</span></a></li>
                <li><a href="#"><i class="fa fa-cog"></i> <span>Account Settings</span></a></li>
                <li><a href="#"><i class="fa fa-question-circle"></i> <span>Help</span></a></li>
                <li><a href="signout.html"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
            </ul>
        </div>

        <h5 class="sidebartitle">Navigation</h5>
        <ul class="nav nav-pills nav-stacked nav-bracket">
            <li class="active"><a href="<?php echo site_url('admin/welcome')?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
            <li><a href="<?php echo site_url('admin/category')?>"><i class="fa  fa-pencil-square-o"></i> <span>Category</span></a></li>
<!--            <li><a href="--><?php //echo site_url('admin/course')?><!--"><i class="fa  fa-pencil-square-o"></i> <span>Course Information</span></a></li>-->
            <li><a href="<?php echo site_url('admin/tutor')?>"><i class="fa  fa-pencil-square-o"></i> <span>Tutors</span></a></li>
            <li><a href="<?php echo site_url('admin/student')?>"><i class="fa  fa-pencil-square-o"></i> <span>Students</span></a></li>


        </ul>
</div><!-- leftpanelinner -->
</div><!-- leftpanel -->
<div class="mainpanel">

    <div class="headerbar">

        <a class="menutoggle"><i class="fa fa-bars"></i></a>
        <div class="header-right">
            <ul class="headermenu">
                <div class="btn-group">
                    <div class="dropdown-menu dropdown-menu-head pull-right">
                    </div>
                </div>

                <li>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-lock"></i>
                            Admin
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                            <li><a href="<?php echo site_url('admin/changepassword')?>"><i class="glyphicon glyphicon-user"></i> Change Password</a></li>
                            <li><a href="<?php echo site_url('admin/logout')?>"><i class="glyphicon glyphicon-log-out"></i> Log Out</a></li>
                        </ul>
                    </div>
                </li>

            </ul>
        </div><!-- header-right -->
     </div><!-- headerbar -->
