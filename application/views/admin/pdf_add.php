<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 9/19/14
 * Time: 3:20 PM
 */

include('nav.php')?>
<div class="pageheader">
    <h2><i class=""></i>Add Pdf To <?php echo $data->name?></h2>
    <div class="breadcrumb-wrapper">
    </div>
</div>

<div class="contentpanel">
    <div class="well" style="background-color: #F7F7F7">
        <a style="text-decoration:none" href="<?php echo site_url('admin/pdf/course/'.$data->id)?>">
            <button class="btn btn-primary ">
                Back
            </button>
        </a>
    </div>
    <div class="panel-body">
        <form method="post" enctype= "multipart/form-data" action="<?php echo site_url('admin/pdf/insert')?>">
            <div class="form-group">

                <label class="col-sm-4 control-label">Upload Pdf </label>
                <div class="col-sm-8">
                    <input type="file" name="file" class="form-control"/>
                    <span class="alert-danger"><?php if($this->session->flashdata('flashError1')):?>
                            <?=$this->session->flashdata('flashError1')?>
                        <?php endif?></span>
                </div>

                <br><br><br>

                <label class="col-sm-4 control-label">Description<span class="asterisk">*</span></label>
                <div class="col-sm-8">

                    <textarea style="height:50px;"name="name" id="" class="redactor"><?php echo set_value('name'); ?></textarea>
                    <span class="alert-danger"><?php if($this->session->flashdata('flashError')):?>
                            <?=$this->session->flashdata('flashError')?>
                        <?php endif?></span>
                </div>
                <input type="hidden" class="form-control" name="category_id" value="<?php echo $data->id?>">

            </div>


    </div><!-- panel-body -->
    <div class="panel-footer">
        <button type="submit" class="btn btn-primary">Add</button>
        <button class="btn btn-default" type="reset">Reset</button>
    </div><!-- panel-footer -->
    </form>

</div><!-- contentpanel -->

</div><!-- mainpanel -->


</section>


<script src="<?php echo base_url()?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url()?>js/jquery-migrate-1.2.1.min.js"></script>

<script src="<?php echo base_url();?>redactor/redactor.min.js"></script>
<script type="text/javascript">
    $(document).ready(
        function()
        {
            $('.redactor').redactor({

            });
        }
    );
</script>
<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>js/modernizr.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url()?>js/toggles.min.js"></script>
<script src="<?php echo base_url()?>js/retina.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.cookies.js"></script>

<script src="<?php echo base_url()?>js/flot/flot.min.js"></script>
<script src="<?php echo base_url()?>js/flot/flot.resize.min.js"></script>
<script src="<?php echo base_url()?>js/morris.min.js"></script>
<script src="<?php echo base_url()?>js/raphael-2.1.0.min.js"></script>

<script src="<?php echo base_url()?>js/jquery.datatables.min.js"></script>
<script src="<?php echo base_url()?>js/chosen.jquery.min.js"></script>

<script src="<?php echo base_url()?>js/custom.js"></script>
<script src="<?php echo base_url()?>js/dashboard.js"></script>

</body>
</html>
