<?php include('nav.php')?>
<div class="pageheader">
    <h2><i class=""></i>Students Record</h2>
    <div class="breadcrumb-wrapper">


    </div>
</div>


<div class="contentpanel">
    <?php if($this->session->flashdata('flashSuccess')):?>
        <p class='alert-success'> <?=$this->session->flashdata('flashSuccess')?> </p>
    <?php endif?>

    <table class="table table-responsive">
        <tr>

            <th>#</th>
            <th>Name</th>
            <th>Username</th>
            <th>Password</th>
            <th>action</th>
        </tr>
        <?php foreach($data as $data) {?>
        <tr>
            <td><?php echo $data->id ?></td>
            <td><?php echo $data->first_name." ".$data->last_name ?></td>
            <td><?php echo $data->username ?> </td>
            <td><?php echo $data->password?></td>

            <td>
              <a style="text-decoration:none" href="<?php echo site_url('admin/student/view')."/".$id = $data->user_id?>"><button class="btn btn-primary" type="button">View</button></a>
              <!-- <a style="text-decoration:none" href="<?php echo site_url('admin/student/delete')."/".$id = $data->user_id?>"><button class="btn btn-danger" onclick="return confirm('Really delete?')" type="button">Delete</button></a> -->
            </td>
        </tr>
        <?php } ?>
    </table>

</div><!-- contentpanel -->

</div><!-- mainpanel -->


</section>


<script src="<?php echo base_url()?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url()?>js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>js/modernizr.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url()?>js/toggles.min.js"></script>
<script src="<?php echo base_url()?>js/retina.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.cookies.js"></script>

<script src="<?php echo base_url()?>js/flot/flot.min.js"></script>
<script src="<?php echo base_url()?>js/flot/flot.resize.min.js"></script>
<script src="<?php echo base_url()?>js/morris.min.js"></script>
<script src="<?php echo base_url()?>js/raphael-2.1.0.min.js"></script>

<script src="<?php echo base_url()?>js/jquery.datatables.min.js"></script>
<script src="<?php echo base_url()?>js/chosen.jquery.min.js"></script>

<script src="<?php echo base_url()?>js/custom.js"></script>
<script src="<?php echo base_url()?>js/dashboard.js"></script>

</body>
</html>
