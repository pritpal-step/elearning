<?php include('nav.php')?>
<div class="pageheader">
    <h2><i class=""></i><?php echo $name->first_name." ". $name->last_name?></h2>
    <div class="breadcrumb-wrapper">


    </div>
</div>


<div class="contentpanel">
    <?php if($this->session->flashdata('flashSuccess')):?>
        <p class='alert-success'> <?=$this->session->flashdata('flashSuccess')?> </p>
    <?php endif?>
    <div class="well" style="background-color: #F7F7F7">

           <a style="text-decoration:none" href="<?php echo site_url('admin/student');?>">
            <button class="btn btn-primary">
                Go Back
            </button>
            </a>
    </div>
<?php if(!empty($data)) {?>
    <table class="table table-responsive">
        <tr>
            <th>Username</th>
            <th>Persuing Course</th>
            <th>Fee</th>
            <th>Satus</th>
            <th>action</th>
        </tr>
        <?php foreach($data as $data) {?>
        <tr>
            <td><?php echo $data->username ?> </td>
            <td><?php echo $data->student_course ?></td>
            <td><?php echo $data->price ?></td>
            <td><?php echo ucfirst($data->status) ?></td>
            <td>
                <a style="text-decoration:none" href="<?php echo site_url('admin/student/edit')."/".$id = $data->stu_id?>"><button class="btn btn-primary" type="button">Update</button></a>
                <!-- <a style="text-decoration:none" href="<?php //echo site_url('admin/student/delete')."/".$id = $data->stu_id?>"><button class="btn btn-danger" type="button">Delete</button></a> -->
            </td>
        </tr>
        <?php } ?>
    </table>
  <?php } else { ?>
    <h3> No record found <!--regarding--><?php //echo $name->first_name." ". $name->last_name?></h3>
  <?php } ?>
</div><!-- contentpanel -->
</div><!-- mainpanel -->


</section>


<script src="<?php echo base_url()?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url()?>js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>js/modernizr.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url()?>js/toggles.min.js"></script>
<script src="<?php echo base_url()?>js/retina.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.cookies.js"></script>

<script src="<?php echo base_url()?>js/flot/flot.min.js"></script>
<script src="<?php echo base_url()?>js/flot/flot.resize.min.js"></script>
<script src="<?php echo base_url()?>js/morris.min.js"></script>
<script src="<?php echo base_url()?>js/raphael-2.1.0.min.js"></script>

<script src="<?php echo base_url()?>js/jquery.datatables.min.js"></script>
<script src="<?php echo base_url()?>js/chosen.jquery.min.js"></script>

<script src="<?php echo base_url()?>js/custom.js"></script>
<script src="<?php echo base_url()?>js/dashboard.js"></script>

</body>
</html>
