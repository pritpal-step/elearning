<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 9/29/14
 * Time: 1:12 PM
 */         include('nav.php')?>
<div class="pageheader">
    <h2><i class=""></i>Add Tutor</h2>
    <div class="breadcrumb-wrapper">


    </div>
</div>

<div class="contentpanel">
    <?php if($this->session->flashdata('flashSuccess')):?>
        <p class='alert-success'> <?=$this->session->flashdata('flashSuccess')?> </p>
    <?php endif?>
    <div class="well" style="background-color: #F7F7F7">
        <a style="text-decoration:none" href="<?php echo site_url('admin/tutor')?>">
            <button class="btn btn-primary ">
                Back
            </button>
        </a>
    </div>
    <div class="panel-body">
        <form method="post" enctype= "multipart/form-data"  action="<?php echo site_url('admin/tutor/insert')?>">
            <div class="form-group">

                <label class="col-sm-4 control-label">Tutor Name:</label>
                <div class="col-sm-8">
                <input type="text" class="form-control" name="name" value="<?php echo set_value('name')?>"/>
                    <span class="alert-danger"><?Php echo form_error('name')?></span>
                </div>

                <br/><br/>

                <label class="col-sm-4 control-label">Description</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="description" value="<?php echo set_value('description')?>"/>
                    <span class="alert-danger"><?Php echo form_error('description')?></span>
                </div>

                <br/><br/>

                <label class="col-sm-4 control-label">Email</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="email" value="<?php echo set_value('email')?>"/>
                    <span class="alert-danger"><?Php echo form_error('email')?></span>
                </div>

                <br/><br/>

                <label class="col-sm-4 control-label">Upload Image <span class="asterisk">*</span></label>
                <div class="col-sm-8">
                    <input type="file" name="file" class="form-control"/>
                    <span class="alert-danger"><?Php echo form_error('file')?></span>
                </div>

            </div>


    </div><!-- panel-body -->
    <div class="panel-footer">
        <button type="submit" class="btn btn-primary">Add</button>
        <button class="btn btn-default" type="reset">Reset</button>
    </div><!-- panel-footer -->
    </form>

</div><!-- contentpanel -->

</div><!-- mainpanel -->


</section>


<script src="<?php echo base_url()?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url()?>js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>js/modernizr.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url()?>js/toggles.min.js"></script>
<script src="<?php echo base_url()?>js/retina.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.cookies.js"></script>

<script src="<?php echo base_url()?>js/flot/flot.min.js"></script>
<script src="<?php echo base_url()?>js/flot/flot.resize.min.js"></script>
<script src="<?php echo base_url()?>js/morris.min.js"></script>
<script src="<?php echo base_url()?>js/raphael-2.1.0.min.js"></script>

<script src="<?php echo base_url()?>js/jquery.datatables.min.js"></script>
<script src="<?php echo base_url()?>js/chosen.jquery.min.js"></script>

<script src="<?php echo base_url()?>js/custom.js"></script>
<script src="<?php echo base_url()?>js/dashboard.js"></script>

</body>
</html>
