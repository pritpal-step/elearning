<html>
<head>
	<title></title>

	<link rel="stylesheet" type="text/css" href="<?Php echo base_url()?>css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/style.css">
</head>
<body>

	<div class="courses-detail-wrapper">
		<div class="row-fluid">
			<div class="col-md-12">
				<div class="courses-nav">
					<ul>
						<li class="nav-active"><a href="<?php echo site_url()?>/benefit">Benifits</a></li>
						<li><a href="<?php echo site_url()?>/syllabus">Syllabus</a></li>
						<li><a href="<?php echo site_url()?>/tutor">Tutor Info</a></li>
						<li><a href="<?php echo site_url()?>/feedback">Feedback</a></li>
						<li><a href="<?php echo site_url()?>/join_course">Join Course</a></li>
					</ul>
					<div class="clear"></div>
				</div>
			</div>
		</div>

		<div class="row-fluid">
			<div class="col-md-12">
				<div class="courses-detail-txt">
					<h1>Benifits of Courses</h1>
					<p>Increasing the competency and effectiveness of employees is usually the driving factor behind a company's decision to either offer or require professional development. But these programs can also boost morale by positioning participants to advance their careers through acquiring new skills or gaining insight into an area of the company they might be unfamiliar with. And being selected to travel to a conference can make an employee feel special or rewarded for his or her hard work.</p>
				</div>
			</div>
		</div>
	</div>

	<script src="<?php echo base_url()?>js/jquery.min.js"></script>
	<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
</body>
</html>