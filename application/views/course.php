

<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/bootstrap.min.css')?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/style.css')?>">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="<?php echo base_url('js/live.js')?>"></script>
</head>
<body>

    <div class="row-fluid course_header" >
        <div class="container c_container">
               <div class="row-fluid">
                   <div class="col-md-6">
                       <h3><?php echo $name->name ?> Training Courses</h3>

                    </div>
										<div class="signin">
											<?php if($this->session->userdata("id")) {?>
												<ul>

													<li class="dropdown" style="list-style-type: none">
														<a class="dropdown-toggle" data-toggle="dropdown" href=""><button class="login-button">My Account</button></a>
														<ul class="dropdown-menu">
															<li><a href="<?php echo site_url('student/profile')?>">Profile</a></li>
																<li><a href="<?php echo site_url('student/profile/change_password')?>">Change Password</a></li>
																<li><a href="<?php echo site_url('logout') ?>">Logout</a></li>
														</ul>
												</li>
											</ul>

												<nav role="navigation" class="collapse navbar-collapse bs-navbar-collapse">
														<ul class="navbar-nav nav">
																<li><a style="color:#FFFFFF" href="<?php echo site_url('')?>">Home</a></li>
																<li><a style="color:#FFFFFF"href="<?php echo site_url('student/join')?>">Join Course</a></li>
																<li><a style="color:#FFFFFF" href="<?php echo site_url('student/profile/courses')?>">My Course</a></li>
														</ul>
												</nav>

											<?php } else{?>
												<ul>
													<li style="list-style-type: none"><a  href="<?php echo site_url('')?>"><button class="login-button">Home</button></a></li>
												</ul>
											<?php }?>
										</div>
								</div>
        </div>
    </div>


    <div class="container content_container">
        <div class="row-fluid">

            <div class="col-md-3 well">
							<?php if(empty($course)){
									if($this->session->flashdata('no_course')){?>
											<p class='alert alert-success' style="color:#000;"> <?=$this->session->flashdata('no_course')?> </p>
									<?php }
							} else {?>
                <div class="testimonial_title text-center">

										<table class="table table-responsive">
											<tr>
													<th>Name</th>
													<th>Action</th>
												</tr>
												<?php foreach($course as $course) {?>
												<tr>
														<td style="color:black"><?php echo ucfirst($course->name) ?></td>
														<td>
															<a href="<?php echo site_url('welcome/description')."/".$id=$name->id."/".$category_id=$course->id ?>">
																<button class="btn btn-primary" type="button">
																		View
																</button>
															</a>
														</td>
												</tr>
														<?php } ?>
														</table>
											<?php	} ?>

                </div>
            </div>

            <div class="col-md-7 col-md-offset-1 well">

                <div class="row-fluid">
                    <div class="col-md-12">
                        <div class="courses-nav">
                            <ul role="tablist" id="myTab">
                                <li class="nav-active"><a href="#home" role="tab" data-toggle="tab">About Course</a></li>
																<li><a href="#features" role="tab" data-toggle="tab">Features</a></li>
                                <li><a href="#syllabus" role="tab" data-toggle="tab">Syllabus</a></li>
                                <li><a href="#tutor" role="tab" data-toggle="tab">tutor</a></li>
                                <li><a href="#messages" role="tab" data-toggle="tab">FAQ</a></li>

                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>



                <div class="tab-content">
                    <div class="tab-pane active" id="home">
                        <div class="row-fluid">
                            <div class="col-md-12">
                                <div class="courses-detail-txt">
                                    <h3><?php echo $name->name ?></h3>
                                    <?php if(!empty($detail)){echo $detail->description;} else{ echo "";} ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="features">
                        <div class="row-fluid">
                            <div class="col-md-12">
                                <div class="courses-detail-txt">
                                    <h3><?php echo $name->name?> Features</h3>
                                    <p><?php if(!empty($detail)){echo $detail->features;} else{ echo "";}?></p>
                                </div>
                            </div>
                        </div>
                    </div>
										<div class="tab-pane" id="syllabus">
												<div class="row-fluid">
														<div class="col-md-12">
																<div class="courses-detail-txt">
																		<h3><?php echo $name->name?> Syllabus</h3>
																		<p><?php if(!empty($detail)){echo $detail->syllabus;} else{ echo "";}?></p>
																</div>
														</div>
												</div>
										</div>
                    <div class="tab-pane" id="tutor">
                        <div class="row-fluid">
                            <div class="col-md-12">
                                <div class="courses-detail-txt">
                                    <h1><?php echo $name->name?> Tutors</h1>
                                    <?php foreach($tutor as $tutor)
                                    {
                                      echo "<img height:'50' width='50' src='".base_url('tutor')."/".$tutor->image."'><b> " .$tutor->tutor_name . " : </b>" . $tutor->description;
                                      echo "<br><br>";
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="messages">
                        <div class="row-fluid">
                            <div class="col-md-12">
                                <div class="courses-detail-txt">
                                    <h1>This is message page</h1>
                                    <p>Increasing the competency and effectiveness of employees is usually the driving factor behind a company's decision to either offer or require professional development. But these programs can also boost morale by positioning participants to advance their careers through acquiring new skills or gaining insight into an area of the company they might be unfamiliar with. And being selected to travel to a conference can make an employee feel special or rewarded for his or her hard work.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        </div>
    </div>

	<script src="<?php echo base_url('js/jquery.min.js')?>"></script>
	<script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>
</body>
</html>
