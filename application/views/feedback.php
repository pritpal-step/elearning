<html>
<head>
	<title></title>

    <link rel="stylesheet" type="text/css" href="<?Php echo base_url()?>css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/style.css">
</head>
<body>

	<div class="courses-detail-wrapper">
		<div class="row-fluid">
			<div class="col-md-12">
				<div class="courses-nav">
					<ul>
                        <li class="nav-active"><a href="<?php echo site_url()?>/benefit">Benifits</a></li>
                        <li><a href="<?php echo site_url()?>/syllabus">Syllabus</a></li>
                        <li><a href="<?php echo site_url()?>/tutor">Tutor Info</a></li>
                        <li><a href="<?php echo site_url()?>/feedback">Feedback</a></li>
                        <li><a href="<?php echo site_url()?>/join_course">Join Course</a></li>
					</ul>
					<div class="clear"></div>
				</div>
			</div>
		</div>

		<div class="row-fluid">
			<div class="col-md-12">
				<div class="courses-detail-txt">
					<h1>Feedback</h1>
				</div>
			</div>
		</div>

		
		<div class="row-fluid">
			<div class="col-sm-2">
				<div class="student-profile">
					<img src="<?Php echo base_url()?>images/profile.png">
				</div>
			</div>
			<div class="col-sm-10">
				<div class="student-comment">
					<b>Tanvir Singh</b><br>
					Very good experience ,good work environment.
				</div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="row-fluid">
			<div class="col-sm-2">
				<div class="student-profile">
					<img src="<?Php echo base_url()?>images/profile.png">
				</div>
			</div>
			<div class="col-sm-10">
				<div class="student-comment">
					<b>Amit Kumar</b><br>
					Excellent training, leadership, growth opportunities, supportive colleagues, Good working environment.
				</div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="row-fluid">
			<div class="col-sm-2">
				<div class="student-profile">
					<img src="<?Php echo base_url()?>images/profile.png">
				</div>
			</div>
			<div class="col-sm-10">
				<div class="student-comment">
					<b>Ravinder Kumar</b><br>
					Perfect Training of Web Development.. I am fully satisfied with the training provided by STEP Professional Experts.
				</div>
			</div>
			<div class="clear"></div>
		</div>




	</div>

	<script src="<?Php echo base_url()?>js/jquery.min.js"></script>
	<script src="<?Php echo base_url()?>js/bootstrap.min.js"></script>
</body>
</html>