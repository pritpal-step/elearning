
<!doctype html>
<html>
<head>

    <!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url('images/iphone-app.png')?>">
    <!-- Normal favicon -->
    <link rel="shortcut icon" sizes="32x32" href="<?php echo base_url('images/favicon.png')?>">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
    <link rel="stylesheet" href="<?php echo base_url()?>css/login.css" type="text/css" />

    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('fancybox/jquery.fancybox.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/style.css')?>">



    <style>.modal-dialog {
            margin: 0;
            position: absolute;
            top: 50%;
            left: 50%;
        }

        .modal-body {
            overflow-y: auto;
        }
        .modal-footer {
            margin-top: 0;
        }

        .forgot:hover{
          text-decoration:none;
        }

        @media (max-width: 767px) {
            .modal-dialog {
                width: 100%;
            }
        }</style>
    <script src="<?php echo base_url('js/jquery.min.js')?>"></script>



</head>
<body><a name="top-navigation"></a>

<div class="top-nav">
    <div class="row-fluid">
        <div class="col-md-12">
            <div class="container-fluid main-container">
                <div class="row-fluid">
                    <div class="col-md-12">
                        <div class="signin">
                            <?php if($this->session->userdata("id")) {?>

                              <ul>

                                <li class="dropdown" style="list-style-type: none">
                                  <a class="dropdown-toggle" data-toggle="dropdown" href=""><button class="login-button">My Account</button></a>
                                  <ul class="dropdown-menu">
                                      <li><a href="<?php echo site_url('student/profile')?>">Profile</a></li>
                                      <li><a href="<?php echo site_url('student/profile/change_password')?>">Change Password</a></li>
                                      <li><a href="<?php echo site_url('logout') ?>">Logout</a></li>
                                  </ul>
                              </li>
                            </ul>
                            <?php }
                            else
                            { ?>
                            <button class="login-button" data-toggle="modal" data-target=".bs-example-modal-sm">
                               Sign In
                            </button>
                          <?php } ?>
                            <script type="text/javascript">
                            $(function(){
                              $('.login-form').submit(function(e){
                                e.preventDefault();
                                if (document.login.username.value == "" ) {
                                    document.getElementById('uname').innerHTML="*Please enter a username*";

                                 return false;
                               }
                                else if(document.login.password.value == "")
                                {
                                    document.getElementById('pword').innerHTML="*Please enter a password*";
                                    return false;
                                }
                                else
                                {
                                    $.post('<?php echo site_url('login/check_login') ?>',
                                    $('.login-form').serialize(),
                                    function(result){
                                      if(result == 'success'){
                                        window.location.replace('<?php echo site_url('student/profile') ?>')
                                      } else{
                                        $('#pword').html("Invalid login details.");
                                      }
                                    },
                                    "html"
                                    )
                                }
                               // return true;
                              })

                            });

                                     function validation()
                                     {

                                   }
                            </script>



                            <div class="modal fade bs-example-modal-sm" tabindex="-1"  role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-sm hide_box">
                                    <div class="modal-content">
                                        <div claas="login-form" style="margin:20px;height:auto;">
                                            <form class="login-form" method="post" action="<?php //echo site_url('login')?>" name="login" style="height:auto">
                                                <h3 class="txt-center">Login</h3>
                                                <input type="text" name="username" class="login-input" placeholder="Email" style="color:#000;">
                                                <div id ="uname" class="alert-danger"></div>

                                                <input type="password" name="password" class="login-input" placeholder="Password" style="color:#000;">
                                                <div  id="pword" class="alert-danger" ></div>

                                                <input type="submit" class="login-btn" value="Login">
                                                <input type="hidden" name="type" class="login-input" value ="user" placeholder="">
                                                <a class="forgot"  data-toggle="modal" data-target=".forgot_box">Fotgot Password</a>
                                                <div id="res"></div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <script type="text/javascript">
                            $(function(){
                              $('.forget-form').submit(function(e){


                                e.preventDefault();
                                if (document.forget.username.value == "" ) {
                                    document.getElementById('empty').innerHTML="*Please enter a username*";

                                 return false;
                               }

                                else
                                {
                                    $.post('<?php echo site_url('login/forget_password') ?>',
                                    $('.forget-form').serialize(),
                                    function(result){

                                      if(result == 'success')
                                      {

                                        $('#username').html("Your password has been sent in mail. Please check your email.");
                                        document.forget.username.value('');
                                      }
                                      else
                                        {
                                          $('#invalid').html("Invalid User Email").css('color:red');
                                        }
                                    },
                                    "html"
                                    )
                                }
                               // return true;
                              })

                            });


                            </script>


                            <div class="modal fade forgot_box" tabindex="-1"  role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div claas="login-form" style="margin:20px;height:auto;">
                                            <form class="forget-form" method="post" action="<?php //echo site_url('login')?>" name="forget" style="height:auto">
                                                <h3 class="txt-center">Forget Password</h3>
                                                <input type="text" name="username" class="login-input" placeholder="Email" style="color:#000;">
                                                <div id ="username" class="alert-success"></div>
                                                <div id ="invalid" class="alert-danger"></div>
                                                <div id ="empty" class="alert-danger"></div>

                                                <input type="submit" class="login-btn" value="Login">
                                                <input type="hidden" name="type" class="login-input" value ="user" placeholder="">

                                                <div id="res"></div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>







                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>

<div class="outer">
    <div class="header-wrapper">
        <div class="row-fluid">
            <div class="col-md-12">
                <div class="main-container">
                    <div class="row-fluid">
                        <div class="col-md-3">
                            <div class="logo">
                                <a href="<?php echo site_url('')?>">
                                  <img src="<?php echo base_url('images/logo.png')?>">
                                  </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-5">
                            <!--<div class="nav">
                                <ul>
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Subjects</a></li>
                                <li><a href="#">Curriculum</a></li>
                                <li><a href="#">Our Tutor</a></li>
                                <li><a href="#">Pricing</a></li>
                            </ul>
                        </div>-->



                            <div class="navbar-header">
                                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar icon-bar-nav"></span>
                                    <span class="icon-bar icon-bar-nav"></span>
                                    <span class="icon-bar icon-bar-nav"></span>
                                </button>
                            </div>
                            <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
                                <ul class="navbar-nav nav">
                                    <li><a href="#top-navigation">Home</a></li>
                                    <li><a href="#courses">Courses</a></li>
                                    <li><a href="#blog">Blog</a></li>
                                    <li><a href="#cont">Contact Us</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div class="clouds">
    <div class="row-fluid">
        <div class="main-container">
            <div class="col-md-7">
                <div class="header-txt">
                    <h1>Better & Effective</h1>
                    <h3>Learning by IT Experts</h3>
                    <input type="button" value="Lets Get Started" class="header-btn">
                </div>
            </div>
            <div class="col-md-5">
                <div class="header-form">

                    <?php if($this->session->flashdata('flashSuccess')):?>
                        <p class='alert alert-success'> <?=$this->session->flashdata('flashSuccess')?> </p>
                    <?php endif?>
                    <form method="post" action="<?php echo site_url('welcome/sign_up')?>">
                        <div class="form-error" >
                        <input type="text" name="first_name" class="form-input" placeholder="First Name">
                          <span class="alert-danger" style= "color:#E8E8E8; text-align: left" ><?php echo form_error('first_name')?></span>
                        </div>
                        <input type="text" name="last_name" class="form-input" placeholder="Last Name">

                        <input type="text" name="email" class="form-input" placeholder="Email">
                        <span class="alert-danger" style= "color:#E8E8E8; text-align: left"><?php echo form_error('email')?></span>

                        <input type="password" name="password" class="form-input" placeholder="Password">
                        <span class="alert-danger" style= "color:#E8E8E8; text-align: left"><?php echo form_error('password')?></span>

                        <input type="submit" value="Sign Up" class="form-btn">

                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>

<div class="services-wrapper">
    <div class="main-container">
        <div class="row-fluid">
            <div class="col-md-3">
                <div class="services">
                    <i class="fa fa-desktop"></i>
                    <h3>Online Classes</h3>
                    <div class="services-txt">
                        <p>Our online classes offer you the opportunity to learn on your own time and at your own pace. All of our classes are completely free and will earn you accredited ...</p>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="services">
                    <i class="fa fa-headphones"></i>
                    <h3>24/7 Access</h3>
                    <div class="services-txt">
                        <p>Welcome to the homepage for the Web Training'z 24 hour Pre-service ... see if your field observation course qualifies as a substitute for the 24-hour Preservice ...</p>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="services">
                    <i class="fa fa-laptop"></i>
                    <h3>Live Projects</h3>
                    <div class="services-txt">
                        <p>Service Provider of Training, PHP & MySQL Training, Web Designing, Live Project Training, Java Training & Services Offered By Web Training'z ...</p>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="services">
                    <i class="fa fa-users"></i>
                    <h3>Our Tutors</h3>
                    <div class="services-txt">
                        <p>Our tutors are real people that you cancount on for expert help 24/7. You can get a tutor online for help in more than 40<br> courses.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>



<div class="courses-wrapper">
  <a name="courses"></a>
    <div class="main-container">
        <div class="row-fluid">
            <?php
                foreach($cources as $data)
                {
                 ?>

                    <div class="col-md-3">
                        <div class="courses">
                            <a  href="<?php echo site_url('welcome/description/'.$data->id."/".$category_id='0')?>"><img height="170" width="152" src="<?php echo base_url('upload/'.$data->logo)?>"></a>
                            <h3></h3>
                            <p>Duration - 6 week</p>
                            <p>Fee <span>- $20</span></p>
                            <div class="courses-text"><?php echo substr($data->detail,0,100)?>....</div>
                        </div>
                    </div>

                <?php
                }
            ?>
        </div>
    </div>
    <div class="clear"></div>
</div>



<div class="footer-wrapper">
  <a name="cont"></a>
<div class="main-container">
    <div class="row-fluid">
        <div class="col-md-6">
            <div class="contact-us">
                <h1>CONTACT US</h1>
                <p>
                  <?php if($this->session->flashdata('flashcontact')):?>
                      <p class='alert alert-success'> <?=$this->session->flashdata('flashcontact')?> </p>
                  <?php endif?>
                    Please dont hesitate to contact us for more information </p>
              <form action="<?php echo site_url('welcome/contact_us')?>" method="post">
                <input type="text" name="username" class="contact-input" placeholder="EMAIL">
                <span class="alert-danger" style= "color:#E8E8E8; text-align: left" ><?php echo form_error('username')?></span>

                <input type="text" name="subject" class="contact-input" placeholder="Subject">
                <span class="alert-danger" style= "color:#E8E8E8; text-align: left" ><?php echo form_error('subject')?></span>

                <input type="text" name="message" class="contact-input" placeholder="MESSAGES"><br>
                <span class="alert-danger" style= "color:#E8E8E8; text-align: left" ><?php echo form_error('message')?></span>

                <input type="submit" value="SEND" class="contact-us-btn">
              </form>
            </div>
        </div>

        <div class="col-md-6">
            <div class="about-us">
                <a href="#">About Us</a>
                <a href="#">Company</a>
                <a href="#">Testimonials</a>
                <a href="#">Subject</a>
                <a href="#">Curicular</a>
                <a href="#">Our Tutor</a>
            </div>

            <div class="about-us">
                <a href="#">Help</a>
                <a href="#">FAQ</a>
                <a href="#">Contact Us</a>
                <a href="#">Subject</a>
                <a href="#">Curicular</a>
                <a href="#">Terms  &
                    Conditions</a>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="col-xs-12">
            <div class="social-icons">
                <a href="http://www.facebook.com/webtrainingz" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="https://twitter.com/webtrainingz" target="_blank"><i class="fa fa-twitter"></i></a>
                <a href="https://www.linkedin.com/profile/view?id=364126050&trk=nav_responsive_tab_profile_pic" target="_blank"><i class="fa fa-linkedin"></i><a/>
                    <a href="https://plus.google.com/u/0/109291872912070159309/posts" target="_blank"><i class="fa fa-google-plus"></i></a>
                    <a href="https://www.youtube.com/channel/UCKTUaTX5IMkE8MmIADxMMQA" target="_blank"><i class="fa fa-youtube"></i></a>
                    <a href="http://www.skype.com" target="_blank"><i class="fa fa-skype"></i></a>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row-fluid">
    <div class="col-xl-12">
        <div class="main-conatiner">
            <div class="copyright">
                <p>@2014 WebTrainings4u.com All rights reserved.Dessigned by Think Designer.</p>
            </div>
        </div>
    </div>
</div>

<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('fancybox/jquery.fancybox.pack.js')?>"></script>

<script type="text/javascript">

    // speed in milliseconds
    var scrollSpeed = 50;

    // set the default position
    var current = 0;

    // set the direction
    var direction = 'h';

    function bgscroll(){

        // 1 pixel row at a time
        current -= 1;

        // move the background with backgrond-position css properties
        $('div.clouds').css("backgroundPosition", (direction == 'h') ? current+"px 0" : "0 " + current+"px");

    }

    //Calls the scrolling function repeatedly
    setInterval("bgscroll()", scrollSpeed);

</script>


<script>
    $(".opentab").fancybox({
        'width'       : '68%',
        'height'      : '50%',
       // 'autoScale'       : false,
       // 'transitionIn'    : 'none',
       // 'transitionOut'   : 'none',
        'type'        : 'iframe'
    });
</script>

<script>
    $(".fancybox").fancybox({
        'width'       : '500px',
        'height'      : '500px;',
      //  'autoScale'       : false,
      //  'transitionIn'    : 'none',
      //  'transitionOut'   : 'none',
        'type'        : 'iframe'
    });
</script>

<script>
    $(".onscroll").header-wrapper({
        'auto-top': true
    });
</script>

<script>
    $(function() {
        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 700);
                    return false;
                }
            }
        });
    });
</script>

<script>
    function adjustModalMaxHeightAndPosition(){
        $('.modal').each(function(){
            if($(this).hasClass('in') == false){
                $(this).show();
            };
            var contentHeight = $(window).height() - 60;
            var headerHeight = $(this).find('.modal-header').outerHeight() || 2;
            var footerHeight = $(this).find('.modal-footer').outerHeight() || 2;

            $(this).find('.modal-content').css({
                'max-height': function () {
                    return contentHeight;
                }
            });

            $(this).find('.modal-body').css({
                'max-height': function () {
                    return (contentHeight - (headerHeight + footerHeight));
                }
            });

            $(this).find('.modal-dialog').css({
                'margin-top': function () {
                    return -($(this).outerHeight() / 2);
                },
                'margin-left': function () {
                    return -($(this).outerWidth() / 2);
                }
            });
            if($(this).hasClass('in') == false){
                $(this).hide();
            };
        });
    };

    $(window).resize(adjustModalMaxHeightAndPosition).trigger("resize");
    //# sourceURL=pen.js
</script>


<script>
    $(document).ready(function() {

        var div = $('.outer');
        var start = $(div).offset().top;
        start = 0;
        var p = $(window).scrollTop();
        $(div).css('position',((p)>start) ? 'fixed' : 'static');
        $(div).css('top',((p)>start) ? '0px' : '');

        $(div).css('position', 'static')
        $.event.add(window, "scroll", function() {
            var p = $(window).scrollTop();
            $(div).css('position',((p)>start) ? 'fixed' : 'static');
            $(div).css('top',((p)>start) ? '0px' : '');
        });

    });
</script>


</body>
</html>
