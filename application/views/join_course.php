<html>
<head>
	<title></title>

    <link rel="stylesheet" type="text/css" href="<?Php echo base_url()?>css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/style.css">
</head>
<body>

	<div class="courses-detail-wrapper">
		<div class="row-fluid">
			<div class="col-md-12">
				<div class="courses-nav">
					<ul>
                        <li class="nav-active"><a href="<?php echo site_url()?>/benefit">Benifits</a></li>
                        <li><a href="<?php echo site_url()?>/syllabus">Syllabus</a></li>
                        <li><a href="<?php echo site_url()?>/tutor">Tutor Info</a></li>
                        <li><a href="<?php echo site_url()?>/feedback">Feedback</a></li>
                        <li><a href="<?php echo site_url()?>/join_course">Join Course</a></li>
					</ul>
					<div class="clear"></div>
				</div>
			</div>
		</div>

		<div class="row-fluid">
			<div class="col-md-12">
				<div class="courses-detail-txt">
					<h1>Join Course</h1>
				</div>
			</div>
		</div>

		<div class="row-fluid">
			<div class="col-md-12">
				<div class="join-course">

					<input type="text" class="form-control join-input" placeholder="First Name">
					<input type="text" class="form-control join-input" placeholder="Last Name">
					<input type="text" class="form-control join-input" placeholder="Email">
					<input type="password" class="form-control join-input" placeholder="Password">
					<input type="button" class="btn" value="Sign Up">
				</div>
			</div>
		</div>


	</div>

	

	<script src="<?Php echo base_url()?>js/jquery.min.js"></script>
	<script src="<?Php echo base_url()?>js/bootstrap.min.js"></script>
</body>
</html>