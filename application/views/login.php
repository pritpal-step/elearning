<html>
<head>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>css/login.css" type="text/css" />
</head>
<body>

	<div class="container-fluid main">
		<div class="row-fluid">
			<div class="col-md-12">
				<div claas="login-form">
                    <form method="post" action="<?php echo site_url('login')?>">
                        <h3 class="txt-center">Login</h3>
                        <input type="text" name="username" class="login-input" placeholder="Email">
                        <input type="password" name="password" class="login-input" placeholder="Password">
                        <input type="hidden" name="type" class="login-input" value ="user" placeholder="">
                        <input type="submit" class="login-btn" value="Login">
                    </form>
				</div>
			</div>
		</div>
	</div>

<script src="<?php echo base_url()?>js/jquery.min.js"></script>
	<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
</body>
</html>