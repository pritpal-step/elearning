<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 10/16/14
 * Time: 1:32 PM
 */

 include('header.php');
 
?>



<div class="container content_container">

    <div class="row-fluid">
        <div class="col-md-2">
        </div>
        <div class="col-md-7 well">
            <div class="join-course">

                <?php if($this->session->flashdata('flashSuccess')):?>
                    <p class='alert alert-success'> <?=$this->session->flashdata('flashSuccess')?> </p>
                <?php endif?>
                <form method="post" action="<?php echo site_url('student/profile/update_password')?>">

                   <input type="password" name="old_password" class="form-input" placeholder="Old Password">
                    <span class="alert-danger" style= "text-align: left" ><?php echo form_error('old_password')?></span>

                    <input type="password" name="new_password" class="form-input" placeholder="New Password">
                    <span class="alert-danger" style= "text-align: left"><?php echo form_error('new_password')?></span>

                    <input type="password" name="confirm_password" class="form-input" placeholder="confirm_Password">
                    <span class="alert-danger" style= "text-align: left"><?php echo form_error('confirm_password')?></span>

                    <input type="submit" value="Change Password" class="form-btn">

                </form>
            </div>

        </div>


    </div>

</div>


<script src="<?php echo base_url('js/jquery.min.js')?>"></script>
<script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>

</body>
</html>
