<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 10/16/14
 * Time: 1:32 PM
 */

 include('header.php');

?>



<div class="container content_container">

    <div class="row-fluid">
        <div class="col-md-2">
        </div>
        <div class="col-md-7 well">
            <div class="join-course">

                <?php if($this->session->flashdata('flashsuccess')):?>
                    <p class='alert alert-success'> <?=$this->session->flashdata('flashsuccess')?> </p>
                <?php endif?>
                <div class="testimonial_title">Contact us for more information </div>
                <form action="<?php echo site_url('welcome/contact_us')?>" method="post">
                  <input type="text" name="username" class="form-input" placeholder="EMAIL">
                  <span class="alert-danger" style= "text-align: left" ><?php echo form_error('username')?></span>

                  <input type="text" name="subject" class="form-input" placeholder="Subject">
                  <span class="alert-danger" style= "text-align: left" ><?php echo form_error('subject')?></span>

                  <input type="text" name="message" class="form-input" placeholder="MESSAGES"><br>
                  <span class="alert-danger" style= "text-align: left" ><?php echo form_error('message')?></span>

                  <input type="submit" value="SEND" class="contact-us-btn">
                </form>
            </div>

        </div>


    </div>

</div>


<script src="<?php echo base_url('js/jquery.min.js')?>"></script>
<script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>

</body>
</html>
