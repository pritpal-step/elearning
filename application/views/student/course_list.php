<?php
include('header.php');
?>
<div class="container content_container">
    <div class="row-fluid">

          <div class="col-md-7 well">
            <div class="testimonial_title"> COURSES YOU HAVE JOINED</div>
            <?php if($this->session->flashdata('flashsuccess')):?>
                <p class='alert alert-success'> <?=$this->session->flashdata('flashsuccess')?> </p>
            <?php endif?>
            <?php if($this->session->flashdata('no_course')){?>
                <p class='alert alert-danger' style="color:#000;"> <?=$this->session->flashdata('no_course')?> </p>
            <?php } ?>
          <?php if(!empty($courses)) {?>
                <table class="table table-responsive">
                    <tr>

                        <th>Name</th>
                        <th>Action</th>

                    </tr>
                    <?php foreach($courses as $course) {?>
                     <tr>
                         <td style="color:black"><?php echo ucfirst($course->name) ?></td>
                         <td><a href="<?php echo site_url('student/profile/description')."/".$category_id=$course->category_id ?>">View</a></td>

                     </tr>
                    <?php } ?>
                 </table>
            <?php	} else {

                    echo "You have not joined courses <a href='".site_url('student/join')."'>click here</a> to join the courses you want";

                  }?>

            </div>
            <div class="col-md-1"></div>
            <div class="col-md-4 well">
                <div class="courses-detail-txt">
                  <div class="testimonial_title">Assignments</div>
                    
                </div>
            </div>
    </div>
</div>

<script src="<?php echo base_url('js/jquery.min.js')?>"></script>
<script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

</body>
</html>
