<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 10/13/14
 * Time: 10:47 AM
 */
include('header.php');
?>
<div class="container content_container">

        <div class="row-fluid">
                <div class="col-md-7 well">
                  <?php foreach($detail as $details) {?>
                      <p><h2><?php echo ucfirst($details->name)?></h2></p>
                      <p><b>Description: </b><?php echo ucfirst($details->description)?></p>
                      <p><b>Feactures: </b><?php echo ucfirst($details->features)?></p>
                      <p><b>Syllabus: </b><?php echo ucfirst($details->syllabus)?></p>
                  <?php }?>
                  <b>PDF: </b>
                  <ul>

                    <?php foreach($tutorial as $tutorials) {?>
                      <li><?php echo $tutorials->pdf_url?> <a href="<?php echo base_url('pdf/'.$tutorials->pdf_url)?>">Download</a></li>
                    <?php }?>
                  </ul>
                  <b>Video Tutorials</b>
                  <ul>
                    <?php foreach($tutorial as $tutorials) {?>
                      <a target="_blank" href="<?php echo $tutorials->tutorial_url?>"><li><?php echo $tutorials->tutorial_url?></li></a>
                    <?php }?>
                  </ul>
                </div>
                <div class="col-md-1">
                </div>
                <div class="col-md-4 well">
                    <div class="courses-detail-txt">

                    </div>
                </div>
        </div>

</div>


<script src="<?php echo base_url('js/jquery.min.js')?>"></script>
<script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

</body>
</html>
