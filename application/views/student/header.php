
<html>
<head>
  <title></title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/bootstrap.min.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/style.css')?>">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="<?php echo base_url('js/live.js')?>"></script>
</head>
<body>

<div class="row-fluid course_header" >
        <div class="container c_container">
               <div class="row-fluid">

                   <div class="col-md-6">
                       <h3>Student Profile</h3>
                        <p><?php echo ucfirst($student->first_name)." ".ucfirst($student->last_name)?></p>
                        <span class="course_feature"></span>
                    </div>


               </div>


               <div class="signin">
                    <ul>

                      <li class="dropdown" style="list-style-type: none">
                        <a class="dropdown-toggle" data-toggle="dropdown" href=""><button class="login-button">My Account</button></a>
                        <ul class="dropdown-menu">
                          <li><a href="<?php echo site_url('student/profile')?>">Profile</a></li>
                            <li><a href="<?php echo site_url('student/profile/change_password')?>">Change Password</a></li>
                            <li><a href="<?php echo site_url('logout') ?>">Logout</a></li>
                        </ul>
                    </li>
                  </ul>
                    <nav role="navigation" class="collapse navbar-collapse bs-navbar-collapse">
                        <ul class="navbar-nav nav">
                            <li><a style="color:#FFFFFF" href="<?php echo site_url('student/profile/courses')?>">My Courses</a></li>
                            <li><a style="color:#FFFFFF"href="<?php echo site_url('student/join')?>">Join Course</a></li>

                            <li><a style="color:#FFFFFF" href="<?php echo site_url('student/profile/contact_us')?>">Contact Us</a></li>
                        </ul>
                    </nav>


               </div>
        </div>
</div>
