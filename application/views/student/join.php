<?php
/**
 * Created by PhpStorm.
 * User: PC-4
 * Date: 10/13/14
 * Time: 10:47 AM
 */
include('header.php');
?>



<div class="container content_container">

        <div class="row-fluid">

                <div class="col-md-4 well">

                        <div class="testimonial_title">Join Course</div>

                        <div class="join-course">

                          <br>
                            <span>Please select a course to join</span>

                            <form action="<?php echo site_url('student/join/add')?>" method="post">
                              <br>
                              <?php if($this->session->flashdata('course_exist')):?>
                                  <p class='alert alert-danger'> <?=$this->session->flashdata('course_exist')?> </p>
                              <?php endif?>
                                <select name="name" class="form-input" id="category" placeholder="" style="color:#000;">
                                    <option value="">--Select Category--</option>
                                    <?php foreach($category as $category) {?>
                                        <option value="<?php echo $category->id?>"><?php echo $category->name?></option>
                                    <?php }?>
                               </select>

                                <span id="sub_category">
                                <!-- display dependent category-->
                                </span>
                                </br></br>
                                <input type="submit" value="JOIN" class="form-btn">
                            </form>

                    </div>
                </div>

                <div class="col-md-1"></div>

                <div class="col-md-7 well">
                    <?php if($this->session->flashdata('empty_cart')):?>
                        <p class='alert alert-danger'> <?=$this->session->flashdata('empty_cart')?> </p>
                    <?php endif?>

                    <table border="1" class="table table-responsive table-bordered">
                       <tr>
                           <th>Name</th>
                           <th>Price</th>
                           <th>Sub Total</th>
                           <th>Action</th>
                       </tr>
                        <?php foreach ($this->cart->contents() as $items) { ?>
                            <tr>
                                <td style="color:black;"><?php echo ucfirst($items['name'])?></td>
                                <td style="color:black;"><?php echo $items['price']?></td>
                                <td style="color:black;">$<?php echo $this->cart->format_number($items['subtotal']); ?></td>
                                <td style="color:black;"><a href="<?php $rowid = $items['rowid']; echo site_url('student/join/delete_item')."/".$rowid ?>"><i class="fa fa-times"></i></a></td>
                            </tr>
                        <?php }?>

                        <tr>
                            <th></th>
                            <th class="right"style="color:black;"><strong>Total</strong></th>
                            <th style="color:black;" >$<?php echo $this->cart->format_number($this->cart->total()); ?></th>
                            <th></th>
                        </tr>


                    </table>
<!--                    <a href="--><?php //echo site_url('student/profile/pay')?><!--"><button type="button" class="form-btn">Pay And Start Classes</button></a>-->

                      <span><a href="<?php echo site_url('student/join/payment')?>">
                        <button type="button" class="form-btn">Pay And Start Classes</button>

                      </a><h1><i class="fa fa-cc-paypal"></i></h1></span>

                </div>

        </div>

</div>


<script src="<?php echo base_url('js/jquery.min.js')?>"></script>
<script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $('#category').change(function() {

            var id=$('#category').val();
            
            $.ajax({
                type: "POST",
                url:"<?php echo site_url('student/profile/select') ?>",
                data: {'id':id},
                success:function(result){

                    $('#sub_category').html(result);
                }});

            });

        });
</script>
</body>
</html>
