<html>
<head>
	<title></title>

	<link rel="stylesheet" type="text/css" href="<?Php echo base_url()?>css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?Php echo base_url()?>css/style.css">
</head>
<body>

	<div class="courses-detail-wrapper">
		<div class="row-fluid">
			<div class="col-md-12">
				<div class="courses-nav">
					<ul>
                        <li class="nav-active"><a href="<?php echo site_url()?>/benefit">Benifits</a></li>
                        <li><a href="<?php echo site_url()?>/syllabus">Syllabus</a></li>
                        <li><a href="<?php echo site_url()?>/tutor">Tutor Info</a></li>
                        <li><a href="<?php echo site_url()?>/feedback">Feedback</a></li>
                        <li><a href="<?php echo site_url()?>/join_course">Join Course</a></li>
					</ul>
					<div class="clear"></div>
				</div>
			</div>
		</div>

		<div class="row-fluid">
			<div class="col-md-12">
				<div class="courses-detail-txt">
					<h1>Syllabus</h1>
					<p>Students in this class should learn enough HTML, HTML5, CSS, JavaScript, XML, JSON, PHP, and SQL and other tools, as needed, to create dynamic web sites that include client-side and server-side scripting.
						Students will hone their writing skills as they blog their goals, plans, design decisions, accomplishments, and user guidelines.
						Students will hone their presentation skills as give technical presentations on their own work and on other web topics and tools.
						Finally, Students will finish this course with their own web site that should be something they can show off to future employers or graduate school admission committees as well as family and friends..</p>
					</div>
				</div>
			</div>
		</div>

		<script src="<?Php echo base_url()?>js/jquery.min.js"></script>
		<script src="<?Php echo base_url()?>js/bootstrap.min.js"></script>
	</body>
	</html>