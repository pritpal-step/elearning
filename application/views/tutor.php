<html>
<head>
	<title></title>

	<link rel="stylesheet" type="text/css" href="<?Php echo base_url()?>css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?Php echo base_url()?>css/style.css">
</head>
<body>

	<div class="courses-detail-wrapper">
		<div class="row-fluid">
			<div class="col-md-12">
				<div class="courses-nav">
					<ul>
                        <li class="nav-active"><a href="<?php echo site_url()?>/benefit">Benifits</a></li>
                        <li><a href="<?php echo site_url()?>/syllabus">Syllabus</a></li>
                        <li><a href="<?php echo site_url()?>/tutor">Tutor Info</a></li>
                        <li><a href="<?php echo site_url()?>/feedback">Feedback</a></li>
                        <li><a href="<?php echo site_url()?>/join_course">Join Course</a></li>
					</ul>
					<div class="clear"></div>
				</div>
			</div>
		</div>

		<div class="row-fluid">
			<div class="col-md-12">
				<div class="courses-detail-txt">
					<h1>Tutor info</h1>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
				</div>
			</div>
		</div>
	</div>

	<script src="<?Php echo base_url()?>js/jquery.min.js"></script>
	<script src="<?Php echo base_url()?>js/bootstrap.min.js"></script>
</body>
</html>