<?php include('nav.php')?>
<div class="pageheader">
    <h2><i class=""></i>Active Students</h2>
    <div class="breadcrumb-wrapper">


    </div>
</div>

<div class="contentpanel">
    <?php if($this->session->flashdata('flashSuccess')):?>
        <p class='alert-success'> <?=$this->session->flashdata('flashSuccess')?> </p>
    <?php endif?>

    <?php echo $pagination;?>
    <table class="table table-responsive">
      <tr>
          <th>  Student name  </th>
          <th>  Email    </th>
          <th>  Course   </th>
          <th>  Start Date   </th>
          <th>  end date Date   </th>
          <th>  Status   </th>

      </tr>

      <?php
        foreach($student as $students)
        { foreach($students as $stu) {?>
          <tr>
            <td><?php echo $stu->first_name; echo' '.$stu->last_name;?> </td>
            <td><?php echo $stu->username;?></td>
            <td><?php echo $stu->name;?></td>
            <td><?php echo date('d-m-Y',strtotime($stu->start_date));?></td>
            <td><?php echo date('d-m-Y',strtotime($stu->end_date));?></td>
            <td class="btn btn-success"><?php echo $stu->status;?></td>
          </tr>

        <?}}
      ?>

    </table>



</div><!-- contentpanel -->

</div><!-- mainpanel -->


</section>


<script src="<?php echo base_url()?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url()?>js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>js/modernizr.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url()?>js/toggles.min.js"></script>
<script src="<?php echo base_url()?>js/retina.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.cookies.js"></script>

<script src="<?php echo base_url()?>js/flot/flot.min.js"></script>
<script src="<?php echo base_url()?>js/flot/flot.resize.min.js"></script>
<script src="<?php echo base_url()?>js/morris.min.js"></script>
<script src="<?php echo base_url()?>js/raphael-2.1.0.min.js"></script>

<script src="<?php echo base_url()?>js/jquery.datatables.min.js"></script>
<script src="<?php echo base_url()?>js/chosen.jquery.min.js"></script>

<script src="<?php echo base_url()?>js/custom.js"></script>
<script src="<?php echo base_url()?>js/dashboard.js"></script>

</body>
</html>
