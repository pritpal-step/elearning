-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 16, 2014 at 04:09 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `elearning`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_id` int(50) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `display_order` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent_id`, `logo`, `display_order`) VALUES
(1, 'PHP', 0, '5434e2c3a7591php.png', ''),
(2, 'HTML', 0, '5434e2d816201html.png', ''),
(4, 'php for 6 week', 1, '', ''),
(5, 'html 5', 2, '', ''),
(6, 'php basics', 1, '', ''),
(7, 'Java', 0, '543b985fe9462java.png', '1');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `features` varchar(255) NOT NULL,
  `syllabus` varchar(555) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `image` varchar(255) NOT NULL,
  `fee` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `category_id`, `name`, `description`, `features`, `syllabus`, `duration`, `start_date`, `end_date`, `image`, `fee`) VALUES
(2, '4', 'php for 6 week', '<p>PHP, also known as the "PHP: Hypertext Preprocessor," is a widely used computer programming language. It is used to create dynamic Web pages, or Web pages that update and display information depending on the user''s activity.</p>', '<p>Some of the key features of PHP 5.1.x include:</p><ul> \r\n<li>A complete rewrite of date handling code, with improved timezone support.</li><li>Significant performance improvements compared to PHP 5.0.X.</li><li>PDO extension is now enabled by default.&', '<p>\r\n	Introduction to PHP</p><ul>\r\n	\r\n<li>&nbsp;Evaluation of Php&nbsp;</li>	\r\n<li>Basic Syntax&nbsp;</li>	\r\n<li>Defining variable</li></ul>', ' days', '2014-10-08 00:00:00', '2014-10-08 00:00:00', '', '1000'),
(3, '5', 'html 5', '<p><strong>HTML</strong> or <strong>HyperText Markup Language</strong> is the standard e&nbsp;used to create&nbsp;</p><p>HTML is written in the form of consisting of <em>tags</em> enclosed in like <code>&lt;html&gt;</code>). HTML tags most commonly come i', '<p>HTML 5 features which are useful <em style="background-color: initial;">right now</em> include:</p><ul> \r\n<li>Web Workers: Certain web applications use heavy scripts to perform functions. Web Workers use separate background threads for processing and i', '<p>HTML</p><p><br><img src="http://www.sssit.org/images/check.png" style="margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Verdana; font-size: 13px;">	Introduction to HTML</p>', ' weeks', '2014-10-08 00:00:00', '2014-10-08 00:00:00', '', '1000'),
(4, '6', 'php basics', '<p>fsf</p><p>fsdfs</p><p>fsf</p><p>sf</p>', '<p>fff</p><p>ef</p><p>f</p><p>ef</p>', '<p>ff</p><p>sdf</p><p>sf</p>', '4 weeks', '2014-10-13 00:00:00', '2014-10-13 00:00:00', '', '200');

-- --------------------------------------------------------

--
-- Table structure for table `course_tutors`
--

CREATE TABLE IF NOT EXISTS `course_tutors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tutor_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `course_tutors`
--

INSERT INTO `course_tutors` (`id`, `tutor_id`, `name`, `category_id`) VALUES
(2, '1', '', '2'),
(3, '1', '', '1'),
(5, '2', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `pdf`
--

CREATE TABLE IF NOT EXISTS `pdf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `pdf`
--

INSERT INTO `pdf` (`id`, `name`, `category_id`, `url`) VALUES
(1, '<p>Php Pdf</p>', '1', '5434eee689fa9DR_Packing_Slip.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `reset` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `first_name`, `last_name`, `username`, `password`, `reset`, `user_id`) VALUES
(1, 'fghfdgf', 'ghfhfgh', 'sd@gmail.com', 'sadsad', '', 'zKHFZVaAA2'),
(2, 'goapl', 'kumar', 'gopalkumar315@gmail.com', 'Abc123def', '', 'YbOQabWQmy'),
(3, 'kamal', 'maan', 'kamalmaan82@gmail.com', '123', '', 'IGxuMQBUmG'),
(4, 'abcde', 'Kaur', 'ran@gmail.com', '123', '', 'FP7XuWuQYO'),
(5, 'ranjit', 'aulakh', 'ranjit8aulakh@gmail.com', 'ranjit', '', '6pD0qFJ6ci');

-- --------------------------------------------------------

--
-- Table structure for table `student_courses`
--

CREATE TABLE IF NOT EXISTS `student_courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `qty` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'new',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `student_courses`
--

INSERT INTO `student_courses` (`id`, `name`, `qty`, `price`, `category_id`, `user_id`, `status`) VALUES
(1, 'html 5', '', '1000', '5', 'FP7XuWuQYO', 'new'),
(2, 'php for 6 week', '', '1000', '4', 'FP7XuWuQYO', 'new');

-- --------------------------------------------------------

--
-- Table structure for table `tutorials`
--

CREATE TABLE IF NOT EXISTS `tutorials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tutors`
--

CREATE TABLE IF NOT EXISTS `tutors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tutors`
--

INSERT INTO `tutors` (`id`, `name`, `description`, `email`, `image`) VALUES
(1, 'Er. Davinder Singh', 'Bachelors in technology and Web Developer', 'davinder@gmail.com', '5434eca7a70f8765-default-avatar.png'),
(2, 'Er. Kamal Maan', 'Bachelors in technology and Web Developer', 'kamal@gmail.com', '54350f50c59ca765-default-avatar.png');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
