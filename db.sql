-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 0.0.0.0    Database: c9
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'admin','admin');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `category_id` int(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `postedby` varchar(255) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog`
--

LOCK TABLES `blog` WRITE;
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
INSERT INTO `blog` VALUES (13,1,'Virtual Reality','Virtual Reality (VR), has become a very hot topic in the recent times. The Virtual Reality or sometimes called Virtual Environments uses a large number of technologies to set up a Virtual World making a user to interact and manipulate with the virtual objects.','Webtrainings4u','2015-01-06 11:18:01'),(15,4,'Virtual Keyboard','Computing is not restricted to desktops and laptops these days. It has found its way into mobile devices like palm tops, cell phones, tablets. But the QWERTY keyboard remains unchanged from the last 50 years.','Webtrainings4u','2015-01-05 06:17:57'),(16,3,'HOW IS CELL CLONING DONE?','It is not a very trivial task or a harsh process to follow. Cloning could easily be done by modifying or replacing the EPROM in the phone with a new chip which would enable us to configure an Electronic serial number (ESN) via software','Webtrainings4u','2015-01-06 10:02:36'),(17,4,'HOW TO PREVENT CELL CLONING?','There are measures to verify MINs and ESNs that can be done electronically so as to prevent fraud. We should never trust mobile phones as a device for communicating/storing confidential information. Therefore always set a Pin that\'s required to be enter before the phone can be used so that no can access your mobile phone in an unauthorized way.','Webtrainings4u','2015-01-05 06:18:53'),(18,1,'Silent Sound Technology','Answering a call while in a movie theater, in a marriage, a noisy restaurant or while walking through the market area irritates all because there is a lot of noise around causes a large scale of disturbance ultimately cause headache. No more disturbance or any irritation now as now this problem is getting eliminated with â€silent soundsâ€, a latest technology that metamorphose movements of lips into a computer-generated voice for the receiver at the other end of the phone.','Webtrainings4u','2015-01-06 10:02:41'),(19,4,'Sensors For 3D Digitization (3D imaging technology)','Digital images are still pictures taken from a scene or scanned from documents such as photographs or artwork. Digital 3D imaging can be used in many different applications such as visual communication and industrial automation etc. This technique of digital imaging requires the integration of different sophisticated sensors. These sensors encompass the photo diodes based on integrated bi cells and the laser based position measurement sensors. The digital image is sampled and thereafter organized as a net of dots. Then an analog signal is produced for display. The analog signal is formed by the interpretation of bits which are further read by the machine. With the emergence technologies like very large scale integration various sophisticated CMOS transistor based sensors of high end digital chip integration are now available for digital 3D imaging which provide a high level of accuracy.\n\n\nThere are different electronic sensors selected to be mounted into VLSI for example a synchronization circuit. This circuit is based on dual photocells which ensure accurate measurements in environments with variations in temperature. But discrete synchronizers have posed many technical problems. The problem is now eliminated due to monolithic chip integration and fabrication technology or one more technique is to use the smart 3D position sensors. 3D imaging technology is getting widely accepted in research laboratories, industries and academics due to VLSI capabilities in integrated Circuit.\n','Webtrainings4u','2015-05-02 11:54:16'),(21,4,'Bluetooth Wireless Technology','Everybody has internet connection which enables them to communicate with other similar \n\ndevices. But do you ever think of the process of communication between various portable \n\ndevices like mobiles and laptops etc. Imagine for a second that if you have to carry yours wire \n\nout along with you to connect your device and enable it to communicate with other. Tensed???? \n\nNot anymore as here is a solution. Bluetooth wireless technology (IEEE 802.15.1) is a short-\n\nrange technology for communication which is intended to replace the cables to connect portable \n\nor fixed devices to maintain high levels of security. Robustness, low power, and low cost are the \n\nkey features of this Bluetooth device. It has been designed through a uniform process. It allows a \n\nwide variety of devices to connect and communicate with each other. To provide this \n\ncommunication among various different devices it uses Bluetooth wireless communication \n\nprotocol. These features made this device to achieve global acceptance. \n\nPico nets (A short-range and ad hoc networks) makes these Bluetooth devices to connect and \n\ncommunicate in a wireless mode. Each device can simultaneously communicate with up to seven \n\nother devices within. A single Pico net enables a device to communicate simultaneously with \n\nseven other devices. The feature that enables users to use a hands-free headset for voice calls, \n\nprinting and synchronizing etc with laptops and mobiles etc is its ability to handle data as well as \n\nvoice transmissions simultaneously','Webtrainings4u','2015-05-07 08:50:04'),(22,5,'Asp.net training institution in Ludhiana','You have probably heard the Word asp.net,it is a set of technologies provided by Microsoft that facilitates application development by offering multiple languages like c#,j#,visual basic. Web application developers have freedom to choose any of these languages which may be easy for develop mentor as clients demand. The main aim of the .net framework is to make easy the development of desktop, web applications and web server. Dot net platform help developers and designers to get benefits of its creative and dynamic characteristics in the layer of already available coding, reuse of code and facility to use different programming languages. \nToday, almost all the companies have their online presence in the form of website either to promote their service or products. This brings the market to a new edge hence creating more employment opportunities. Asp.net is purely server side technology. In the early days of the web, the contents of web pages were largely static. These pages needed to be constantly and manually modified. So it brings a need to create website that were dynamic and would automatically updated. Thus, it creates a lot of opportunity in this field for the candidates who are looking for a job and it doesn’t require having any expensive degrees.\nIt is important to note that asp.net classes are not designed for the beginners; in order to become eligible for asp.net class; one must be able to create HTML pages.\n\n\"...what makes any training good or poor is a consequence of how well it is designed, and intend to create practical skills among trainees.”\n\nWe all talk about web services, web services can do this and web services can do that. It’s the source of creating a link between the business & development. Therefore, it requires an interest & dedication to become an expert of it in few months. \n\n','Webtrainings4u','2015-05-08 11:36:17'),(23,1,'Which is the best php training institute in ludhiana','PHP (Hypertext Preprocessor), Wait Wait Wait, it’s not something related to hardware of computer as its name reflects. But it is a language that is mostly used for software development. Yes, it’s easy to use and its flexible code entitled it to be the simplest and most effective programming language.\nPHP (Pay Heed and Pursue):- Yes, It is very dynamic language to create websites easily. It’s very easy code will inspire to reach your system immediately and start learning PHP. Hypertext Preprocessor (PHP) creates wonders to a website while designing by its Graphical User Interface (GUI). The website designed while using PHP is so simple that a person having basic knowledge of computer can use it like an expert.\nPHP (Practice for High Platform) removes the fear of the programming languages forever, as it is just like to understand 2+2 is 4 for a graduate. And once involved in PHP it will take you to a higher level of software development. \nI know that now a question is striking in your mind that which company is to join with to learn PHP? No worries now as STEP Consultancy is here to help you out to achieve your aim. STEP consultancy is a leading IT software and web development company. It has a long proven track in providing training of PHP and all other Web development languages. STEP consultancy provides a highly professional environment in which you have a golden chance to work on live projects while being associated with the team of experts. So what are you waiting for? Take your foot ahead to step in STEP consultancy.\n','Webtrainings4u','2015-08-05 09:00:55'),(24,4,'Joomla and CMS - Webtrainings4u','Joomla is a free to use and an open source content management system (CMS) for publishing content on the World Wide Web (WWW) and intranets or a Web application framework. Written in PHP, Joomla uses object-oriented programming (OOP) concepts and software design patterns. Joomla stores data in a MySQL database and it incorporates features like page caching, news flashes, blogs and polls etc.\nA content management system (CMS) is a system which provides a collection of procedures that are used to manage work flow in a collective environment. In a CMS, data can be defined as anything like documents, movies, text, pictures, phone numbers, and scientific data etc. CMSs are frequently used to store, control and revise data as well as to publish documents. The CMS increases the version level by providing new updates to an existing file as it serves as a central repository.\nIf we look into past, having a webpage was cool. But the scenario is not the same these days as in this era of technology everybody is present on the web, one way or the other. There are various different websites like gossip websites, websites on politics and websites on sports and so on. Why, there is a mob of websites! Does that mean everyone has to be a geek to place him or her on web? No. All you need to adapt a Content Management System. Well, not any CMS, what you need is Joomla. It’s a dynamic open-source system that is very easy to work with.\n Joomla will let you to forget trivial tasks like coding, programming and designing. You just need to sit back, Joomla Itself do all the hard work a. Joomla is easy to install and operate. Joomla can do the wonders to your work..\n','Webtrainings4u','2015-05-14 06:24:25'),(25,4,'OMT (Object Modeling Techniques)','Object –Oriented techniques allows user to understand the overall structure, environment, \n\nidentify components and relationship between objects of the model. These object oriented \n\nmodels are very useful for understanding the problems, concerns with application experts, \n\nmodeling, preparing documentations and designing programs and database.\n\nOMT methodologies use following models:\n\nThe Object model describes the objects in the system and their relationships. This model \n\ndescribes an object, their attributes and their operations. To design any object model it provides \n\nimportant framework into which the dynamic and functional model can be placed. The main \n\nmotive is to construct an object model is to capture those concepts that are essential for the \n\nimplementation of the system. The object model describes the static structure of a system in \n\nterms of objects and relationships. \n\nThe object model can be represented graphically with the help of classes. Classes are \n\narranged in a hierarchy order to perform a specific task.\n\nThe dynamic model describes the interactions among objects in the system. It also describes the \n\ncontrol structure of a system in terms of events and states. It also describes the aspects of a \n\nsystem that change over time. It is used to specify and control structure of the system basically \n\nrelated to time and events.\n\n The dynamic model graphical represent with the help of state diagrams. The state \n\ndiagram is like as a graph whose nodes are states and arcs are transitions between states caused \n\nby events.\n\nThe functional model describes the data values transformation within the system. This model \n\nexplains all the transformations that are related to mapping, constraints and functional \n\ndependencies.\n\n The functional model represent with the help of DFD (data flow diagram). DFD is like \n\nas graph that describes the input from user, output to the user, and process.','Webtrainings4u','2015-05-16 06:11:28'),(26,4,'Delegation','Delegation is the assignment of responsibility or authority to from a manager to a subordinate to carry out specific tasks. It is basic concept of management leadership.\nA delegation can also be a group of individuals, often called delegates, who represent the interests of a larger organization or body, often from a geographical area.\n But it is also a programming technique. It has the same meaning in programming language the operation (method) is caught in the desired class and forward to another class for actual execution. Since each operation must be explicitly forward unexpected side effects are less likely to occur. The name of the catching class may differ from these in the class supplying the operations. Each class should choose names which are most appropriate for its purpose. When classes are not in true generalization relationship but methods are required to share among them. In case we should use delegation instead of inheritance.\nDelegation is the process of a computer user handing over their authentication credentials to another user. In role-based access control models, delegation of authority involves delegating roles that a user can assume or the set of permissions that he can acquire, to other users.\nThere are basically two classes of delegation.\nDelegation at Authentication/Identity Level\nDelegation at Authorization/Access Control Level\nDelegation at Authentication/Identity Level\nIn an authentication mechanism provides an effective identity different from the validated identity of the user then it is called identity delegation at the authentication level, provided the owner of the effective identity has previously authorized the owner of the validated identity to use his identity. \nDelegation at Authorization/Access Control Level\nDelegation of authority specifies who is authorized to act on behalf of the department or agency head and other key official for specific purpose.\nIf the delegation is for very specific rights, also known as fine-grained, such as with Role-based access control (RBAC) delegation, then there is always a risk of under-delegation, i.e., the delegator does not delegate all the necessary permissions to perform a delegated job. This may cause the denial of service, which is very undesirable in some environments, such as in safety critical systems or in health care.','Webtrainings4u','2015-05-20 07:08:13'),(27,4,'TEXTURE','Texture is the process to see that how things look like .We can see there are so many textures \n\nthings around us. It can be jagged and smooth. A soft cotton ball has a soft texture while sand \n\nhas a rough texture.\n\nMostly, textures are used by artists to make smooth and flat pictures using water paints. \n\nTexture can be defined as the property that is held by the surface of any object and whose \n\nexternal surface can be felt by the sense of touch. \n\nIn music, you can find a musical texture. In painting, texture is supposed to be the feeling \n\nof an individual about the painting and the application method used to paint the surface. \n\nBut in a computer graphics, texture is a bitmap image used to apply a design on to the \n\nsurface of 3-D computer model.\n\nThe texturing method is used to handle the variations of reflectance and involves storing of \n\nreflectance as a pixel image or function and mapping reflectance onto the required surface. This \n\ntype of image called text map and the process is texturing is calling texture mapping. So, we can \n\nsay texture is the image used to modify the appearance the object.\n\nTexture mapping is a graphical design in which a 2-D surface, that is texture map, is around a 3-D \n\nobject. \n\nPhysical texture is also known as actual texture. Physical texture can be felt by touch the object. \n\nFor examples, wood, statues, sand, glass and so on.\n\nVisual texture can be created by repetitive use of shapes and lines.','Webtrainings4u','2015-05-23 10:13:52'),(28,4,'Detection of Cyber Crime','Nowadays, internet miscreant and cyber criminals are increasing day by day and to detect and \nstop them has become a serious issue. Malicious flux networks are of major concern as this \nnetwork started thrives these days. Malicious flux networks are illegitimate content delivery \nnetworks (CDNs).These types of network uses the set of resolved IP addresses associated to \nthese networks change frequently even after each DNS query, thereby make it difficult to detect them.\n\nThe limitation of these works lies in the use of spam email. These use the spam mail as the primary information source. Through this they detect fast-flux domains advertised through email spam. This approach identify potential fast-flux domain names in the URLs found in the body of the spam emails. Then, a strategy is applied, by repeatedly issuing DNS queries to collect information about the set of resolved IP addresses, and by classifying each domain name one after another to either fast-flux or non fast-flux category.\n\nOur detection approach is based on passive monitoring. Monitoring passively live users DNS \ntraffic allows capturing queries to flux domain names that are advertised through a variety of \nmeans like blog spam, social websites spam, search engine spam, and instant messaging spam \netc. This approach could help to nab the criminals on cyber world.','Webtrainings4u','2015-05-27 06:11:07'),(29,4,'3D Technology','Do you know about 3D technology? In simple words, 3D technology stands for three-dimensional technology that offers a wide array of possibilities in near future in almost every walk of life and especially in entertainment segment. The use of 3D technology in TVs, laptops and other products is growing because the basic content required to support such products includes sports and movies. \n\nHow 3D technology works? In order to understanding the basic 3D technology techniques, it is important to first understand how the human sight works. Human beings have two eyes which are about 3 inches apart from each other. This distance between the two eyes produce two slightly different images which are transmitted to the brain. The brain will then make a space in where distance and depth can be perceived\n\n3D technology, as we know was once the private realm of skilled computer-graphics developers and gamers with high-end software. Complex mix of colors, textures, virtual lighting and perspective are necessary to make the images appear three-dimensional. Earlier, the 3D technology was expensive and difficult to use too. But today, the scenario has changed completely. One can find uses of 3D technology not confined to only games and entertainment, but there are multiple uses of 3D technology in PC, Architecture, and industry as well as in education too!\n\nThe images developed by using 3D technology are widely used in various presentations and are an essential part of modern design projects. 3D Architecture models are able to show any product from its best side and can be made in a very short time.\n','Webtrainings4u','2015-06-01 11:49:24'),(30,1,'Improve Your Php Skills with an Effective Online Php Training','In today’s competitive world, it is almost impossible to land a dream job that will fulfill all your expectations. But opting for the wise training module at the right time can surely make the career path to your dream job an easy one. Now a days many training programs claim to provide you the job which can wholly transform your life. However, the only training program that can now help you to grow and ultimately grab the high salaried jobs in IT companies. Since, PHP has the largest community of developers who have proven its capability and versatility by developing and maintaining some of the most highly visited and popular websites, this in turn has unleashed the profound significance of a PHP training course these days.\nPHP (recursive acronym for PHP: Hypertext Preprocessor) is a widely-used open source general-purpose scripting language that is especially suited for web development and used to create dynamic Web pages. PHP is used to add functionality to your site that HTML alone can’t achieve. PHP is probably the most popular scripting language on the web. With PHP, you can do things like create username and password login pages, check details from a form, create forums, picture galleries, surveys, and a whole lot more. PHP is widely used in different areas of web development because it provides several advantages:-\n\n    Compatibility with all operating systems\n    Unparalleled usability and efficiency.\n    Easy to mix with popular web applications.\n\nBefore checking into PHP tutorial for fresher, it is important seeking acquaintance with its ins and outs. You need to know its core content, usefulness and the generic features of usability. The language which has been scripted by the developers is singular in its platform and open sourced in its package. You are facilitated using the same by way of your web browser. The language helps you give way to versatile sites of e-commerce with the chiseled features of creativity, dynamism and interaction.\nWeb Training4u PHP program and its curriculum\nWeb Training4u offers extremely useful PHP training of six weeks within an affordable cost of Rs 6500/-  only, which is even reasonable than the training courses provided by established training institutes. We offer Online PHP training to let students learn PHP programming in the most effective manner.  As part of the PHP course for beginners, the focus mainly revolves around the integration of PHP & that of the MYSQL. Keeping this in mind; the curriculum includes the following aspects:-\n1. PHP & its essentials\n2. Introduction to MYSQL Database, with focus on Normalization and performing various operations on tables such as insertion, selection, deletion and updating.\n3. The multifaceted aspects of HTML, CSS and JavaScript are also a part of the curriculum.\n4. Advance PHP curriculum includes:-\n\n    Introduction to AJAX and XMLHTTPREQUEST object\n    Working with MVC by Implementing the CRUD implementation in Code Igniter (Framework).\n    Working with post, media, pages, and comments, widgets in Word press (CMS) with a focus on the configuration, installation and Conversion of Html template to Word press theme.\n\nThis will help trainees in acquiring a better understanding of the language and a practical learning exposure. The most beneficial aspect of our training program is that the trainees will get a chance to get employed in our organization. Through this training program, we transform the ability of the students to think differently and innovatively, and prepare them to become a prospective employee of an organization. \n\nFor any further assistance you can contact on +91-991584427 or Visit Our site at http://www.webtrainings4u.com\n','Webtrainings4u','2015-07-29 07:00:46'),(31,3,'               Build Your Future with Android','Nowadays, web applications are in high demand because it is open source and easy to use. Mobile phone technologies growth is increasing because numbers of users are increasing and the world is contracting. Among the various operating systems Android is used to develop web applications.\nAndroid is a Linux based operating system which is customizable and  it empowers more than billion devices across the globe from phones,tablets,cars, tv,watches and many more. This is popular due the fact  that applications are developed  in a  language that can be easily installed on mobile phones .There are various code names for android such as Aestro, Gingerbread, Blender, Donut,Honeycomb, Cupcake, Éclair, Ice Cream Sandwich, Jelly Bean, Kit Kat , Lollipop,Froyo . The various advantages of android are we can make applications then sell it to the goggle. The application then can be downloaded by many users. We can earn money by making our application useful to others. There are lots of  android applications in the market that are very popular these days like puzzle games etc.It is used  for entertainment and  to sharp our mind.  So, android training course is a necessity these days because nowadays everything is done through android applications and also android is evolving very fast in the industry. If you want to become a professional android Programmer then you need to contact with industry experts or you can join the online android Training Classes \nAndroid Training Classes by Web Training 4 u\nWeb training 4 u offers android training course  within an affordable cost Rs 6500. We offer best android training to let students learn android programming in our training module include.\nIntroduction \nAndroid Architecture\nCreating a project\nUser Interface Widgets\nNotification ,Toast,Dialog,List\nLocation and Map\nWorking with data storage\nAnimation, Content Provider\nNetwork Communication ,Services\nPublishing your application\n\n If you want to pursue your career in android then android training is very beneficial for you because we provide opportunity to students to work on live projects during training. We provide practical knowledge rather than theoretical knowledge to students and well experienced faculty solve the problem of students.\nhttp://webtrainings4u.blogspot.in/2015/07/build-your-future-with-android.html','Webtrainings4u','2015-08-05 07:49:46');
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_category`
--

DROP TABLE IF EXISTS `blog_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_category` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_category`
--

LOCK TABLES `blog_category` WRITE;
/*!40000 ALTER TABLE `blog_category` DISABLE KEYS */;
INSERT INTO `blog_category` VALUES (1,'PHP'),(3,'Android'),(4,'General'),(5,'asp.net');
/*!40000 ALTER TABLE `blog_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_comment`
--

DROP TABLE IF EXISTS `blog_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_comment` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `blog_id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_comment`
--

LOCK TABLES `blog_comment` WRITE;
/*!40000 ALTER TABLE `blog_comment` DISABLE KEYS */;
INSERT INTO `blog_comment` VALUES (1,13,'ss','ss','ss','ss'),(2,17,'codetrics','puneet9.4u@gmail.com','777777','aaaa'),(3,13,'ranjit','ranjit8kaur@gmail.com','1234567890','this is  comment'),(4,23,'Jeromemen','lawrencerobinson718@gmail.com','lawrencerobinson718@gmail.com','wells fargo wire routing  <a href= http://oxycontin.tribalpages.com/ >buy oxycontin</a>  prescription antifungal cream jock itch '),(5,31,'xznvzqnnhc','vtdhyb@hqnrqs.com','wAsLuYzW','AhfbCt  <a href=\"http://eexsuhkazlti.com/\">eexsuhkazlti</a>, [url=http://utezwbipslmp.com/]utezwbipslmp[/url], [link=http://zmiflpyuppev.com/]zmiflpyuppev[/link], http://xteemepsgxds.com/'),(6,30,'okfrygg','jhfhnt@efzvzw.com','NgluKjsCyZVsyHhv','9lmCRN  <a href=\"http://ejcuvpmeasgi.com/\">ejcuvpmeasgi</a>, [url=http://vocperlkmngu.com/]vocperlkmngu[/url], [link=http://uzvucvtnanxx.com/]uzvucvtnanxx[/link], http://wyupnwthzxmq.com/'),(7,28,'ovdjkoos','swxgiy@kjdoaz.com','WBKntTZTMUQlXNj','kO2sBZ  <a href=\"http://rmjzdtmapjqk.com/\">rmjzdtmapjqk</a>, [url=http://ffkfsizwznfy.com/]ffkfsizwznfy[/url], [link=http://nkblatotehlc.com/]nkblatotehlc[/link], http://qzwubxrsukrg.com/'),(8,29,'gnkqzvtt','vqlyyn@vxfyrb.com','jmwXPsvFTUZvxAiNK','v9gVSR  <a href=\"http://asjkgxagkkyk.com/\">asjkgxagkkyk</a>, [url=http://xflkkuclydyp.com/]xflkkuclydyp[/url], [link=http://prhhgptwwbid.com/]prhhgptwwbid[/link], http://whqbwrdxrajn.com/'),(9,29,'ixvawcr','pnocna@uotwxi.com','ScXtQnJXQApbRA','x9Qz22  <a href=\"http://xgkpwabzhjpr.com/\">xgkpwabzhjpr</a>, [url=http://fwfxcqsngegg.com/]fwfxcqsngegg[/url], [link=http://zeipijiopdup.com/]zeipijiopdup[/link], http://xgsjpaufonwk.com/'),(10,22,'hwypgl','kllecc@pjuewx.com','AJQMEyLNYumfrWPJ','O9WI1u  <a href=\"http://goqsskwevypa.com/\">goqsskwevypa</a>, [url=http://octbgewgsjvx.com/]octbgewgsjvx[/url], [link=http://wedkyrqyvnmq.com/]wedkyrqyvnmq[/link], http://pulpiigtrazw.com/'),(11,31,'lmlmgokvscg','yevyer@uwozcl.com','eesaQtIh','wHUKCZ  <a href=\"http://tdgetynemxhw.com/\">tdgetynemxhw</a>, [url=http://ynjiwsvxvykb.com/]ynjiwsvxvykb[/url], [link=http://ectwqxursefq.com/]ectwqxursefq[/link], http://aoekantstrgi.com/'),(12,29,'wxukpse','ibablb@aqesdu.com','ukWGVOfVck','vQKZYp  <a href=\"http://ddbcalmupdiw.com/\">ddbcalmupdiw</a>, [url=http://tsfohwlhryzu.com/]tsfohwlhryzu[/url], [link=http://vnytmohevvlc.com/]vnytmohevvlc[/link], http://mosyvsodclrc.com/'),(13,27,'eeblndda','yidapg@jrwtiu.com','BTImxSWtEJtCjsZDIF','i4qYy6  <a href=\"http://yejvkcpykulb.com/\">yejvkcpykulb</a>, [url=http://urfmqhqzyenv.com/]urfmqhqzyenv[/url], [link=http://fkndynqefzlq.com/]fkndynqefzlq[/link], http://ahhatzqyflsm.com/'),(14,27,'mzirivrr','acfuqf@lwabbo.com','qCwyKeztUdws','E87yz1  <a href=\"http://jxracjqasged.com/\">jxracjqasged</a>, [url=http://odopalnaidgt.com/]odopalnaidgt[/url], [link=http://rzripdcgmssv.com/]rzripdcgmssv[/link], http://axfkvipiquts.com/'),(15,23,'Georgesi','ronaperry66@gmail.com','ronaperry66@gmail.com','how many states have legalized medical marijuana  <a href= http://oxycontin.tribalpages.com/ >buy oxycontin online</a>  american alliance of healthcare providers '),(16,23,'efqoatlwr','livmli@avbfkf.com','GEwymjpmqkqLUWudrtC','ywkEhB  <a href=\"http://xzetuwafzypp.com/\">xzetuwafzypp</a>, [url=http://xoessfoioucs.com/]xoessfoioucs[/url], [link=http://zisrwzjektpt.com/]zisrwzjektpt[/link], http://lublkzuebwxj.com/'),(17,13,'jcyetvfx','lekbpk@wwlsam.com','lRPqpTdlVAMgiLa','Wlih3c  <a href=\"http://bugghrcgwxia.com/\">bugghrcgwxia</a>, [url=http://yzpekoasbjml.com/]yzpekoasbjml[/url], [link=http://cuaeqnrbqtln.com/]cuaeqnrbqtln[/link], http://rjlbsgiqlahv.com/'),(18,30,'ergjoj','brhyle@kjmsar.com','jkJrQIpakkxo','XT2j4q  <a href=\"http://pypwlrvshwjh.com/\">pypwlrvshwjh</a>, [url=http://aaqevogwtppw.com/]aaqevogwtppw[/url], [link=http://abaauztpfwzv.com/]abaauztpfwzv[/link], http://klqbmhfzgosu.com/'),(19,24,'jztvvxtq','momgnz@htieue.com','akzcXUtVkYmjSJfEPw','zceeGV  <a href=\"http://kzjlklylnuyy.com/\">kzjlklylnuyy</a>, [url=http://aqbxhyzcpppv.com/]aqbxhyzcpppv[/url], [link=http://epsyewjbvyas.com/]epsyewjbvyas[/link], http://nfemgimfhnao.com/'),(20,16,'hdiuhrsfpo','mvftzj@dfdsfg.com','cXDCsmzcNh','EovKgS  <a href=\"http://fswueozmbzie.com/\">fswueozmbzie</a>, [url=http://bygzldjvawde.com/]bygzldjvawde[/url], [link=http://qkzclcxnnsin.com/]qkzclcxnnsin[/link], http://csenkzzagtat.com/'),(21,28,'gtjjrvew','cjteqg@gmtgld.com','lEMxbANDQYCS','D1vvAM  <a href=\"http://moxpcnagxqpa.com/\">moxpcnagxqpa</a>, [url=http://wmecenglznrm.com/]wmecenglznrm[/url], [link=http://avunmktnilqd.com/]avunmktnilqd[/link], http://acrmpheecnhh.com/'),(22,30,'csdhms','gemtci@dxanab.com','aKUgtBsQR','7i9KNM  <a href=\"http://lqitvmghclnp.com/\">lqitvmghclnp</a>, [url=http://ptcflunjxnot.com/]ptcflunjxnot[/url], [link=http://ojnnffcnvebw.com/]ojnnffcnvebw[/link], http://kmjkewjxqiox.com/'),(23,22,'kxmumxqybxn','alxmzv@gipsiq.com','eTzjfHbuKz','isqOLg  <a href=\"http://otmfwmoftlrz.com/\">otmfwmoftlrz</a>, [url=http://kvgzbivttipe.com/]kvgzbivttipe[/url], [link=http://okffgicgybye.com/]okffgicgybye[/link], http://psirepfkgpuu.com/'),(24,25,'nvfqjxhznlj','xtutxr@arcmpr.com','qsoVFfUTzDVppuCdRv','1Zsh3C  <a href=\"http://xvnjxyxuankc.com/\">xvnjxyxuankc</a>, [url=http://lozxettkwvgf.com/]lozxettkwvgf[/url], [link=http://ixfsqtpqweez.com/]ixfsqtpqweez[/link], http://ddnibllwxkwa.com/'),(25,26,'qimeznw','udmgve@avoiub.com','jyeVhfUj','HiNyv8  <a href=\"http://rumsckhennnb.com/\">rumsckhennnb</a>, [url=http://hssjdfrandur.com/]hssjdfrandur[/url], [link=http://inxnewhlwxpp.com/]inxnewhlwxpp[/link], http://kifwyhxleboy.com/'),(26,31,'Mark','mark357177@hotmail.com','mark357177@hotmail.com','J7MxlX http://www.FyLitCl7Pf7kjQdDUOLQOuaxTXbj5iNG.com'),(27,31,'Mark','mark357177@hotmail.com','mark357177@hotmail.com','3x0XX1 http://www.FyLitCl7Pf7kjQdDUOLQOuaxTXbj5iNG.com'),(28,31,'Mark','mark357177@hotmail.com','mark357177@hotmail.com','INOaSC http://www.FyLitCl7Pf7kjQdDUOLQOuaxTXbj5iNG.com'),(29,31,'Mark','mark357177@hotmail.com','mark357177@hotmail.com','hGmQgZ http://www.FyLitCl7Pf7kjQdDUOLQOuaxTXbj5iNG.com');
/*!40000 ALTER TABLE `blog_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_images`
--

DROP TABLE IF EXISTS `blog_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_images` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `blog_id` int(255) NOT NULL,
  `image` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_images`
--

LOCK TABLES `blog_images` WRITE;
/*!40000 ALTER TABLE `blog_images` DISABLE KEYS */;
INSERT INTO `blog_images` VALUES (2,13,'4.jpeg'),(3,13,'5.jpeg'),(4,15,'6.jpeg'),(5,16,'8.jpeg'),(7,17,'5.jpeg'),(9,18,'7.jpeg'),(10,13,'Hydrangeas.jpg'),(11,13,'about.png'),(12,13,'fixit.png'),(13,13,'fixit.png'),(14,13,'fixit.png'),(15,13,'man.jpg'),(20,22,'20120503202942.gif'),(21,13,'webtrainings4u template.jpg'),(22,23,'web-traning-orng.jpg'),(23,23,'web-traning-orng.jpg'),(24,22,'web-train-twit.jpg5.jpg');
/*!40000 ALTER TABLE `blog_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_id` int(50) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `detail` text NOT NULL,
  `display_order` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'PHP',0,'php.png','PHP (Hypertext Preprocessor) is a very widely used language used as open source scripting language which can be executed on the serve â€“ side . It runs on various platforms like Windows, Mac OS, Unix, Linux etc.It suports various databases like MySql, Microsoft SQL Server, Sybase etc .It is embedded in HTML, and it is used to manages dynamic content,sesion tracking,cookie management,databases etc.',''),(2,'HTML',0,'html.png','HTML is a markup language. The word markup was used by editors who marked up manuscripts when giving instructions for revisions. A markup language as it relates to browsers is a language with specific syntax that gives instructions to a web browser about how to display a page. HTML uses a pre-defined set of elements to identify content types. Elements contain one or more \"tags\" that contain or express content. Tags are surrounded by angle brackets, and the \"closing\" tag (the one that indicates the end of the content) is prefixed by a forward slash.\n',''),(4,'php for 6 week',1,'','',''),(5,'html 5',2,'','',''),(6,'php basics',1,'','',''),(7,'Java',0,'java.png','Java is a high-level, third generation programming language, like C, FORTRAN, Smalltalk, Perl, and many others.Java is an object-oriented programming language that can handle graphics and user interfaces and that can be used to create web applications,desktop applications etc .You can use Java to write computer applications that play games, store data or do any of the thousands of other things computer software can do. It is platform independent language. Java also has standard libraries and syntax of Java is the same as C and C++. ','1'),(9,'Cake php',1,'','',''),(10,'.net',0,'net.png','The .NET platform is a component of Microsoft Windows operating system for building , running software applications and Web services. The .NET framework provides a new,simplified model for programming and deploying applications. It provides multiplatform applications, automatic resource management, and simplification of application.Security is an necessary part of .NET, it provides security support for e.g code authenticity,access authorizations, declarative,imperative and cryptographic security methods for embedding into applications. ',''),(11,'IOS',0,'apple.png','iOS is the operating system that runs on iPad, iPhone, and iPod touch devices. The operating system provide the technologies whcich are required to implement native apps. The operating system also have various system apps,for example Phone, Mail, and Safari, that provide standard system services. The iOS Software Development Kit (SDK) contains the tools and interfaces needed to develop, install, run, and test native apps that appear on an iOS device\'s Home screen. Native apps are built using the iOS system frameworks and Objective-C language and run directly on iOS.',''),(13,'Advance IOS',11,'','',''),(14,'Android',0,'android.png','Android is most popular and widely used operating system. Developed by Google, this OS is based on Linux kernel. Android is best suited for companies dealing with technology as it is customizable for device like mobiles, smart phones, tablets etc. It is foundation for community driven projects because of its Open - source feature. As it is very much corresponds to real world applications, devices with Android OS are becoming an integral part of everyone\'s daily routine.',''),(15,'JQuery',0,'jquery.png','jQuery takes a lot of common tasks that require many lines of JavaScript code to accomplish, and wraps them into methods that you can call with a single line of code.jQuery also simplifies a lot of the complicated things from JavaScript, like AJAX calls and DOM manipulation. The use of jQuery is much easier than the use of JavaScript on your website. ',''),(16,'Wordpress',0,'wordpress.png','WordPress is the most popular Content Management System (CMS) available today. This course will teach you how to use the power of WordPress CMS to build and navigate websites.How to publish websites using the core functionality of the WordPress platform. You will also be introduced on how to customize themes, build e-commerce shops, and implement basic SEO into your WordPress website. Prerequisites: HTML and CSS or have similar working knowledge and experience ',''),(17,'Java for 6 weeks',7,'','',''),(18,'Android for bigners',14,'','',''),(19,'asp.net',10,'','',''),(20,'Jquery for 6 months',15,'','',''),(21,'Wordpress CMS',16,'','','');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_tutors`
--

DROP TABLE IF EXISTS `course_tutors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_tutors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tutor_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_tutors`
--

LOCK TABLES `course_tutors` WRITE;
/*!40000 ALTER TABLE `course_tutors` DISABLE KEYS */;
INSERT INTO `course_tutors` VALUES (2,'1','','2'),(3,'1','','1'),(5,'2','','1');
/*!40000 ALTER TABLE `course_tutors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` varchar(255) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `features` text NOT NULL,
  `syllabus` text NOT NULL,
  `duration` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `image` varchar(255) NOT NULL,
  `fee` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES (2,'4','php for 6 week','<p>PHP, also known as the \"PHP: Hypertext Preprocessor,\" is a widely used computer programming language. It is used to create dynamic Web pages, or Web pages that update and display information depending on the user\'s activity.</p>','<p>Some of the key features of PHP 5.1.x include:</p><ul> <li>A complete rewrite of date handling code, with improved timezone support.</li><li>Significant performance improvements compared to PHP 5.0.X.</li><li>PDO extension is now enabled by default.&</li></ul>','<p>\r\n	Introduction to PHP</p><ul>\r\n	\r\n<li>&nbsp;Evaluation of Php&nbsp;</li>	\r\n<li>Basic Syntax&nbsp;</li>	\r\n<li>Defining variable</li></ul>',' 2weeks','2014-10-08 00:00:00','2014-10-08 00:00:00','','1000'),(3,'5','html 5','<p><strong>HTML</strong> or <strong>HyperText Markup Language</strong> is the standard e&nbsp;used to create&nbsp;</p><p>HTML is written in the form of consisting of <em>tags</em> enclosed in like <code>&lt;html&gt;</code>). HTML tags most commonly come i','<p>HTML 5 features which are useful <em style=\"background-color: initial;\">right now</em> include:</p><ul> <li>Web Workers: Certain web applications use heavy scripts to perform functions. Web Workers use separate background threads for processing and</li></ul>','<p>HTML</p><p><br><img src=\"http://www.sssit.org/images/check.png\" style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Verdana; font-size: 13px;\">	Introduction to HTML</p>','6 weeks','2014-10-08 00:00:00','2014-10-08 00:00:00','','2'),(4,'6','php basics','<p>fsf</p><p>fsdfs</p><p>fsf</p><p>sf</p>','<p>fff</p><p>ef</p><p>f</p><p>ef</p>','<p>ff</p><p>sdf</p><p>sf</p>','4 weeks','2014-10-13 00:00:00','2014-10-13 00:00:00','','200'),(5,'9','Cake php','<p>The CakePHP cookbook is an openly developed and community editable documentation project. We hope to maintain a high level of quality, validity and accuracy for the documentation. Notice the “Improve this Doc” button in the upper right-hand corner; it will direct you to the GitHub online editor of the active page, allowing you to easily contribute any additions, deletions, or corrections to the documentation.</p>','<h6>Conventions</h6><p>\n	Learn about a typical <a href=\"http://book.cakephp.org/2.0/en/getting-started/a-typical-cakephp-request.html\"><em>CakePHP request</em></a> and the <a href=\"http://book.cakephp.org/2.0/en/getting-started/cakephp-conventions.html\"><em>conventions</em></a> that power CakePHP.</p><h3>Controllers</h3><p>\n	Controllers handle requests and help co-ordinate and prepare responses for the client. Read more about <a href=\"http://book.cakephp.org/2.0/en/controllers.html\"><em>Controllers</em></a>.</p><h3>Views</h3><p>\n	Views are the presentation layer in CakePHP. They convert the data fetched from Models into the output format requested by the client. Read more about <a href=\"http://book.cakephp.org/2.0/en/views.html\"><em>Views</em></a>.</p><h3>Models</h3><p>\n	Models are the heart of your application. They handle the validation, storage and retrieval of your data. Read more about <a href=\"http://book.cakephp.org/2.0/en/models.html\"><em>Models</em></a>.</p>','<h1>Blog Tutorial</h1><p>Welcome to CakePHP. You’re probably checking out this tutorial because you want to learn more about how CakePHP works. It’s our aim to increase productivity and make coding more enjoyable: we hope you’ll see this as you dive into the code.</p><p>This tutorial will walk you through the creation of a simple blog application. We’ll be getting and installing CakePHP, creating and configuring a database, and creating enough application logic to list, add, edit, and delete blog posts.</p><p>Here’s what you’ll need:</p><ol><li>A running web server. We’re going to assume you’re using Apache, though the instructions for using other servers should be very similar. We might have to play a little with the server configuration, but most folks can get CakePHP up and running without any configuration at all. Make sure you have PHP 5.2.8 or greater.</li><li>A database server. We’re going to be using MySQL server in this tutorial. You’ll need to know enough about SQL in order to create a database: CakePHP will be taking the reins from there. Since we’re using MySQL, also make sure that you have <tt>pdo_mysql</tt> enabled in PHP.</li><li>Basic PHP knowledge. The more object-oriented programming you’ve done, the better: but fear not if you’re a procedural fan.</li><li>Finally, you’ll need a basic knowledge of the MVC programming pattern. A quick overview can be found in <a href=\"http://book.cakephp.org/2.0/en/cakephp-overview/understanding-model-view-controller.html\"><em>Understanding Model-View-Controller</em></a>.  Don’t worry, it’s only half a page or so.</li></ol><p>Let’s get started!</p>','6 weeks','2014-10-18 00:00:00','2014-10-18 00:00:00','','20'),(6,'13','Advance IOS','<p>wrwr</p>','<p>werwer</p>','<p>werwr</p>','4 weeks','2014-11-05 00:00:00','2014-11-05 00:00:00','','100'),(7,'17','Java for 6 weeks','<p>Java is a high-level, third generation programming language, like C, FORTRAN, Smalltalk, Perl, and many others.Java is an object-oriented programming language that can handle graphics and user interfaces and that can be used to create web applications,desktop applications etc .You can use Java to write computer applications that play games, store data or do any of the thousands of other things computer software can do. It is platform independent language. Java also has standard libraries and syntax of Java is the same as C and C++.</p>','<h5>Features of Java</h5><p>There is given many features of java. They are also known as java buzzwords. The Java Features given below are simple and easy to understand.</p><ol> <li>Simple</li><li>Object-Oriented</li><li>Platform independent</li><li>Secured</li><li>Robust</li><li>Architecture neutral</li><li>Portable</li><li>Dynamic</li><li>Interpreted</li><li>High Performance</li><li>Multithreaded</li><li>Distributed</li></ol>','<h5>\nSyllabus  of Java\n</h5><p>Java - What, Where and Why? <br>History and Features of Java <br>&nbsp;Internals of Java Program<br>Difference between JDK,JRE and JVM <br>Internal Details of JVM <br>Variable and Data Type<br>Unicode System <br>Naming Convention</p>','6 weeks','2014-12-10 00:00:00','2014-12-10 00:00:00','','50'),(8,'18','Android for bigners','<p>Android is most popular and widely used operating system. Developed by Google, this OS is based on Linux kernel. Android is best suited for companies dealing with technology as it is customizable for device like mobiles, smart phones, tablets etc. It is foundation for community driven projects because of its Open - source feature. As it is very much corresponds to real world applications, devices with Android OS are becoming an integral part of everyone\'s daily routine.</p>','<dt>Messaging</dt><dd>SMSand MMS are available forms of messaging, including threaded text messaging and Android Cloud To Device Messaging (C2DM) and now enhanced version of C2DM, Android Google Cloud Messaging (GCM) is also a part of Android Push Messaging service.</dd><dt>Web browser</dt><dd>The web browser available in Android is based on the open-source Blink (previously WebKit) layout engine, coupled with Chrome\'s V8 JavaScript engine. The browser scores 100/100 on the Acid3 test on Android 4.0.</dd><dt>Voice based features</dt><dd>Google search through voice has been available since initial release.Voice actions for calling, texting, navigation, etc. are supported on Android 2.2 onwards.As of Android 4.1, Google has expanded Voice Actions with ability to talk back and read answers from Google\'s Knowledge Graph when queried with specific commands.<sup><a href=\"http://en.wikipedia.org/wiki/List_of_features_in_Android#cite_note-6\">[</a></sup>The ability to control hardware has not yet been implemented.</dd><dt>Multi-touch</dt><dd>Android has native support for multi-touch which was initially made available in handsets such as the HTC Hero. The feature was originally disabled at the kernel level (possibly to avoid infringing Apple\'s patents on touch-screen technology at the time).Google has since released an update for the Nexus One and the Motorola Droidwhich enables multi-touch natively.</dd><dt>Multitasking</dt><dd>Multitasking of applications, with unique handling of memory allocation, is available.</dd><dt>Screen capture</dt><dd>Android supports capturing a screenshot by pressing the power and volume-down buttons at the same time.<sup>[10]</sup> Prior to Android 4.0, the only methods of capturing a screenshot were through manufacturer and third-party customizations or otherwise by using a PC connection (DDMS developer\'s tool). These alternative methods are still available with the latest Android.</dd><dt>Video calling</dt><p>Android does not support native video calling, but some handsets have a customized version of the operating system that supports it, either via the UMTS network (like the Samsung Galaxy S) or over IP. Video calling through Google Talk is available in Android 2.3.4 and later. Gingerbread allows Nexus S to place Internet calls with a SIP account. This allows for enhanced VoIP dialing to other SIP accounts and even phone numbers. Skype 2.1 offers video calling in Android 2.3, including front camera support. Users with the Google+ Android app can video chat with other google+ users through hangouts</p>','<h5>Syllabus Overview:</h5><p> Our Google ANDROID course is of Three months duration. Here is the course outline:</p><p>\n	1. Programming Revision: Java fundamentals are revised – to be an Android developer, you basics should be clear.<br>\n	2. Android: Activities, Content Providers, Intents, Services, Storage, Network, Multimedia, GPS, Phone services, XML Layouts, Widgets, Permissions, Sensor Manager, Accelerometer, Gyroscope, SQLite, RSS, Threads, Animation, Refresh handler, JSON, Game Engine, Full game, Animated 2D Games, Google Play Store and a lot more.<br>\n	3. Trainings on 3 Live Projects with Certificates. For our Sunday only classes, there are 2 Live Projects.<br>\n	4. Nationwide placement assistance : CV Preperation, HR Interview – English preperation, Technical Interview preperation.</p>','1 months','2014-12-10 00:00:00','2014-12-10 00:00:00','','10'),(9,'19','asp.net','<p>The .NET platform is a component of Microsoft Windows operating system for building , running software applications and Web services. The .NET framework provides a new,simplified model for programming and deploying applications. It provides multiplatform applications, automatic resource management, and simplification of application.Security is an necessary part of .NET, it provides security support for e.g code authenticity,access authorizations, declarative,imperative and cryptographic security methods for embedding into applications.</p>','<h5>\n	Features of asp.net\n</h5><p>\n	<strong style=\"background-color: initial;\">Model Binding - Isolating the Web Form from the Model</strong></p><p>\n	The Model binding feature in ASP.NET 4.5 enables you to develop Webforms that are independent of the Model that populates the view. The biggest advantage of using Model Binding in ASP.NET is that you can easily unit test the methods. In ASP.NET 4.5 support for model binding is provided through the usage of the  ‘System.Web.ModelBinding’ namespace. This namespace contains value provider classes like ControlAttribute, QueryStringAttribute, etc. All these classes are inherited from the ValueProviderSourceAttribute class.</p><p>\n	<strong>Value Providers</strong></p><p>\n	ASP.NET4.5 provides many Value Providers that can be used to filter data. These are:</p><ul>\n	\n<li>Querystring</li>	\n<li>Session</li>	\n<li>Cookie</li>	\n<li>Control Value</li></ul><p>\n	You can also have your own custom value providers.</p><p>\n	<strong>Support for OpenID in OAuth Logins</strong></p><p>\n	ASP.NET 4.5 provides support for OpenID for OAuth logins - you can easily use external services to login to your application. Like ASP.NET MVC 4, ASP.NET 4.5 enables you to register OAuth provider in the App_Start/AuthConfig.cs file. We can also use this data dictionary to pass additional data.</p>','<h5>Overview of the ASP.NET</h5><ul>\n	\n<li>Introduction of different Web Technology</li>	\n<li>What is Asp.Net</li>	\n<li>How Asp.Net Works</li>	\n<li>Use of visual studio</li>	\n<li>Different Languages used in Asp.Net.</li>	\n<li>Summary</li></ul><p>Framework</p><ul>\n	\n<li>Common Language Runtime (CLR)</li>	\n<li>.NET Framework Class Library.</li>	\n<li>Summary</li></ul><p>Setting up and Installing ASP.NET</p><ul>\n	\n<li>Installing Internet Information Server</li>	\n<li>Installation of Asp.Net</li>	\n<li>virtual directory</li>	\n<li>Application Setting in IIS.</li>	\n<li>Summary</li></ul><p>Microsoft SQL Server 2012</p><ul>\n	\n<li>Overview of SQL Server 2012</li>	\n<li>Installation of SQL Server 2012</li>	\n<li>Features of SQL Server Express</li>	\n<li>SQL Server 2008 Express management tools</li>	\n<li>Summary</li></ul>','2 months','2014-12-10 00:00:00','2014-12-10 00:00:00','','15'),(10,'20','Jquery for 6 months','<p>jQuery takes a lot of common tasks that require many lines of JavaScript code to accomplish, and wraps them into methods that you can call with a single line of code.jQuery also simplifies a lot of the complicated things from JavaScript, like AJAX calls and DOM manipulation. The use of jQuery is much easier than the use of JavaScript on your website.</p>','<p>Query is a lightweight, \"write less, do more\", JavaScript library.</p><p>The purpose of jQuery is to make it much easier to use JavaScript on your website.</p><p>jQuery takes a lot of common tasks that require many lines of JavaScript code to accomplish, and wraps them into methods that you can call with a single line of code.</p><p>jQuery also simplifies a lot of the complicated things from JavaScript, like AJAX calls and DOM manipulation.</p><p>The jQuery library contains the following features:</p><ul> <li>HTML/DOM manipulation</li><li>CSS manipulation</li><li>HTML event methods</li><li>Effects and animations</li><li>AJAX</li><li>Utilities</li></ul>','<h5>Javascript course syllabus</h5><section><p>1. Introduction</p><ol>\n	\n<li>Simple Addition</li>	\n<li>Fun With Text</li>	\n<li>Combining Text and Numbers</li></ol></section><section><p>2. Variables and Operators</p><ol>\n	\n<li>Favorite Language, Again</li>	\n<li>What is a Variable?</li>	\n<li>Your Name</li>	\n<li>Let\'s Make a Sentence</li>	\n<li>Numbers in Variables</li>	\n<li>Changing Variable Values</li>	\n<li>More on Assignment</li>	\n<li>Equality</li>	\n<li>What\'s Your Type?</li>	\n<li>Empty Cans, Empty Variables</li>	\n<li>Increment Operator</li>	\n<li>Adding Numbers, Again</li>	\n<li>Another Addition Operator</li></ol></section><section><p>3. Dive into Strings</p><ol>\n	\n<li>What Are Strings?</li>	\n<li>Combining Strings</li>	\n<li>Length of a String</li>	\n<li>Selecting Characters</li>	\n<li>Substrings</li>	\n<li>Change to Upper Case</li>	\n<li>What is your Index?</li>	\n<li>Reversing a String</li>	\n<li>What\'s the Secret?</li></ol></section>','6 months','2014-12-10 00:00:00','2014-12-10 00:00:00','','25'),(11,'21','Wordpress CMS','<p>WordPress is the most popular Content Management System (CMS) available today. This course will teach you how to use the power of WordPress CMS to build and navigate websites.How to publish websites using the core functionality of the WordPress platform. You will also be introduced on how to customize themes, build e-commerce shops, and implement basic SEO into your WordPress website. Prerequisites: HTML and CSS or have similar working knowledge and experience</p>','<h5>\n	Features\n</h5><li><strong>Simplicity</strong> Simplicity makes it possible for you to get online and get publishing, quickly. Nothing should get in the way of you getting your website up and your content out there. WordPress is built to make that happen.</li><li><strong>Flexibility</strong> With WordPress, you can create any type of website you want: a personal blog or website, a photoblog, a business website, a professional portfolio, a government website, a magazine or news website, an online community, even a network of websites. You can make your website beautiful with themes, and extend it with plugins. You can even build your very own application.</li><li><strong>Publish with Ease</strong> If you\'ve ever created a document, you\'re already a whizz at creating content with WordPress. You can create Posts and Pages, format them easily, insert media, and with the click of a button your content is live and on the web.</li><li><strong>Publishing Tools</strong> WordPress makes it easy for you to manage your content. Create drafts, schedule publication, and look at your post revisions. Make your content public or private, and secure posts and pages with a password.</li><li><strong>User Management</strong> Not everyone requires the same access to your website. Administrators manage the site, editors work&nbsp;</li>','<h5>Syllabus</h5><p>Seminar</p><p>\n	Lecture mode. Bring your devices to take notes if you like but this is not a hands-on seminar. It will be recorded and provided to you after the class.<br>\n	<a href=\"http://socialmediabymichelle.com/wordpress/wp-content/uploads/2012/11/IMG_6669.jpg\"><img title=\"IMG_6669\" alt=\"\" src=\"http://socialmediabymichelle.com/wordpress/wp-content/uploads/2012/11/IMG_6669.jpg\" width=\"400\" height=\"266\" style=\"font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; margin: 5px 0px 20px 20px; padding: 0px; float: right;\"></a></p><p>\n	Claim a physical seat (come to Whitinsville) or sign up for a live virtual seat.</p><ul>\n	<li>Why blogging is important</li>	<li>Host Providers and Domain Names</li>	<li>WordPress.com vs. self hosted WordPress.org</li>	<li>Costs and steps to set up WordPress.org</li>	<li>Should it BE your website or sit inside your website</li>	<li>Reviewing the dashboard</li>	<li>Themes, plugins, widgets, menus</li>	<li>Pages vs. Posts</li>	<li>Premium themes and where to find them (adult/child themes)</li>	<li>Creating basic pages and posts</li></ul>','5 months','2014-12-10 00:00:00','2014-12-10 00:00:00','','35');
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pdf`
--

DROP TABLE IF EXISTS `pdf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pdf`
--

LOCK TABLES `pdf` WRITE;
/*!40000 ALTER TABLE `pdf` DISABLE KEYS */;
INSERT INTO `pdf` VALUES (1,'<p>Php Pdf</p>','1','5434eee689fa9DR_Packing_Slip.pdf'),(3,'hfgjfgjg htfghfgh','2','05php.pdf'),(4,'<p>fgd gfcfdg</p>','1','544de1732507b05php.pdf');
/*!40000 ALTER TABLE `pdf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_courses`
--

DROP TABLE IF EXISTS `student_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `qty` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'new',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_courses`
--

LOCK TABLES `student_courses` WRITE;
/*!40000 ALTER TABLE `student_courses` DISABLE KEYS */;
INSERT INTO `student_courses` VALUES (1,'php for 6 week','','1000','4','1','Course Over','2014-12-15','2014-12-29'),(3,'Java for 6 weeks','','50','17','3','Course Over','2014-12-15','2015-01-26'),(4,'Cake php','','20','9','0','Course Over','2014-12-16','2015-01-27'),(5,'html 5','','2','5','1','Course Over','2014-12-16','2015-01-27'),(6,'php basics','','200','6','1','Course Over','2014-12-16','2015-01-13');
/*!40000 ALTER TABLE `student_courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (1,'ranjit','aulakh','ranjit8kaur@gmail.com','123','123456'),(2,'tttttt','tttttt','tttttttttt@gmail.com','ttttttt','231323'),(3,'kamal','maan','kamalmaan82@gmail.com','123','123213'),(4,'abc','bhbh','aulakh8ranjit@gmail.com','123','123456'),(5,'aman','kaur','amandeep00923@gmail.com','123','12345'),(6,'test','test','ranjit00923@gmail.com','123456','1234567890'),(7,'prince','kumar','princekumar00923@gmail.com','123456','123456789');
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutorials`
--

DROP TABLE IF EXISTS `tutorials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tutorials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tutorials`
--

LOCK TABLES `tutorials` WRITE;
/*!40000 ALTER TABLE `tutorials` DISABLE KEYS */;
INSERT INTO `tutorials` VALUES (1,'<p>tutorial about php</p>','1','https://www.youtube.com/watch?v=l21g8dJmD7U&index=1&list=PL21E20F9A122DC853'),(2,'<p>gd ytn&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; jgyj</p>','2','https://www.youtube.com/watch?v=v4oN4DuR7YU');
/*!40000 ALTER TABLE `tutorials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutors`
--

DROP TABLE IF EXISTS `tutors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tutors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL DEFAULT 'elearning@12345',
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tutors`
--

LOCK TABLES `tutors` WRITE;
/*!40000 ALTER TABLE `tutors` DISABLE KEYS */;
INSERT INTO `tutors` VALUES (1,'Er. Davinder Singh','Bachelors in technology and Web Developer','davinder@gmail.com','123','5434eca7a70f8765-default-avatar.png'),(2,'Er. Kamal Maan','Bachelors in technology and Web Developer','kamal@gmail.com','123','54350f50c59ca765-default-avatar.png');
/*!40000 ALTER TABLE `tutors` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-27  7:12:58
